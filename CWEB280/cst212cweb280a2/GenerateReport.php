<!DOCTYPE html>
<html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <link type="text/css" href="tableStyle.css" rel="stylesheet">
            <title>Dog Database</title>
            <style>
                
            </style>
                
        </head>
        <body>
            <h1>Dog Report</h1>
                <ul class="menu">
                  <li><a href="index.php" class="active"><span>Home</span></a></li>
                  <li><a href="Registration.php"><span>Registration</span></a></li>
                </ul>
                
            <div class="CSSTableGenerator">
            <?php
            $db = new DbObject();

            if(isset($_REQUEST["Owner"]))
            {   
                if($_COOKIE["sortCounter"] % 2 == 0)
                {                                        
                    $qryStmt = "SELECT dogName AS Name,
                               dogBreed AS Breed, dogAge AS Age, dogGender AS Gender,
                               dogFixed AS Fixed, ownerName AS Owner,
                               phoneNumber AS 'Phone#', emailAddress AS Email
                               FROM Dog D JOIN Owner O ON D.ownerID = O.ownerID ORDER BY Owner;";
                    
                    $qryResults = $db->runQuery($qryStmt);                           
                    $db->displayRecords($qryResults);                    
                    $qryResults->free();    
                }
                else
                {
                    $qryStmt = "SELECT dogName AS Name, 
                            dogBreed AS Breed, dogAge AS Age, dogGender AS Gender,
                            dogFixed AS Fixed, ownerName AS Owner,
                            phoneNumber AS 'Phone#', emailAddress AS Email
                            FROM Dog D JOIN Owner O ON D.ownerID = O.ownerID ORDER BY Owner DESC;";
                    
                    $qryResults = $db->runQuery($qryStmt);                           
                    $db->displayRecords($qryResults);
                    $qryResults->free();    
                }        
            }
            elseif(isset($_REQUEST["Name"]))
            {
                if($_COOKIE["sortCounter"] % 2 == 0)
                {
                    $qryStmt = "SELECT dogName AS Name, 
                                dogBreed AS Breed, dogAge AS Age, dogGender AS Gender,
                                dogFixed AS Fixed, ownerName AS Owner,
                                phoneNumber AS 'Phone#', emailAddress AS Email
                                FROM Dog D JOIN Owner O ON D.ownerID = O.ownerID ORDER BY Name;";
                    
                    $qryResults = $db->runQuery($qryStmt);                           
                    $db->displayRecords($qryResults);
                    $qryResults->free();            
                }
                else
                {                       
                    $qryStmt = "SELECT dogName AS Name, 
                                dogBreed AS Breed, dogAge AS Age, dogGender AS Gender,
                                dogFixed AS Fixed, ownerName AS Owner,
                                phoneNumber AS 'Phone#', emailAddress AS Email
                                FROM Dog D JOIN Owner O ON D.ownerID = O.ownerID ORDER BY Name DESC;";
                    
                    $qryResults = $db->runQuery($qryStmt);                           
                    $db->displayRecords($qryResults);
                    $qryResults->free();                          
                }
            }
            elseif(isset($_REQUEST["Age"]))
            {
                if($_COOKIE["sortCounter"] % 2 == 0)
                {
                    $qryStmt = "SELECT dogName AS Name, 
                                dogBreed AS Breed, dogAge AS Age, dogGender AS Gender,
                                dogFixed AS Fixed, ownerName AS Owner,
                                phoneNumber AS 'Phone#', emailAddress AS Email
                                FROM Dog D JOIN Owner O ON D.ownerID = O.ownerID ORDER BY Age;";
                    
                    $qryResults = $db->runQuery($qryStmt);                           
                    $db->displayRecords($qryResults);
                    $qryResults->free();            
                }
                else
                {
                    $qryStmt = "SELECT dogName AS Name, 
                                dogBreed AS Breed, dogAge AS Age, dogGender AS Gender,
                                dogFixed AS Fixed, ownerName AS Owner,
                                phoneNumber AS 'Phone#', emailAddress AS Email
                                FROM Dog D JOIN Owner O ON D.ownerID = O.ownerID ORDER BY Age DESC;";
                    
                    $qryResults = $db->runQuery($qryStmt);                           
                    $db->displayRecords($qryResults);
                    $qryResults->free();
                }
            }
            else
            {            
                $qryStmt = "SELECT dogName AS Name, 
                           dogBreed AS Breed, dogAge AS Age, dogGender AS Gender,
                           dogFixed AS Fixed, ownerName AS Owner,
                           phoneNumber AS 'Phone#', emailAddress AS Email
                           FROM Dog D JOIN Owner O ON D.ownerID = O.ownerID;";
            
                $qryResults = $db->runQuery($qryStmt);            
                $db->displayRecords($qryResults);
                $qryResults->free();
            }
            ?>
                
            </div>
        </body>
    </html>
    <?php
        function __autoload($className)
        {
            $fileName = "../cst212cweb280a2/$className.class.php";
            if(file_exists($fileName))
            {
                require_once ($fileName);                
            }
        }        
?>