<!DOCTYPE html>
<html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <link type="text/css" href="tableStyle.css" rel="stylesheet">
            <title>Results Posted for Owner!</title>
        </head>
        <body>
            <h1>Posted Results</h1>
            <ul class="menu">
                <li><a href="index.php" class="active"><span>Home</span></a></li>
                <li><a href="GenerateReport.php"><span>Generate Report</span></a></li>
                <li><a href="Registration.php"><span>Registration</span></a></li>
            </ul>
            <?php
            $db = new DbObject();
            
            unset($_POST["addOwner"]);
            if(isset($_POST))
            {
                $db->insert($_POST, "Owner");
            }
            else
            {
                echo ":( Your results have not been posted!";
            }       
            ?>
        </body>
    </html>
<?php
        function __autoload($className)
        {
            $fileName = "../cst212cweb280a2/$className.class.php";
            if(file_exists($fileName))
            {
                require_once ($fileName);      
            }
        }        
?>