<!DOCTYPE html>
<html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <link type="text/css" href="tableStyle.css" rel="stylesheet">
            <title>Add a Dog</title>        
        </head>
        <body>
            <ul class="menu">
                <li><a href="index.php" class="active"><span>Home</span></a></li>
                <li><a href="GenerateReport.php"><span>Generate Report</span></a></li>
                <li><a href="Registration.php"><span>Registration</span></a></li>
            </ul>
            <?php
            $db = new DbObject();
            $form = new ImprovedGenerateForm();
            $qryResults = $db->select("ownerID AS ID, ownerName AS Name", 
                                      "Owner" ,"" ,"ownerName", "");
            if($qryResults)
            {
                $ownerSel = $db->createArray($qryResults);
                $qryResults->free();
            }
            
            $form->startForm("DogForm", "Add a Dog!", "",
                             "PostResultsDog.php" , "POST", "");
            $form->select("OwnerID", "Owners", $ownerSel);
            $form->textbox("dogName", "Dog Name", 45);            
            $form->textbox("dogBreed", "Dog Breed", 45);            
            $form->textbox("dogAge", "Dog Age", 2);
            $form->textbox("dogGender", "Dog Gender", 1);
            $form->checkbox("dogFixed", "Fixed?");
            $form->endForm("addDog", "Submit");            
            ?>
        </body>
    </html>
<?php
        function __autoload($className)
        {
            $fileName = "../cst212cweb280a2/$className.class.php";
            if(file_exists($fileName))
            {
                require_once ($fileName);      
            }
        }        
?>