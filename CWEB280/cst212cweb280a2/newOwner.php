<!DOCTYPE html>
<html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <link type="text/css" href="tableStyle.css" rel="stylesheet">
            <title>Add a New Owner</title>
        </head>
        <body>
            <ul class="menu">
                <li><a href="index.php" class="active"><span>Home</span></a></li>
                <li><a href="GenerateReport.php"><span>Generate Report</span></a></li>
                <li><a href="Registration.php"><span>Registration</span></a></li>
            </ul>
            <?php
                $db = new DbObject();
                $form = new ImprovedGenerateForm();

                $form->startForm("OwnerForm", "Add a Owner!", "",
                                 "PostResultsOwner.php" , "POST", "");
                $form->textbox("ownerName", "Owner Name", 45);            
                $form->textbox("phoneNumber", "Phone #", 15);
                $form->textbox("emailAddress", "Email", 45);                                
                $form->endForm("addOwner", "Submit");            
            ?>
        </body>
    </html>
<?php
        function __autoload($className)
        {
            $fileName = "../cst212cweb280a2/$className.class.php";
            if(file_exists($fileName))
            {
                require_once ($fileName);      
            }
        }        
?>
