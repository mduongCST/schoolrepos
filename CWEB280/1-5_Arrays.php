<?php 
    //logic block
    //used for processing with no output to the browser

    //ARRAYS in PHP
    //PHP can define/create arrays in multiple ways
    //PHP can create 2 types of arrays numeric and associative(arays that use key value pairs)

    //Numeric Arrays
    //Method 1: using the array function

$instructors = array("Bryce Barrie", "Ernesto Basoalto", "Ben Benson", "Kevin Bryant");

    //Method 2: Define a variable with array notation and index

    $students[0]= "Fernando 'Fernanbro' Alister";
    $students[1]= "Mitchell 'O Dizzle' Duong";
    $students[2]= "Maxwell 'to the MAX' Garies";
    $students[3]= "Victoria 'Vic' Hall";
    $students[4]= "Nathan 'NateDogg' Hendrickson";

    //Method 3: For numeric ascending arrays there is no need to specify the index

    $pets[] = "Anaconda";
    $pets[] = "Bats";
    $pets[] = "Cats";
    $pets[] = "Dogs";

    //Associative Arrays
    //Method 1: Using the array function
    $provinces = array("AB"=>"Alberta","BC"=>"British Columbia");

    //Method 2: Using array notation with a string key index
    $provinces["MB"] = "Manitoba";
    $provinces["SK"] = "Saskatchewan";

    //2d arrays example from chapter 5 section creating arrays
    $customers = array(); //empty array
    $customers[] = array("Jason","james@example.com","614-999-9999");
    $customers[] = array("Jesse James", "jesse@example.net", "818-999-9999");
    $customers[] = array("Donald Duck", "donald@exa mple.org", "212-999-9999");    
?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>1-5 Arrays</title>
    </head>
    <body>
        <h1>1-5 Arrays</h1>

        <div>
            <h2>Instructors</h2>
        <?php
        // primarily nested output to the browser
        // out put instructors using array notation $instructors[0], ....
        echo "<p>$instructors[0], $instructors[1], $instructors[2]</p>";
        ?>
        </div>

        <div>
            <h2>Students</h2>
            <?php
            // mini exercise use a for loop and the count function to get the length of the array
            for($num = 0; $num < count($students); $num++ )
            {
                echo "<p> $students[$num]</p>";
            }
            ?>
        </div>


        <div>
            <h2>Pets</h2>
            <?php
            // mini exercise use a while loop with the functions current and next
            // hint: see chapter 5 of text book section traversing arrays

            while($pet = current($pets))
            {
                echo "<p>$pet</p>";
                next($pets);  //increments the array pointer  
            }
            ?>
        </div>


        <div>
            <h2>Province Names</h2>
            <?php
            // mini exercise output just the province names using a foreach loop
            foreach ($provinces as $value)
            {
                echo "<p> $value </p>";
            }

            ?>
        </div>


        <div>
            <h2>Province Names and Abbreviations</h2>
            <?php
            foreach ($provinces as $key => $value) 
            {
                echo "<p> $key, $value";
            }
            ?>
        </div>


        <div>
            <h2>Customers - with for loop</h2>
            <?php
            foreach ($customers AS $customerinfo) 
            {
                echo "<ul>";

                foreach ($customerinfo as $info) 
                {
                    echo "<li>$info</li>";
                }

                echo "</ul>";

            }

            ?>
        </div>


        <div>
            <h2>Customers - using vprintf</h2>
            <?php
            foreach ($customers AS $customerinfo) 
            {
                // %s is a placeholder for strings
                vprintf("<ul><li>Name: %s</li><li>Email: %s</li><li>Phone: %s</li></ul>", $customerinfo);
            }
            ?>
        </div>

    </body>
</html>
