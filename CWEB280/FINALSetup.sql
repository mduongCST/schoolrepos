DROP TABLE JobApplications;
DROP TABLE TutorRequests;
DROP TABLE Users;
DROP TABLE Courses;


CREATE TABLE Users(
userID VARCHAR(6) NOT NULL PRIMARY KEY,
password VARCHAR(128) NOT NULL,
userType INT(1) NOT NULL DEFAULT 1,
userPhone VARCHAR(15),
userEmail VARCHAR(50) NOT NULL,
lastName VARCHAR(30) NOT NULL,
firstName VARCHAR(30)NOT NULL

) ENGINE = InnoDB;

CREATE TABLE Courses(
courseID VARCHAR(8) NOT NULL PRIMARY KEY,
courseDesc VARCHAR(50) NOT NULL
) ENGINE = InnoDB;

CREATE TABLE TutorRequests(
requestID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
userID VARCHAR(6) NOT NULL,
courseID VARCHAR(8) NOT NULL,
requestDate DATETIME NOT NULL,
pending BOOLEAN DEFAULT true,

FOREIGN KEY (userID) REFERENCES Users (userID),
FOREIGN KEY (courseID) REFERENCES Courses (courseID)
) ENGINE = InnoDB;

CREATE TABLE JobApplications(
applicationID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
requestID INT NOT NULL,
applicationDate DATETIME NOT NULL,

FOREIGN KEY (requestID) REFERENCES TutorRequests (requestID)
) ENGINE = InnoDB;

SELECT * FROM Users;
SELECT * FROM Courses;
SELECT * FROM TutorRequests;


INSERT INTO Courses (courseID, courseDesc) VALUES('CNET 184', 'Data Communications and Networking 1');
INSERT INTO Courses (courseID, courseDesc) VALUES('COAP 173', 'Data and Document Management');
INSERT INTO Courses (courseID, courseDesc) VALUES('COOS 181', 'Operating Systems');
INSERT INTO Courses (courseID, courseDesc) VALUES('COSC 180', 'Introduction to Programming');
INSERT INTO Courses (courseID, courseDesc) VALUES('CDBM 190', 'Introduction to Database Management');
INSERT INTO Courses (courseID, courseDesc) VALUES('CNET 190', 'Network Administration 1');
INSERT INTO Courses (courseID, courseDesc) VALUES('COSC 190', 'Intermediate Programming');
INSERT INTO Courses (courseID, courseDesc) VALUES('CWEB 190', 'Internet Programming/Web Applications 1');
INSERT INTO Courses (courseID, courseDesc) VALUES('COSC 195', 'Mobile Application Programming');
INSERT INTO Courses (courseID, courseDesc) VALUES('CWEB 195', 'Web Site Interface Design');

INSERT INTO JobApplications (requestID, applicationDate) VALUES(5, '2013-12-13 12:39:00');
INSERT INTO JobApplications (requestID, applicationDate) VALUES(7, '2013-12-13 12:21:00');
INSERT INTO JobApplications (requestID, applicationDate) VALUES(10, '2013-12-13 12:42:00');

UPDATE TutorRequests SET pending = false WHERE requestID = 5 OR requestID = 7 OR requestID = 10;