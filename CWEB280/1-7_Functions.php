<?php
// easy to define new functions in php

function outputHello()
{
    echo "<p>Hello World</p>";
}

function defaults($name, $age, $motto="YOLO SWAG!")
{
    echo "<p>Hello, my name is $name. I am $age years old. I live by the motto '$motto' </p>";
}

function getString($name2, $age2)
{
    return "<p>Hello, my name is $name2. I am $age2 years old.</p>";
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>1-7 Functions</title>
    </head>
    <body>
        <h1>1-7 Functions - Chapter 4 in textbook</h1>
        <?php
            outputHello();
            defaults("Ernesto", 40);
            defaults("Shane", 50, "Hit you with the dropkick Marty Janetty");
            $description = getString("Mitchell", 19);
            echo $description;
        ?>
    </body>
</html>
