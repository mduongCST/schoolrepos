<?php

/**
 * Description of GenerateForm
 * this class will be used to generate HTML forms using php
 * @author ins226
 */
class GenerateForm {
    // these are constants for use with sprintf and printf i.e. they are format strings
    const FORMSTART = '<form id="%s" action="%s" method="%s" %s><fieldset %s><legend>%s</legend>';
    const FORMEND = '<div %s><input type="submit" value="%s" name="%s" /><input type="reset" value="%s" /></div></fieldset></form>';
    //
    const INPUTWRAP = <<<EOT

<div>
            <label>%s</label>
            %s
</div>
EOT;
// the above line indicates the end of the string - nothing else can be on that line - no spaces or tabs


    /*
     * startForm
     * generates the beginning of the generated form using the passed in params
     * name - the id of the form element - used by browser only
     * display - the legend of the fieldset
     * formExtras - any other attributes we may need for the form ex. enctype="multipart/form-data"
     * action - the page to post to default: post to self
     * method - the way to send data to the server default: POST
     * fieldsetExtras - any other attributes we may need for the fieldset
     */
    public function startForm($name, $display, $formExtras="", $action="#", $method="POST", $fieldsetExtras="")
    {
        printf(self::FORMSTART,$name,$action,$method,$formExtras,$fieldsetExtras,$display);
    }
    
    //minicise create endForm function using - $nameSubmit, $displaySubmit, $displayReset="Reset", $divExtras=""
    /*
     * endForm
     * generates the closing form tags and buttons
     * nameSubmit - the name of the submit button sent to the server
     * displaySubmit - the value attribute of the submit button - displayed to the user
     * displayReset - the value attribute of the reset button - displayed to the user
     * divExtras - any other attributes that may be needed for the div button container
     */
    public function endForm($nameSubmit, $displaySubmit, $displayReset="Reset", $divExtras="")
    {
        printf(self::FORMEND, $divExtras, $displaySubmit, $nameSubmit,$displayReset);
    }
    
   /*
     * textbox
     * generates a wrapped text input
     * name- the name send to the server and the id of the element
     * display - the label of the field
     * value - the default value populated in the textarea
     * extras - any other attributes that may be required
     */	    
    public function textbox($name, $display, $max="50", $value="", $extras="")
    {
        // first use form string to generate input tag
        $input = sprintf('<input type="text" name="%s" id="%s" maxlength="%d" value="%s" %s/>',
                $name,$name,$max,$value,$extras);
        printf(self::INPUTWRAP, $display, $input );        
    }
    
}



// when defing classes in  their own file we do not need the closing php tag
// this may seem irrational but this will prevent issues when using php to generate html attachments