<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Select a supplier to update</title>
    </head>
    <body>
        <h1>Update a Supplier</h1>
        
        <?php

// Connect to the database
$db = new DbObject();

// Query the database
$qryResults = $db->select( "SupplierID, CompanyName", "Suppliers",
        "", "CompanyName" );

// Build the option list
$supplierOptionList = DbObject::createArray( $qryResults );

// We're done with the results -- free them
$qryResults->free();

// Create the supplier selection form
$form = new GenerateForm();
$form->startForm( "supplierForm", "Pick a supplier to update", "",
        "6-DisplaySupplier.php", "POST" );
$form->select( "SupplierID", "Supplier", $supplierOptionList );
$form->endForm( "subSupplier", "Submit" );

        ?>
    </body>
</html>

<?php

function __autoload( $className )
{
    $fileName = $className . ".class.php";
    if (file_exists( "../classes/$fileName" ) ) {
        require_once( "../classes/$fileName" );        
    } else {
        require_once( "../../private-classes/$fileName" );
    }
}

?>