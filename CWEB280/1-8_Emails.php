<?php
    //php uses the mail method to send emails using a specified server in the config.ini file
    function sendMail($to, $subject="Email from PHP", $message="Message from PHP!!", 
            $senderName="Mitch", $senderEmail="ins226@cst.siast.sk.ca")
    {
        $header = "From:$senderName<$senderEmail>";
        return mail($to, $subject, $message, $header);
    }
    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>1-8 Emails</title>
    </head>
    <body>
        <h1>1-8_Emails - Chapter 16 - cstmail server</h1>
        <?php
        if(sendMail("cst212@cst.siast.sk.ca"))
        {
            echo "Email Sent!!!<br/>";
        }
        else
        {
            echo "Email failed :( <br />";
        }
        ?>
    </body>
</html>
