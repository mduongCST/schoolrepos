<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<?php
//Regular Expressions in PHP - the nuances
//delemiters - all php RE must start with a delmiter
//example with forward slash -  /^bob\w*/i
//example with hash          -   #^bob\w*#i
//example with ~             -   ~^bob\w*~i

//Meta Characters - ca.php.net/manual/en/regexp.reference.meta.php
//[] - character class and it lets you specify a range of characters (alot like a bunch of or (|) )
//() grouping - specify sub groups within the patternMIM
// * quantifier - specifies that 0 or more of the pattern must exist
// + quantifier - specifies that 1 or more of the pattern must exist
//{ - start min/max quantifier
//}- end min/max quantifier
//
//Part of a pattern that is in square brackets is called a "character class". In a character class the only meta-characters are:
//\ general escape character
//^ negate the class, but only if the first character
//- indicates character range

//Escape sequences - ca.php.net/manual/en/regexp.reference.escape.php
// \d - digits same as [0-9]
// \s - white space same as [ \t\r\n ]
// \w - characters same as [a-z-_]
// . - anything except newline


//methods in PHP
//preg_match
//preg_replace

function checkURL($websiteURL)
{
    $pattern = "#^(http(s)?)://([a-z0-9\.]+\.[a-z]{2,4})(/[\w/-_+%]+)#i";
    $message = "($websiteURL) is not valid a URL <br />";
    
    if(preg_match($pattern, $websiteURL))
    {
        $message = "Yes ($websiteURL) is a valid URL - browse on!!<br />";
    }
    return $message;
    
}

//substitute found patterns in replacement strings

function rearrangeDate($dateString)
{
    //this method will take a string like : "September 03, 2013"
    //and return the string like : "2013-September-03"
    
    $pattern = "/(\w+) (\d+), (\d{2,4})/";
    $replacement = "$3-$1-$2"; // the key char $ tells preg_replace to substitute
                               // the found pattern, acts like a place holder
    
    return preg_replace($pattern, $replacement, $dateString);
    
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>1-6 Strings and Regular Expressions</title>
    </head>
    <body>
        <h1>1-6 Strings and Regular Expressions</h1>
        <h2>Check URL: </h2>
        <?php
        //using a valid URL
            echo checkURL("http://php.ca/manual/en/function.preg-match.php");
            //using an invalid URL
            echo checkURL("pooplolo swagsititiy CoDlol.com");
        ?>
        
        <h2>Re-arrange the date from "December 29, 1993"</h2>
        <?php
            echo rearrangeDate("December 29, 1993") . "<br />";
        ?>
    </body>
</html>
