<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>1-2 Control Structures</title>
    </head>
    <body>
        <h1>1-2 Control Structures</h1>
        
        <div>
            <h1>Conditional Statements</h1>               
               
        <?php
        
            // random number from 1-10
            $rnd = rand(1,10);
            
            if($rnd <= 5)
            {
                echo "the random number: $rnd is less than or equal to 5";
            }
            elseif($rnd <= 7)
            {
                echo "the random number: $rnd is less than or equal to 7";
                
            }
            else
            {
                echo "the random number: $rnd is less than or equal to 10";                
            }
        ?>
            <h2>Mini Exercise</h2>
            use conditional statements to display whether the random number is
            odd or even. If even make the font color green. Else the color red.
            
            <?php
            
            
            
            if($rnd %2 == 0)
            {
                echo "<p style='color:green;'>the random number: $rnd is even</p>";                
            }
            else 
            {
            ?>
                echo "<p style='color:red;'>the random number: 
                    <?php echo $rnd; ?> is odd</p>
                <?php 
                //Close else statement fromt the last block
            }
            ?>

            
            
            
                
        </div>
        
        <div>
            <h1>Switch Statements</h1>               
               
            <p>
        <?php
            //$category = "News";
            
            //using $_GET to parse the query in the URL for the value of "cat"
            $category = isset($_GET["cat"]) ? $_GET["cat"] : "";
        
            switch ($category)
            {
                case "News":
                    echo "What's happening around the world...";
                    break;
                case "Sports":
                    echo "The latest sports highlights...";
                    break;
                case "Weather":
                    echo "Up to date weekly forecasts...";
                    break;
                default :
                    echo "NO matching category found";
                    break;                
            }
        ?>
            </p>
            <h2>Mini exercise</h2>
            use a switch statement to display: 
                random number less than or equal to 3
                random number less than or equal to 6 but greater than 3
                random number less than 10 but greater than 6
                
                <br>
                <h3>Result</h3>
                <?php
                
                switch($rnd)
                {
                    case $rnd <= 3:
                        echo "$rnd is less than or equal to 3";
                        break;
                    case $rnd <= 6 && $rnd > 3:
                        echo "$rnd is less than or equal to 6 but greater than 3";
                        break;
                    case $rnd <= 10 && $rnd > 6:
                        echo "$rnd is less than or equal to 10 but greater than 6";
                        break;
                    default :
                        echo "$rnd";
                        break;
                    
                }
                
                
                
                
                ?>
                
        </div>
        
        <div>
            <h1>While Loops</h1>               
            <ol>
        <?php
            $count = 1;
            $sum = 0;
            
            //loop while count is less than the random number
            while ($count <= $rnd)
            {
                $sum += $count;
                echo "<li>$sum</li>";
                $count++;
            }
        ?></ol>
        </div>
        
        <div>
            <h1>For Loop</h1>               
            mini exercise do the same as above using for loop
            
            <ol>
        <?php
            $sum = 0;
            for($count = 1; $count <= $rnd; $count++)
            {
                $sum += $count;
                echo "<li>$sum</li>";
            }
        ?></ol>
        </div>
    </body>
</html>
