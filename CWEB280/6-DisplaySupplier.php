<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Update a Supplier</title>
    </head>
    <body>
        <h1>Update Supplier Information</h1>
        
        <?php

// Create the supplier information update form
$form = new GenerateForm();
$form->startForm( "supplierInfoForm",
        "Supplier being updated: ", "",
        "6-UpdateSupplier.php", "POST" );
$form->textbox( "CompanyName", "Company name",  40 );
$form->textbox( "ContactName", "Contact name",  30 );
$form->textbox( "ContactTitle", "Contact title",  40 );
$form->textbox( "Address", "Address",  60 );
$form->textbox( "City", "City",  15 );
$form->textbox( "Region", "Region",  15 );
$form->textbox( "PostalCode", "Postal code",  10 );
$form->textbox( "Country", "Country",  15 );
$form->textbox( "Phone", "Phone",  24 );
$form->textbox( "Fax", "Fax",  24 );
$form->textbox( "HomePage", "Home page", 50 );
$form->endForm( "subSupplier", "Update supplier" );

        ?>
    </body>
</html>

<?php

function __autoload( $className )
{
    $fileName = $className . ".class.php";
    if (file_exists( "../classes/$fileName" ) ) {
        require_once( "../classes/$fileName" );        
    } else {
        require_once( "../../private-classes/$fileName" );
    }
}

?>