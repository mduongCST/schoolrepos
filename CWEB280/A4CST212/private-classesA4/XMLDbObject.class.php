<?php
require_once '../private-classes/DbObject.class.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of XMLDbObject
 *
 * @author CST212
 */
class XMLDbObject extends DBobject
{
    private $XSLT;
    
    public function __construct ($XSLT="", $server = "kelcstu05.cst.siast.sk.ca",
            $user = "CST212", $password = "RCFFXT", $schema = "CST212")
    {
        $this->XSLT = $XSLT;
        parent::__construct($server, $user, $password, $schema);

    }
    
    function selectToXML($rootName, $colList, $tableList,
            $condition="", $sort="", $other="")
    {
        $queryResult = $this->select($colList, $tableList,
                $condition, $sort, $other);
        //we want to the table name as the child node name
        //but we need to clean up the name first
        $childName = explode(" ", $tableList);        
        $childName = strtolower($childName[0]);        
        $childName = rtrim($childName, "s");
        //call help function to loop through query and read into 2D array
        return $this->getXml($queryResult, $rootName, $childName);
    }
    
    function getXml($queryResult, $rootName, $childName="row")            
    {
        //init 2D array
        $infoData = array();
        
        while($rowData = $queryResult->fetch_assoc())
        {
            $infoData[] = $rowData; // add current rowdata to 2D array                        
        }
        
        //call the recursive function to add items to simplexml node        
        return $this->convertToXML(null, $infoData, $rootName, $childName);
    }
    
    function convertToXML($obXml, $infoData, $rootName, $childName="row")
    {
        //handle null obXml - create new node
        if($obXml == NULL)
        {
            $obXml = new SimpleXMLElement("<?xml version='1.0' encoding='utf-8' ?><$rootName />");
        }
        //TODO handle null obXml create new node
        foreach($infoData as $key => $value)
        {
            if(is_array($value))
            {
                $currNode = $obXml->addChild($childName);
                $this->convertToXML($currNode, $value, $childName);                
            }
            else
            {
                //just add the element to the xml doc
                $obXml->addChild($key, htmlspecialchars($value));
            }
        }
        return $obXml;
    }
    
}

