<?php

/*
 * The DbObject class represents a connection to a MySQL server.
 * With objects of this class, we'll be able to create and execute
 * query statements. This is a convenience class, wrapped around
 * the mysqli object.
 */

class DbObject 
{
    /**
     * The database connections
     */
    
    private $dbConnect;
    
    /**
     * Purpose: To create a connection to a MySQL server and
     * open a database on that server.
     * 
     * @param string $server - name of the MySQL server
     * @param string $user - name of the MySQL user
     * @param string $password - user's password
     * @param string $schema - name of the schema to use
     */
    public function __construct($server="kelcstu05.cst.siast.sk.ca", 
            $user="CST212", $password="RCFFXT", $schema="CST212") 
    {
    //Create a mysqli object, and assign it to the internal attribute
    
        $this->dbConnect = new mysqli($server, $user, $password, $schema);
    //If the connection failed
        if($this->dbConnect->connect_errno)
        {
          //Display an error message
          echo "<p>Failed to connect! " . $this->dbConnect->connect_error . "</p>\n";
          //Exit
          exit();
        }
        else 
        {
        }        
    }
    
    /**
     * Purpose: Perform a SELECT query on the database
     * @param type $colList List of columns to be selected
     * @param type $tableList List of tables to select from
     * @param type $condition Optional SQL condition to select with
     * @param type $sort Optional SQL sort clause to apply
     * @param type $other Optional any other SQL clauses to apply
     * @return mysqli_result The result of the SELECT query, or FALSE if
     *   the query fails
     */
    public function select($colList, $tableList, $condition="", $sort="", $other="")
    {
        //Create the basic SELECT statement
        $qryStmt = "SELECT $colList FROM $tableList";
        
        //If a condition is specified, add it to the query
        if( $condition != "")
        {
            $qryStmt .= " WHERE $condition";
        }
        //If a condition is specified, add it to the query
        if( $sort != "")
        {
            $qryStmt .= " ORDER BY $sort";
        }
        //Add any other SQL clauses if they've been specified
        if( $other != "")
        {
            $qryStmt .= " $other";
        }
        
        return $this->runQuery($qryStmt);
    }
    
    /**
     * Purpose: Display the results of a database query
     * @param type $qryResults Results of a previous database query
     */
    static public function displayRecords($qryResults)
    {
        // Display the opening table tag
        echo "<table>\n";
        // Display a table row opening tag
        echo "    <tr>";
        // LOOP for all query result columns
        foreach ($qryResults->fetch_fields() as $fieldInfo)
        {
            // Display the column name within a table header tag
            echo"<th>{$fieldInfo->name}</th>";
        }            
        // Display a table row closing tag 
        echo "</tr>\n";
        
        // LOOP for all the query rows returned
        while($row = $qryResults->fetch_row())
        {
            // Display a table row opening tag
            echo "    <tr>";
            // LOOP for all the query result columns
            for($i = 0; $i < $qryResults->field_count; $i++)
            {
                //Display the value of this query result row and column
                echo"<td>" . htmlspecialchars($row[$i]) . "</td>";
                //echo "<td>{$row[$i]}</td>";
            }
            // Display a table row closing tag
            echo "</tr>\n";
        }
        // Display the closing table tag
        echo "</table>\n";
    }
    
    
    /**
     * Purpose: This routine will run the query that is provided by the query
     *          string that is passed in. If the query fails, exit ungracefully.
     * @param string $qryString The SQL query string that is to be run
     * @return mysqli_result The result of the query
     */
    public function runQuery($qryString)
    {
        // Execute the query
        $qryResult = $this->dbConnect->query($qryString);
        // IF the query failed
        if($qryResult)
        {
            // Return the result of the query
            return $qryResult;
        }
        // ELSE
        else
        {
            // Print an error message, then exit
            echo "<p>Query $qryString couldn't execute</p>\n";
            exit();
        }

    }
    
    /**Purpose: Creates an associative array to be used with the GenerateForm
     *  class' methods that populate lists 
     * @param type $qryResults The query result record set. The result should
     * consist two columns: the first column will contain an ID, and the second
     * will contain the corresponding text
     * @return array An Associative array, where the index comes from the qryResults'
     * first column, and the array value comes from the qryResults' second column.
     */
    static public function createArray($qryResults)
    {
        //Create an empty result array
        $result = array();
        
        //Loop through all rows in the result set
        while($row = $qryResults->fetch_row())
        {
            //Set an entry in the result array with the index as the first
            //column in the result set, and the value as the second column
            $result[$row[0]] = $row[1];
        }
        
        //reutrn the result array
        return $result;
    }
    
    /**
     * Purpose: This method will add a new record to the specified table
     * @param array $newRecord An associative array of the field names
     *      (the array index) and the values to be inserted (the array values)
     * @param string $tableName The name of the table to add the record to
     * @return int The number of rows inserted
     */
    public function insert($newRecord, $tableName)
    {  
        //Construct the field name and value lists
        $fieldList = "";
        $valueList = "";
        foreach( $newRecord as $field=>$value)
        {
            $fieldList .= $field . ", ";
            //Don't forget to escape the user-supplied value, in order
            // to prevent an SQL injection attack!
            $valueList .= "'" . $this->dbConnect->real_escape_string($value) . "', ";
        }
        // We've finished adding all the field names and values to their
        // respective lists, so delete the final comma and space.
        
        $fieldList = rtrim($fieldList, ", ");
        $valueList = rtrim($valueList, ", ");
        

        //Perform the insertion
        $insStatement = "INSERT INTO $tableName ($fieldList) VALUES ($valueList);";
        echo "<p>Insert statement is: " . $insStatement . "</p>\n";
        
        $this->runQuery($insStatement);
        //Return the number of affected rows
        return $this->dbConnect->affected_rows;
        //return $this->runQuery($insStatement);
        
    }
    
    /** 
     * Purpose: Updates a record in the specified table with the values
     *  passed in.
     * Assumption: There is a value for that primary key in the values array,
     *  and we don't want to update that.
     * 
     * @param array $newRecord $values An associative array of field names and the values
     *  that those fields are to be updated to
     * 
     * @param string $tableName The name of the table to update
     * @param string $primaryKey The name of the primary key
     * @return int The number of rows updated
     */
    public function update($newRecord, $tableName, $primaryKey)
    {
        // Minicize: Write the update method. You'll want to loop through
        // all the entries in the associative array, and create a list of
        // fields and their values to update (use the foreach loop!).
        // DON'T FORGET TO SANITIZE YOUR DATABASE INPUTS. Be sure NOT to
        // include the primary key field, because we won't be updating that

        //Construct the field name and value lists
        $updateStatement = "UPDATE $tableName SET ";        
        foreach( $newRecord as $fieldName=>$fieldValue)
        {         
            //If this isn't the primary key
            if($fieldName != $primaryKey)
            {
                $updateStatement .= $fieldName . "=' " . 
                        $this->dbConnect->real_escape_string($fieldValue) . 
                        "', ";
            }                                    
        }
        
        //Oops, we added in one too many commas -- remove it
        $updateStatement = rtrim($updateStatement, ", ");
        
        //Perform the update
        $updateStatement .= " WHERE " . $primaryKey . "='" . 
                $fieldName[$primaryKey] . "';";
        
        //Debugging: print out the query statement
        echo "<p>Update statement: $updateStatement</p>\n";
        
        //Run the query
        $this->runQuery($updateStatement);
        //Return the number of affected rows
        return $this->dbConnect->affected_rows;
        //return $this->runQuery($updateStatement);
        
    }
    
    /**
     * Purpose: Prepare an SQL statement for execution by our database
     *  connection
     * @param type $query The query to be prepared
     * @return msqli_stmt The prepared statement, or FALSE if an error
     *  occured.
     */
    public function prepare($query)
    {
        return $this->dbConnect->prepare($query);
    }
    
    /**
     * Purpose: Close the database connection
     * The destructor gets called when the object goes out of scope
     * (function terminates, program ends). Rather than having a seperate
     * close method. (which might be a good idea anyways, because then we can
     * close early if we want), we'll just close the connection here.
     */
    public function __destruct() 
        {
            $this->dbConnect->close();
        }
        
        
        /**
         * this function gets the last inserted identity / autoincrement value 
         * @return integer
         */
        public function getInsertID()
        {
        return $this->dbConnect->insert_id;
        }     
}

?>
