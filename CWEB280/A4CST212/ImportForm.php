<?php
require_once 'XML.include.php';
$rootNode = null;
$db = new DbObject();

//Set variables for SQL injection
$title;
$link;
$description;
$language;
$itemTitle;
$itemchanLink;
$itemDescription;
$itemAuthor;
$itemPubDate;
$stmtItem;
$stmtChan;
global $channelID;

/**
 * This will parse the XML file and and set the global variables to the
 * attributes of the channel.
 * @global String $title Name of the Channel
 * @global String $link Link of the Channel
 * @global String $description Description of the Channel
 * @global String $language Language the Channel is in
 * @param String $chanNode The child of the rootNode
 */
function parseChannel($chanNode)
{
    global $title,$link,$description,$language;
    
    //set each global variable to the corresponding xml file value    
    $title = (string) $chanNode->title;
    $link = (string) $chanNode->link;
    $description = (string) $chanNode->description;
    $language = (string) $chanNode->language;
}

/**
 * This function will go through each item in a channel and set the global variables
 * to the attributes of the Item.
 * @global String $title title of the Channel
 * @global String $itemTitle title of the Item
 * @global String $itemchanLink Link to the article 
 * @global String $itemDescription Description of the article
 * @global String $itemAuthor Author of the article
 * @global String $itemPubDate The date the article was published
 * @global String $stmtItem the prepared statement for the item's SQL injection
 * @param node $chanNode child node of the rootNode
 */
function processItems($chanNode)
{
    global $title, $itemTitle, $itemchanLink, $itemDescription, $itemAuthor,
            $itemPubDate, $stmtItem; 
    
    //For each channel's item set as itemNode
    foreach($chanNode->item as $itemNode)
    {
        //Set the variables to the values of the xml file
        $itemTitle = (string)$itemNode->title;
        $itemchanLink = (string)$itemNode->link;
        $itemDescription = (string)$itemNode->description;
        $itemAuthor = (string)$itemNode->author;
        //If there is no author, set it to N/A
        if($itemAuthor === "")
        {
            $itemAuthor = "N/A";
        }
        //Take in the published date as a string and format it
        date_default_timezone_set('America/Regina');
         $date = strtotime($itemNode->pubDate);         
         $itemPubDate = date("Y-m-d H:i:s",$date);
        
         //Execute the insertion statement
        if($stmtItem->execute())
        {
            //If it is succesful state which item was added
            echo "<p>Channel: $title Item: $itemTitle has been added.<p>";
        }
        else
        {
            echo "Item Error: {$stmtItem->error}";
        }
    }
    
    
}

//Prepare statement for entering a channel into the DB
$stmtChan = $db->prepare("INSERT INTO CST212Channel
    (ChannelID, chanTitle, chanLink, chanDescription, Language)
    VALUES(?,?,?,?,?)");

$stmtChan->bind_param("issss", $channelID, $title,
        $link, $description, $language); 

//Prepare statement for entering an item into the DB
$stmtItem = $db->prepare("INSERT INTO CST212Item
    (itemTitle, itemchanLink, 
    itemDescription, itemAuthor, itemPubDate, channelID)
    VALUES(?,?,?,?,?,?)");

$stmtItem->bind_param("sssssi", $itemTitle, $itemchanLink,
        $itemDescription, $itemAuthor, $itemPubDate, $channelID);


?>
<!DOCTYPE html>
<html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <link rel="stylesheet" type="text/css" href="css/cst212style2.css"/>
            <title>Import Form</title>
        </head>
        <body>            
            <a href="index.php">Home</a>
            <a href="ListArticles.php">List The Articles</a>            
            <h1>Import an RSS File</h1>
            <?php
            $form = new ImprovedGenerateForm();
            $form->startForm("FormRSS", "RSS", 'enctype="multipart/form-data"');
            $form->file("FileRSS", "Upload RSS");
            //If a file was submitted                
            $form->endForm("ImportRSS", "Import", "Cancel");                                                        
            if(isset($_REQUEST["ImportRSS"]))
            {                                
                //Check if there was a file uploaded
                if(!is_uploaded_file($_FILES["FileRSS"]["tmp_name"]))
                {
                    echo "<p>ERROR! No file selected!</p>";
                }
                //else Get the rootNode from the file
                elseif (is_uploaded_file($_FILES["FileRSS"]["tmp_name"])) 
                {                                                
                    $rootNode = getRootNode();       
                    //For each child of the rootNode set it as the chanNode
                    foreach($rootNode->children() as $chanNode)
                    {
                        //call the function to parse through the channel
                        parseChannel($chanNode);
                        //Execute the prepared statement for Channel
                        if($stmtChan->execute())
                        {
                            //Get the channelID
                            $channelID = $db->getInsertID();
                            //process the items in the channel
                            processItems($chanNode);
                        }
                        else
                        {
                            //If the insertion failed submit error
                            echo "Error: {$stmtChan->error}";
                        }
                        
                    }                  
                    $stmtChan->close();
                    $stmtItem->close();
                }                    
            }                    
            
            ?>
            
            
        </body>
    </html>
