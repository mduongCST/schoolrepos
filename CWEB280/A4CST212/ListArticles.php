<?php
    require_once 'private-classesA4/ImprovedGenerateForm.class.php';
?>
<!DOCTYPE html>
<html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>List The Articles</title>
            <script type="text/javascript" src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
            <link rel="stylesheet" type="text/css" href="css/cst212style2.css"/>
            <link type="text/css" href="css/tableStyle2.css" rel="stylesheet">
            <script type="text/javascript">
                //If there is an AJAX error output it.
                $(document).ajaxError(function(event, req, settings)
                {
                    $("#msg").append("<li> AJAX Error: " + settings.url + "</li>").show();
                });
             
                /**
                 * This will return the items for the channel selected
                 * @returns {undefined}
                 */
                function getItems()
                {
                    var ids = '';
                    
                    //find select options and loop through them
                    $('#Channel option').each(function()
                    {
                        //The key word this refers to a DOM option element
                        if(this.selected)
                        {
                            ids += ',' + this.value;
                        }
                    });
                    
                    //equivalent to ltrim in other languages
                    ids = ids.substr(1);
                    
                    //This is the element that got clicked
                    var sortCode = $(this).attr('sort');
                                        
                    //jquery ajax call using get action
                    //second param in the $.get function is the url parameters sent in the ajax call
                    //url params are case sensitive - getCities.php is looking for provid and
                    //sort url params
                    $.get('getItems.php',
                            {chanid:ids,sort:sortCode},
                            loadContent);
                }
                
                /**
                 * This function will take in the link of the link that was clicked
                 * and will set the class in order for CSS to know that the link
                 * has been selected
                 * @returns {undefined}
                 */
                function readLink()
                {
                    //Pull the link from the rows attribute
                    var link = $(this).attr("link");
                    //Add the class read to the row
                    $(this).addClass("read");
                    
                    var ids = '';
                    
                    $('#Channel option').each
                    (
                            function()
                            {
                                if(this.selected)
                                    {
                                        ids += ',' + this.value;
                                    }
                            }
                    );
                    //second parma in the $.get function is the url parameters sent
                    //in the ajax call, will send which link was clicked.
                    ids = ids.substr(1);
                    $.get('getItems.php', {chanid:ids, read:link}, loadContent);
                }
                
                /**
                 * This function will take in an XML document and load content
                 * based on which Channels were selected.
                 * @param {type} xml
                 * @returns {undefined}
                 */
                function loadContent(xml)
                {
                    var tbl = $('#content').empty(); //find the tbody and empty out the children
                    
                    var div = tbl.parent().parent().hide(200);
                    
                    //Find each item in the XML
                    $(xml).find('item').each(
                        function(i)
                        {
                            var item = $(this); //convert city node to jquery object
                            var row = $('<tr />').hide();
                            
                            //added table cells with the values from the xml file
                            row.append('<td>' + item.find('channelID').text() + '</td>');
                            row.append('<td>' + item.find('itemTitle').text() + '</td>');
                            row.append('<td>' + '<a href="' + item.find('itemchanLink').text()
                                    + '" target="_blank" onclick="readLink();">' + 'Link' + "</a>" + "</td>");
                            row.attr("link", item.find('itemchanLink').text());
                            if(item.find('read').text() === "read")
                            {
                                item.addClass("read");
                            }
                            row.append('<td>' + item.find('itemAuthor').text() + '</td>');
                            row.append('<td>' + item.find('itemPubDate').text() + '</td>');                            
                            //added the table row to the existing table body
                            row.appendTo(tbl);
                            row.delay(i++*50).fadeIn(100);         
                        });
                        div.show(200);
                }
                
                /**
                 * Populate the multiselect box with the Channels from the DB
                 * @param {type} xml
                 * @returns {undefined}                 
                 */ 
                function populateList(xml)
                {
                    $(xml).find('channel').each(
                        function()
                        {
                            var chan = $(this); //convert provincei node to jquery object
                            var id = chan.find("id").text();
                            var channel = chan.find("channelName").text();
                            
                            //use jquery to find select box options then use javascript to populate
                            var opts =$('#Channel')[0].options;
                            opts[opts.length] = new Option(channel, id);
                        });
                }
                
                //Load the channels into the select box, set an onchange Listener
                //for the channel select box, and when a header is clicked
                //sort
                function init()
                {
                    $($.get('getChannels.php', populateList));
                    $('#Channel').change(getItems);
                    $('th').click(getItems);
                }
                
                 //call function when document is ready
                $(init);
             </script>
             
        </head>
        <body>
            <a href="index.php">Home</a>
            <a href="ImportForm.php">Upload an RSS file!</a>            
            <?php
                $form = new ImprovedGenerateForm();            
                $form->startForm("FormItem", "Save Item to XML");
                //Generate empty select box

                $form->multiselect("Channel", "Select Channel", array());
                $form->endForm("SubmitItem", "Save to XML");
            ?>
            
            </br>
            <div class='CSSTableGenerator'>
            <table>
            <thead>
                <tr>
                    <th sort="4">Channel</th>
                    <th sort="0">Item Title</th>
                    <th sort="1">Link</th>
                    <th sort="2">Author</th>
                    <th sort="3">Publish Date</th>                    
                </tr>
            </thead>
            <tbody id="content"></tbody>
        </table>
        </div>        
            
            
        </body>
    </html>
