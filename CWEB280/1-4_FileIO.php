<?php
$path = "../wide_open/";
$file = NULL;
//$file will be an indicator to let us know a file uploaded.
    function handleUpload()
    {
        //Telling function to use variables as global variables
        global $path, $file;
        $return = "";
        //check to see if file was uploaded properly
        //is_uploaded_file is a security check to see if it is legitimately an uploaded file
        if(isset($_POST["subFile"]) && is_uploaded_file($_FILES["filUpload"]["tmp_name"]))
        {
        //$_FILES is a special 2D associative array for handling posted files
        //the second key is used to refer to various properties of the uploaded file
        //tmp_name = the temporary location where uploaded files get saved by the server
        //type = the "MIME" type of the uploaded file
        //size = the number of bytes of the file
        //name = the name of the file specified by the client browser
        // i.e. the friendly name
            
        //minicise 
        //Check the file size to ensure it is less than 1024 bytes
        //Next check the file type to be "text/plain"
        if( $_FILES["filUpload"]["type"] != "text/plain")
        {
            $return = "File must be a text file - the current file is: " . 
                    $_FILES["filUpload"]["type"];
        }
        elseif ($_FILES["filUpload"]["size"] > 1024)
        {
                $return = "File must be less than 1024 bytes - the current file is (bytes):"
                        . $_FILES["filUpload"]["size"];
        }
        else //process the file
        {
            $file = $path . $_FILES["filUpload"]["name"]; // the final path and name for the upload file
            
            // use another php function to move the file from the temp location to the wide open location
            if(move_uploaded_file($_FILES["filUpload"]["tmp_name"], $file))
            {
                $return = $_FILES["filUpload"]["name"] . " was succesfully uploaded and moved!";
            }
            else
            {
                $return = $_FILES["filUpload"]["name"] . " failed to uploaded and/or move!";                
            }
        }
            return $return;
        }
    }
    
    //Now let's use php to open and read the file and output to the webpage
    //There are several ways to do this - we will show 3 ways
    
    //Method 1: old school open file read line by line and close file - still useful for performance tuning
    function readFileByLine()
    {
        global $file;
        if($file) //Check to see if the file was properly uploaded
        {
            $fileHandler = fopen($file, "r"); // mode r - means READ only starting at the beginning of the file
            while(!feof($fileHandler))// while we are not at the End of File, continue
            {
                echo fgets($fileHandler,1024); //Read the current line up to 1024 bytes
            }
            fclose($fileHandler); //Close the file after you are done with it
        }
    }
    //Method 2: Read file into an array of lines
    function readFileArray()
    {
        global $file;
        if($file) //Check to see if the file was properly uploaded
        {
            $lines = file($file); //The file method rads the file and returns an array of lines
            foreach($lines as $current)
            {
                echo $current;
            }
        }
    }
    //Method 3: read file all at once
    function readFileAll()
    {
        global $file;
        if($file) //Check to see if the file was properly uploaded
        {
            return file_get_contents($file);
        }
    }
    
    //we are going to use fopen to append and to overwrite the uploaded file
    
    function appendFile($newLine)
    {
        //this function will take the submitted textarea and append it to the file
        global $file;
        if($file)
        {
            $fileHandler = fopen($file, "a"); 
            //mode a - to open the file in append mode
            // and start the file pointer at the eof
            fwrite($fileHandler, $newLine);
            fclose($fileHandler); //we made sure to close the file - it saves it too.
        }
    
    }
        function overwriteFile($newLine)
    {
        //this function will take the submitted textarea and append it to the file
        global $file;
        if($file)
        {
            $fileHandler = fopen($file, "w"); 
            //mode w - to open the file at the start 
            //of the file and over write the content
            fwrite($fileHandler, $newLine);
           
            fclose($fileHandler); //we made sure to close the file - it saves it too.
        }
    
    }
    
        
        
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>1-4_FileIO Chapter 10</title>
        <style>
            fieldset {width: 400px; border: 2px solid blue;}
            legend {padding: 5px 20px; border: 2px solid blue;}
            fieldset div {margin: 10px 5px; padding: 3px 0; border-bottom: 1px solid blue;}
            label {width: 150px; display: inline-block;}
            select, input[type='text'] {width: 200px;}
        </style>
        
    </head>
    <body>
        <!--If it uploads a file it HAS TO BE POST - also more secure | Always have enctype="multipart/form-data"-->
        <form id="Upload" name="frmUpload" action="1-4_FileIO.php" method="POST" enctype="multipart/form-data">
            <fieldset>
                <legend>User Info</legend>
                <div style="text-align: center;">
                    <input type="file" name="filUpload" id="upload"/>
                </div>
                <div style="text-align: center;">
                    <input type="submit" name="subFile" value="Upload"/>
                </div>
                
                <div>
                    <textarea name="txtArea" id="textArea" 
                    maxlength="500" rows="10" cols="50"></textarea>
                    
                </div>
            </fieldset>
        </form>
            <h2>
                <?php
                echo handleUpload();
                ?>
            </h2>
        
        <h3>output file - line by line </h3>
        <pre><?php readFileByLine()  ?></pre>
        <h3>output file - with array</h3>
        <pre><?php readFileArray() ?></pre>
        <h3>output file - All at once</h3>
        <pre><?php echo readFileAll() ?></pre>
        <h3>append textarea to the uploaded file</h3>
        
       
        <pre><?php 
        if(isset($_POST["subFile"]))//checking to see if the submit button was posted
        {
        //minicise append the text area text to the file - call appendFile
            
        appendFile(($_POST["txtArea"]));// we now know that the textArea is posted so we use $_POST
            
        echo readFileAll();
        }
        ?></pre>
        
        
        <h3>Use textarea to overwrite the uploaded file</h3>
        <pre><?php 
        if(isset($_POST["subFile"]))//checking to see if the submit button was posted
        {
        //minicise create a method to overwrite the file called overwriteFile
            
        overwriteFile(($_POST["txtArea"]));// we now know that the textArea is posted so we use $_POST
            
        echo readFileAll();
        }
        ?></pre>
        
        
        
    </body>
</html>
