<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>1-3 HTML Forms</title>
        <style>
            fieldset {width: 400px; border: 2px solid blue;}
            legend {padding: 5px 20px; border: 2px solid blue;}
            fieldset div {margin: 10px 5px; padding: 3px 0; border-bottom: 1px solid blue;}
            label {width: 150px; display: inline-block;}
            select, input[type='text'] {width: 200px;}
        </style>
    </head>
    <body>
        <h1>1-3 HTML Forms</h1>
        <?php
        // let's hide the form and output a success message when the form is submitted
        //first thing is check if the form was submitted
        if(isset($_GET["subUser"])) //if subUser is set then we know the form was submmitted
        {
            echo "<h1>Success - form submitted</h1>";
            
            echo "<br/> FirstName: " . $_GET["txtFirstName"];
            
            echo "<br/> LastName: " . $_GET["txtLastName"];
            
            $gender = isset($_GET["rdoGender"]) ? $_GET["rdoGender"] : "NOT SET";

            echo "<br/> Gender: " . $gender;
            
            echo "<br/> City: " . $_GET["txtCity"];
            
            echo "<br/> Adress: " . $_GET["txtAddress"];
            
            echo "<br/> Province: " . $_GET["selProvince"];

            $agreement = isset($_GET["chkAgreement"]) ? $_GET["chkAgreement"] : "NOT SET";
            
            echo "<br/> Agreed to terms: " . $agreement;
            
        }
        else
        {
            //show the form
        ?>
        
        
        <form id="frmUser" name="frmUser" action="1-3_HTMLForms.php" method="GET">
            <fieldset>
                <legend>User Info</legend>
                <div>
                    <label for="firstName">First Name</label>
<!--        
                    ID strictly used by the browser not the server and not posted to the server
                    name is the variable name posted/sent to the server
                    without a name an input is NOT sent to the server
                    the browser will alwath send data from a textbox even if the textbox is empty
-->
                    <input type="text" name="txtFirstName" id="firstName" maxlength="40"/>
                </div>
                <div>
                    <label for="lastname">Last Name</label>
                    <input type="text" name="txtLastName" id="lastName" maxlength="40"/>
                </div>
<!-- 
                if a radiobutton is not checked the browser will NOT submit any data from the button-
                not even the name
-->
                <div>
                    <label for="gender">Gender</label>
                    <input type="radio" name="rdoGender" id="gender" value="male"/>Male
                    <input type="radio" name="rdoGender" id="gender" value="Female"/>Female
                </div>
                <div>
                    <label for="address">Address</label>
                    <input type="text" name="txtAddress" id="address" maxlength="100"/>
                </div>
                
<!--            mini exercise 
                finish the form
                textbox city
                select box province (AB,BC,MB,SK) full province name in display
                check box for "Agree to Terms"
                submit and reset button -center in form
-->
                <div>
                    <label for="city">City</label>
                    <input type="text" name="txtCity" id="city" maxlength="50"/>
                </div>
                <div>
                    <label for="province">Province</label>
                    <select name="selProvince" id="province">
                        <option value="AB">Alberta</option>
                        <option value="BC">British Columbia</option>
                        <option value="MB">Manitoba</option>
                        <option value="SK">Saskatchewan</option>
                        
                    </select>
                    
                </div>
                <div>
                    <label for="agreement">Agree to Terms</label>

<!--                if a checkbox is not checked the browser will NOT submit any data from the checkbox-
                    not even the name
-->

                    <input type="checkbox" name="chkAgreement" id="agreement"/>
                </div>
                <div style="text-align: center;">
<!--
                    specified a name for the submit button to use it in php to see if the form was submitted
-->
                    <input type="submit" name="subUser" value="Create User"/>
                    <input type="reset" value="Clear"/>
                </div>
    
                    
    
            
            </fieldset>
        </form>
        <?php
        } //close else statement
        ?>
        
        <hr />
        
        <?php
        // let's hide the form and output a success message when the form is submitted
        //first thing is check if the form was submitted
        if(isset($_POST["subUser"])) //if subUser is set then we know the form was submmitted
        {
            echo "<h1>Success - form submitted</h1>";
            
            echo "<br/> FirstName: " . $_POST["txtFirstName"];
            
            echo "<br/> LastName: " . $_POST["txtLastName"];
            
            $gender = isset($_POST["rdoGender"]) ? $_POST["rdoGender"] : "NOT SET";

            echo "<br/> Gender: " . $gender;
            
            echo "<br/> City: " . $_POST["txtCity"];
            
            echo "<br/> Adress: " . $_POST["txtAddress"];
            
            echo "<br/> Province: " . $_POST["selProvince"];

            $agreement = isset($_POST["chkAgreement"]) ? $_POST["chkAgreement"] : "NOT SET";
            
            echo "<br/> Agreed to terms: " . $agreement;
            
        }
        else
        {
            //show the form
        ?>
        
        
        <form id="frmUser" name="frmUser" action="1-3_HTMLForms.php" method="POST">
            <fieldset>
                <legend>POSTED User Info</legend>
                <div>
                    <label for="firstName">First Name</label>
<!--        
                    ID strictly used by the browser not the server and not posted to the server
                    name is the variable name posted/sent to the server
                    without a name an input is NOT sent to the server
                    the browser will alwath send data from a textbox even if the textbox is empty
-->
                    <input type="text" name="txtFirstName" id="firstName" maxlength="40"/>
                </div>
                <div>
                    <label for="lastname">Last Name</label>
                    <input type="text" name="txtLastName" id="lastName" maxlength="40"/>
                </div>
<!-- 
                if a radiobutton is not checked the browser will NOT submit any data from the button-
                not even the name
-->
                <div>
                    <label for="gender">Gender</label>
                    <input type="radio" name="rdoGender" id="gender" value="male"/>Male
                    <input type="radio" name="rdoGender" id="gender" value="Female"/>Female
                </div>
                <div>
                    <label for="address">Address</label>
                    <input type="text" name="txtAddress" id="address" maxlength="100"/>
                </div>
                
<!--            mini exercise 
                finish the form
                textbox city
                select box province (AB,BC,MB,SK) full province name in display
                check box for "Agree to Terms"
                submit and reset button -center in form
-->
                <div>
                    <label for="city">City</label>
                    <input type="text" name="txtCity" id="city" maxlength="50"/>
                </div>
                <div>
                    <label for="province">Province</label>
                    <select name="selProvince" id="province">
                        <option value="AB">Alberta</option>
                        <option value="BC">British Columbia</option>
                        <option value="MB">Manitoba</option>
                        <option value="SK">Saskatchewan</option>
                        
                    </select>
                    
                </div>
                <div>
                    <label for="agreement">Agree to Terms</label>

<!--                if a checkbox is not checked the browser will NOT submit any data from the checkbox-
                    not even the name
-->

                    <input type="checkbox" name="chkAgreement" id="agreement"/>
                </div>
                <div style="text-align: center;">
<!--
                    specified a name for the submit button to use it in php to see if the form was submitted
-->
                    <input type="submit" name="subUser" value="Create User"/>
                    <input type="reset" value="Clear"/>
                </div>
    
                    
    
            
            </fieldset>
        </form>
        <?php
        } //close else statement
        ?>
    </body>
</html>
