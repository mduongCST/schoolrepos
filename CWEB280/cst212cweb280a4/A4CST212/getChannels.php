<?php
require_once 'private-classesA4/XMLDbObject.class.php';
//Make page expect XML 
header("Content-type: text/xml");

$xdb = new XmlDbObject();

//Prepare statement to retrieve Channels from the database
$stmt = $xdb->prepare("SELECT ChannelID,
    chanTitle FROM CST212Channel ORDER BY chanTitle");
//Bind the results to variables
$stmt->bind_result($id, $channel);

//Execute query
$stmt->execute();

//While there are channels to fetch
while($stmt->fetch())
{
    //Set the channelname along with its ID into an associatve array
    $infoData[]= array("id"=>$id, "channelName"=>$channel);
}

//Close the statement
$stmt->close();

//Set the rootnode to grab the channels
$rootNode = $xdb->convertToXML(null, $infoData, "channels", "channel"); 

echo $rootNode->asXML();