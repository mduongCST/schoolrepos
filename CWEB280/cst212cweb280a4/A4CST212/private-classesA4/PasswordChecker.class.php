<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * The PasswordChecker allows us to check if an entered username and
 * password is valid.
 * 
 * @author CST212
 */
class PasswordChecker 
{    
    
    /**
     * Purpose: Create a pssword checker object.
     */
//    public function __construct()
//    {
        //Fill in the password array
//        $this->password["ernesto"] = "dancer";
//        $this->password["shane"] = "fighter";
//        $this->password["bryce"] = "lover";
        
        
//    }
    
    /**
     * Purpose: Determine whether the supplied username and password
     *          combination is valid
     * @param string $userName The username to check
     * @param string $password The password to check
     * @return boolean TRUE if user/password combination is valid
     *         and false otherwise
     */
    public function isValid($username, $password)
    {
        //Verify that the suername exists, and the supplied password
        //Matches the real password
        //Open a database connection
        $db = new DbObject();
        //Query for the password for the specified username
        $qryResults = $db->select("password", "Password",
                      "username = '$username'");
        //If there was one row returned, check the password against
        //the supplied password
        $valid = FALSE;
        if($qryResults->num_rows == 1)
        {
            $passwordRow = $qryResults->fetch_row();
            if($passwordRow[0] == crypt($password, $passwordRow[0]))
            {
            $valid = true;
            }
        }
        //Return whether the username and password combination is valid                
        return $valid;
    }
    
        /**
     * Purpose: Add a user into the password list
     * @param string $username The username to add
     * @param string $password The password associated with the username
     * @return boolean TRUE if the user was successfully added,
     *   FALSE otherwise
     */
    public function addUser( $username, $password ) {
        // Create the array to use with the insert method
        $record["username"] = strip_tags($username);
        $record["password"] = crypt($password);
        
        
        // Open a database connection
        $db = new DbObject();
        
        // Insert the user into the Password database
        $numRows = $db->insert( $record, "Password" );

        return ( $numRows == 1 );
    }

}
?>