<?php
/**
 * Description of GenerateForm
 *  this class will define HELPER methods to generate HTML forms on webpage
 * we will continue using this class going forward to speed up coding of examples and assignments
 * @author ins226
 */
class ImprovedGenerateForm {
    // these constants are format strings for use in sprintf calls
    const FORMSTART = '<form id="%s" action="%s" method="%s" %s><fieldset %s><legend>%s</legend>';
    const FORMEND = '<div %s><input type="submit" value="%s" name="%s" /><input type="submit" value="%s" name="%s" /></div></fieldset></form>';
    //using heredoc notation to define a nicely indented html code output
    const INPUTWRAP = <<<EOT

<div>
    <label for="%s">%s</label>
    %s
</div>
EOT;
// the above line indicates the end of string - nothing else can be on that line
    
    // define a public variable as a flag as to whether or not 
    // to get the value from the post and set the value of the generated form elements
    public $rememberUserInput = true;// used in get value
    
    /*
     * remberUserInput as optional - used to deterimine whether the values should be remembered on post back
     */
    function __construct($rememberUserInput=true) {
        $this->rememberUserInput = $rememberUserInput;
    }
    
    /*
     * startForm
     * generates the begining of the form using the passed in params
     * name - the id of the fomr element
     * display - is the legend of the fieldset
     * formExtras - any other attributes to the form that may be requires ex: enctype="multipart/form-data"
     * action - the page to post to
     * method - the way data is sent to the server
     * fieldsetExtras - any other attributes to the fieldset element that may be requires ex: style="color:#000;"
     * 
     */
    public function startForm($name, $display, $formExtras="",
            $action="#", $method="POST", $fieldsetExtras="")
    {
        printf(self::FORMSTART,$name,$action,$method,$formExtras,$fieldsetExtras,$display);
    }
    //minicise create endForm function that generates the end form
    // use these variables in the best order you can think of
    //$nameSubmit, $divExtras, $displayReset, $displaySubmit
    
    /*
     * endForm
     * generates the closing form tags and buttons
     * nameSubmit - the name of the submit button sent to the server
     * displaySubmit - the value attribute of the submit button - displayed to the user
     * displayReset - the value attribute of the reset button - displayed to the user
     * divExtras - any other attributes that may be needed for the div button container
     */
    public function endForm($nameSubmit, $displaySubmit, $displayReset="Reset", $divExtras="", $nameCancel="Cancel")
    {
        printf(self::FORMEND, $divExtras, $displaySubmit, $nameSubmit,$displayReset, $nameCancel);
    }
    
 /*
     * getValue will try to get the posted value for an input from the $_POST or the $_GET arrays
     * if the value is not set then the functon with return a default value
     * params:
     * name - the name of the posted input
     * defaultValue - optional return value - by default : null
     */
    private function getValue($name, $defaultValue=null)
    {
        // use a ternary statement to return the user input value or the default value
        // in order to get posted data from the GET or POST we can use the $_REQUEST array
        // check to see if this form is supposed to remeber the input vlaue
        return $this->rememberUserInput && isset($_REQUEST[$name]) ?  $_REQUEST[$name] : $defaultValue;
    }
        
	
    /*
     * textbox
     * generates a wrapped text input
     * name- the name send to the server and the id of the element
     * display - the label of the field
     * max - maxlength of the  text box
     * value - the default value populated in the text box
     * extras - any other attributes that may be required
     */	
    public function textbox($name, $display, $max=50, $value="", $extras="")
    {
        $value = $this->getValue($name,$value);
        $input = sprintf('<input type="text" name="%s" id="%s" maxlength="%d" value="%s" %s />',
                $name, $name, $max, htmlentities($value), $extras);
        printf(self::INPUTWRAP, $name, $display, $input);
    }
    
    
    /*
     * password
     * generates a wrapped text input
     * name- the name send to the server and the id of the element
     * display - the label of the field
     * max - maxlength of the password
     * value - the default value populated in the text box
     * extras - any other attributes that may be required
     */	
    public function password($name, $display, $max=16, $value="", $extras="")
    {
        $value = $this->getValue($name,$value);
        $input = sprintf('<input type="password" name="%s" id="%s" maxlength="%d" value="%s" %s />',
                $name, $name, $max, htmlentities($value), $extras);
        printf(self::INPUTWRAP, $name, $display, $input);
    }
    
    
    /*
     * hidden
     * generates a hidden form input
     * name- the name send to the server and the id of the element
     * value - the default value populated in the text box
     * extras - any other attributes that may be required
     */	
    public function hidden($name, $value="", $extras="")
    {
        $value = $this->getValue($name,$value);
        //hidden inputs are not visible to the user so no need for the Input wrapper
        printf('<input type="hidden" name="%s" id="%s" value="%s" %s />',
                $name, $name,  htmlentities($value), $extras);        
    }    
    
    /*
     * file
     * generates a wrapped file input
     * name- the name send to the server and the id of the element
     * display - the label of the field
     * value - the default value populated in the text box
     * extras - any other attributes that may be required
     */	
    public function file($name, $display, $value="", $extras="")
    {
        $value = $this->getValue($name,$value);
        
        $input = sprintf('<input type="file" name="%s" id="%s" value="%s" %s />',
                $name, $name, htmlentities($value), $extras);
        printf(self::INPUTWRAP, $name, $display, $input);
    }     
    
    /*
     * textArea
     * generates a wrapped text input
     * name- the name send to the server and the id of the element
     * display - the label of the field
     * value - the default value populated in the textarea
     * extras - any other attributes that may be required
     */		
    public function textarea($name, $display, $value="", $extras="")
    {
        $input = sprintf('<textarea name="%s" id="%s" %s >%s</textarea>',
                $name, $name, $extras, 
                $this->getValue($name,$value));
        printf(self::INPUTWRAP, $name, $display, $input);
    }
    
    /*
     * checkbox
     * generates a wrapped checkbox input
     * name- the name send to the server and the id of the element
     * display - the label of the field
     * checked - whether the box is checked Default - not checked
     * value - the default value of the checkbox Default - empty string
     * extras - any other attributes that may be required
     */	
    public function checkbox($name, $display, $value="1", $extras="")
    {
       $checked = $this->getValue($name) ? 'checked="yes"' : '';
       
       $input = sprintf('<input type="checkbox" name="%s" id="%s" value="%s" %s %s />',
            $name, $name, $value, $checked, $extras);
       
        printf(self::INPUTWRAP, $name, $display, $input);
    }
    
    /*
     * option
     * generates a single option markup for a select box
     * nameSelect - the name of the parent select box used to determine whether to mark
     *      the current option as selected
     * display- the option text
     * value - the option value
     * selectedValue - the default value to mark as selected used to determine whether to mark
     *      the current option as selected in conjunction with the nameSelect
     */
    private function option($nameSelect, $display, $value, $selectedValue="")
    {
       $selected = $this->getValue($nameSelect,$selectedValue) == $value ? 'selected="yes"' : '';
       
       return sprintf('<option value="%s" %s >%s</option>
',  htmlentities($value), $selected, $display);
       
    }
    
    /*
     * select
     * generates a wrapped select box and options
     * name - the name and id of the select box
     * display - the label of the selectbox
     * options - associative array of option values and text
     * selectedValue - the default option to be selected
     * extras- any other attributes that maybe needed
     */
    public function select($name, $display, $options, $selectedValue="", $extras="")
    {
        $optionsMarkUp = "";
        foreach($options as $valueOption=>$displayOption)
        {
            $optionsMarkUp .= $this->option($name,$displayOption,$valueOption, $selectedValue);
        }
        $input = sprintf('<select name="%s" id="%s" %s >
            %s
            </select>',
            $name, $name, $extras, $optionsMarkUp);
        printf(self::INPUTWRAP, $name, $display, $input);
    }
    
    
    /*
     * multiselect - alias of select method with extras param of 'multiple="multiple" '
     * @param string $name - the name and id of the select box
     * @param string $display -  the label of the selectbox
     * @param type $options associative array of option values and text
     * @param string $selectedValues - comma separated list of value of the default options to be selected (default: empty string)
     * @param string $extras - any other attributes that maybe needed (default: empty string)
     */
    public function multiselect($name, $display, $options, $selectedValues="", $extras="")
    {
        //adding '[]' to the name will indicate to php to save the multiple values in an array
        // specifying 'multiple="multiple" ' in the extras param tells the browser render a select as a listbox
        $this->select($name, $display, $options, $selectedValues, 'multiple="multiple" '.$extras);
    }    
    
    /*
     * radio
     * generates a single option markup for a select box
     * name - the name of the radio group used to determine whether to mark
     *      the current radio as checked
     * display- the label of the radio
     * value - the radio button value
     * selectedValue - the default value to mark as checked used to determine whether to mark
     *      the current radio as checked in conjunction with the name
     */    
    private function radio($name, $display, $value, $selectedValue="", $extras="")
    {
       //determine whether to mark the current radio button as checked 
       $checked = $this->getValue($name,$selectedValue) == $value ? 'checked="yes"' : '';
       
       //create a unique id from the name and cleaned value of the radio button
       //the id is used by label adjacent to the radiobutton
       $id = $name . preg_replace("/\W/i", "" ,$value);
       
       return sprintf('<div><input type="radio" name="%s" id="%s" value="%s" %s %s /><label for="%s">%s</label></div>
',$name, $id, htmlentities($value), $checked, $extras, $id, $display);
       
    }    
    
    /*
     * radioGroup
     * generates a wrapped group of radio buttons
     * name - the name of the radio group
     * display - the label for the radio Group
     * options - associative array of radio values and labels
     * selectedValue - the default radio to be checked
     * extras- any other attributes that maybe needed
     */
    public function radioGroup($name, $display, $options, $selectedValue="", $extras="")
    {
        
        // surround the radio groups with a span tag
        $radioMarkUp = "<span>";
        
        //loop through the options array and call the option method
        foreach($options as $valueRadio=>$displayRadio)
        {
            //append the radiobutton string to the markup
            $radioMarkUp .= $this->radio($name,$displayRadio, $valueRadio, $selectedValue, $extras);
        }
        
        $radioMarkUp .= "</span>";
        
        // no need to specify the name this time since each radio button will have its own label
        printf(self::INPUTWRAP, "", $display, $radioMarkUp);

    }

    
    
}
