<?php
function loadClass($className){
    $classPaths[]="../classes/";
    $classPaths[]="private-classesA4/";
    
    foreach($classPaths as $path){
        $file = "$path$className.class.php";
        if(file_exists($file)){require_once $file;}
    }
}
spl_autoload_register('loadClass'); 

/**
 * The routine checks for the xml file and creates an xml document if it does not exist
 * @global string $xmlDataFile
 * @return SimpleXMLElement
 */
function getRootNode(){
    global $xmlDataFile;
    
//If there was an uploaded file    
if(is_uploaded_file($_FILES["FileRSS"]["tmp_name"]))
{
        //check if it is an XML file
        if($_FILES["FileRSS"]["type"] == "text/xml" && preg_match("(xml)", $_FILES["FileRSS"]["type"]))
        {
            //Set the xmlDataFile to the file uploaded
            $xmlDataFile = $_FILES["FileRSS"]["tmp_name"];
        }    
        //Else file uploaded is not a valid XML file.
        else 
        {
            echo "<p>Error! File uploaded is not an XML file.</p>";
        }
}
    //init our return variable
    $rootNode = null;
    
    // see if xml file exists
    if(file_exists($xmlDataFile)){
        $rootNode = simplexml_load_file($xmlDataFile); //load the xml file and return SimpleXMLElement
    }else{
        // if file does not exist create one
        $rootNode = new SimpleXMLElement("<?xml version='1.0' encoding='utf-8' ?><channels />");
        /*$rootNode = simplexml_load_string("<?xml version='1.0' encoding='utf-8' ?><provinces/>");*/
    }
    
    return $rootNode;
}

/**
 * this routine saved the xml document to a file on the disk
 * may also format the document to be human readable
 * @global string $xmlDataFile
 * @param SimpleXMLElement $rootNode
 * @param boolean $format
 * @return boolean
 */
function saveXML($rootNode, $format=false){
    global $xmlDataFile;
    //init return variable
    $saved = false;
    
    if($format){
        //use DOMDocument to format the xml into pretty human readable xml
        //this is excessive processing but hey what you gonna do
        $xmlDOM = dom_import_simplexml($rootNode)->ownerDocument;
        $xmlDOM->preserveWhiteSpace = false;
        $xmlDOM->formatOutput = true;
        // use the DOMDocument save method
        $saved = $xmlDOM->save($xmlDataFile);
    }else{
        //using simplexml asXML method to save the xml to disc
        //returns true or false depending on whether it saved the file
        $saved = $rootNode->asXML($xmlDataFile);
    }
    return $saved;    
}