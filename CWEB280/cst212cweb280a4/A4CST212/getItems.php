<?php
    session_start(); //gonna remember sort order
    require_once 'private-classesA4/XMLDbObject.class.php';
    if(!isset($_GET['chanid']))
    {
        echo "<p>Invalid channel id";
        exit(0); //force end of execution
    }
    
    //Tell the browser to expect xml data
   header("Content-type: text/xml");
   
    // if get is set
    if(isset($_GET["read"]))
    {
        // check if session is set
        if(isset($_SESSION["readAlready"]))
        {
            // set session and add itemid
            array_push($_SESSION["readAlready"], $_GET["read"]);
        }
        // else set if not set and add itemid
        else
        {
            $_SESSION["readAlready"] = array();
            array_push($_SESSION["readAlready"], $_GET["read"]);
        }
    }
   //sort order logic
   
   $ssCode = &$_SESSION['sortCode'];
   $ssDir = &$_SESSION['sortDirection'];
   
   if(!isset($_GET['sort']))
   {
       //get the order from the session
       $sortCode = !isset($ssCode) ? 0 : $ssCode;
       $sortDirection = empty($ssDir) ? "ASC" : $ssDir;
   }
   else
   {
      $sortCode = $_GET['sort'];
      //really really really compliciated
      $sortDirection = !isset($ssCode) || $ssCode != $sortCode || 
              $ssDir == "Desc" ? 'ASC' : 'Desc';

   }
   
   //Save the current sort code and direction to session
   $ssCode = $sortCode;
   $ssDir = $sortDirection;
   
   $fieldList = "itemTitle, itemchanLink, itemAuthor,
       itemPubDate, c.chanTitle";
   $fieldArray = explode(',', $fieldList);
   
   //multiple select logic - aka dynamic number of paramas in prepared statement
   $ids = explode(',', $_GET['chanid']);
   $placeHolder = '?'.str_repeat(',?', count($ids)-1);
   
   //add references to from the ids array to the new $reds array
   $refs = array(str_repeat('i', count($ids)));
   foreach(array_keys($ids) as $key)
   {
       $refs[] = &$ids[$key];
   }
    
    $xdb = new XMLDbObject();

    //Method 1: Using prepared statements with parameters
    $stmt = $xdb->prepare("SELECT $fieldList FROM CST212Item i
                            JOIN CST212Channel c ON c.channelID = i.channelID
                            WHERE i.channelID IN ($placeHolder)
                            ORDER BY {$fieldArray[$sortCode]} $sortDirection");
                            
    
    //$stmt->bind_param('i', $_GET['provid']);
    call_user_func_array(array($stmt, 'bind_param'), $refs);
    $stmt->bind_result($itemTitle, $itemchanLink, $itemAuthor, $itemPubDate, $channelID);
    $stmt->execute();

    $infoData = array();
    while($stmt->fetch())
    {
        
        $read = "null";
        if(isset($_SESSION["readAlready"]))
        {
            if(in_array("" + $itemchanLink, $_SESSION["readAlready"]))
            {
                $read = "read";
            }
            
        }
        //write data into a 2D array
        //Because our linux php implementation does not support 
        //$stmt->get_result we have to go through the extra step
        // of reading the data into a 2D array.
        
        //This will define the xml element names and values
        $infoData[] = array(
            "itemTitle" => $itemTitle,
            "itemchanLink" => $itemchanLink,
            "itemAuthor" => $itemAuthor,
            "itemPubDate" => $itemPubDate,
            "channelID" => $channelID,
            "read" => $read
             );
    }
    
    //done looping through statement so close
    $stmt->close();

    
    $rootNode = $xdb->convertToXml(null, $infoData, "items", "item");
    
    //output xml as string - use asXML to convert to string
    echo $rootNode->asXML();