INSERT INTO Owner (ownerID, ownerName, phoneNumber, emailAddress)
VALUES (1, 'Marty McFly', '1-306-555-5555', 'mcFly@backtothefuture.com');

INSERT INTO Owner (ownerID, ownerName, phoneNumber, emailAddress)
VALUES (2, 'Jon Snow', '1-369-231-7844', 'whosmymom@thewall.com');

INSERT INTO Dog (dogID, dogName, dogBreed, dogAge, dogGender, dogFixed, ownerID)
VALUES (1, 'Yuno', 'Wild Wolf', 6, 'M', 1, 2);

INSERT INTO Dog (dogID, dogName, dogBreed, dogAge, dogGender, dogFixed, ownerID)
VALUES (2, 'Nuthang', 'Wild Wolf', 8, 'F', 1, 2);

INSERT INTO Dog (dogID, dogName, dogBreed, dogAge, dogGender, dogFixed, ownerID)
VALUES (3, 'Maggie McFly', 'Poodle', 10, 'F', 0, 1);

SELECT P.productID, P.productName, C.categoryName
                      FROM Products P JOIN Categories C ON P.categoryID = C.categoryID;