﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matrix
{
    public class Program
    {
        static void TestLeastSquares()
        {
            double[,] dArray = { {-2,4}, {-1,2}, {0,1}, {1,2}, {2,4} };
            Matrix m = new Matrix(dArray);
            //Pass in the correct degree
            Console.WriteLine(m.LeastSquares(2).ToString());
        }

        static void TestMatrix()
        {
            double [,] dArray = {{90.1, 65.7, 98}, {87.9, 67.6, 86.8}};
            Matrix m = new Matrix(dArray);
            Console.WriteLine(m.ToString());
        }

        static void TestAdd()
        {
            double[,] d1 = { { 1, 3 }, { 4, -1 } };
            double[,] d2 = { { 0, -3 }, { 1, 2 } };

            Matrix m1 = new Matrix(d1);
            Matrix m2 = new Matrix(d2);
            Matrix mSum = null;
            mSum = m1 + m2;
            Console.WriteLine(mSum.ToString());

        }

        static void TestMultiply()
        {
            double[,] d1 = { { 1, 3 }, { 4, -1 } };
            double[,] d2 = { { 0, -3 }, { 1, 2 } };

            Matrix m1 = new Matrix(d1);
            Matrix m2 = new Matrix(d2);
            Matrix mProduct = null;
            mProduct = m1 * m2;
            Console.WriteLine(mProduct.ToString());

        }

        static void TestSubtract()
        {
            double[,] d1 = { { 1, 3 }, { 4, -1 } };
            double[,] d2 = { { 0, -3 }, { 1, 2 } };

            Matrix m1 = new Matrix(d1);
            Matrix m2 = new Matrix(d2);
            Matrix mDiff = null;
            mDiff = (Matrix)m1.Subtract(m2);
            Console.WriteLine(mDiff.ToString());

        }

        static void TestScalarMultiply()
        {
            double[,] dArray = { { 90.1, 65.7, 98 }, { 87.9, 67.6, 86.8 } };
            Matrix m = new Matrix(dArray);
            Console.WriteLine(m * 3.0);
        }

        static void TestGaussJordan()
        {
            double[,] dArray = { { 8, -5, 1, -10 }, { 2, -4, -1, -20 }, { 6, -8, 1, -100 } };
            Matrix m = new Matrix(dArray);
            Matrix mSolution = (Matrix) m.GaussJordanElimination();
            Console.WriteLine(mSolution.ToString());
        }
        static void Main(string[] args)
        {
            //TestMatrix();            
            //TestAdd();
            //TestScalarMultiply();
            //TestSubtract();
            //TestMultiply();
            TestGaussJordan();
            //TestLeastSquares();
        }
    }
}
