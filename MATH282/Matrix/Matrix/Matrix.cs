﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matrix
{
    class Matrix: AMatrix
    {
        #region Attributes
        private double[,] dArray;
        #endregion

        public Matrix(double[,] dArray)
        {
            this.dArray = dArray;
            this.Rows = dArray.GetLength(0);
            this.Cols = dArray.GetLength(1);
        }

        internal override AMatrix NewMatrix(int iRows, int iCols)
        {
            return new Matrix(new double[iRows, iCols]);
        }

        public override double GetElement(int iRow, int iCol)
        {
            return dArray[iRow - 1, iCol - 1];
        }

        public override void SetElement(int iRow, int iCol, double dValue)
        {
            dArray[iRow - 1, iCol - 1] = dValue;
        }

        
        #region Operator Overloading
        //Some languages support operator overloading and others do not (Java).
        //You should look at the documentation to see what operators are allowed to be overloaded.
        //Some languages require operators to be overloaded in pairs. For example if you overload +
        //then you would have to overload -
        public static Matrix operator +(Matrix LeftOp, Matrix RightOp)
        {
            return (Matrix)LeftOp.Add(RightOp);
        }

        public static Matrix operator -(Matrix LeftOp, Matrix RightOp)
        {
            return (Matrix)LeftOp.Subtract(RightOp);
        }

        public static Matrix operator *(Matrix LeftOp, Matrix RightOp)
        {
            return (Matrix)LeftOp.Multiply(RightOp);
        }

        public static Matrix operator *(Matrix LeftOp, double dScalar)
        {
            return (Matrix)LeftOp.ScalarMultiply(dScalar);
        }

        public static Matrix operator *(double dScalar, Matrix RightOp)
        {
            return (Matrix)RightOp.ScalarMultiply(dScalar);
        }

        #endregion
    }
}
