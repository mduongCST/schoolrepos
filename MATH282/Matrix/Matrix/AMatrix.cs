﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matrix
{
    public abstract class AMatrix: IMatrix, ICloneable
    {
        #region Attributes
        private int iRows;
        private int iCols;
        #endregion

        #region Properties
        public int Rows
        {
            get { return iRows; }
            set {
                    if (value < 1)
                    {
                        throw new ApplicationException("Rows must be one or greater");
                    } 
                    iRows = value;
                }
        }
        public int Cols
        {
            get { return iCols; }
            set {
                    if (value < 1)
                    {
                        throw new ApplicationException("Cols must be one or greater");
                    } 
                    iCols = value;
                }
        }
        #endregion 

        #region Abstract Members
        internal abstract AMatrix NewMatrix(int iRows, int iCols);
        public abstract double GetElement(int iRow, int iCol);
        public abstract void SetElement(int iRow, int iCol, double dValue);
        #endregion

        public object Clone()
        {
            return ScalarMultiply(1);
        }

        #region Least Squares
        /// <summary>
        /// This method is called by a matrix of data points.
        /// The size of the matrix should be 2 x n where n is the number 
        /// of data points
        /// </summary>
        /// <param name="m">Degree (order) required</param>
        /// <returns>Solved Augmented Matrix</returns>
        public AMatrix LeastSquares(int m)
        {
            //Create the augmented matrix
            AMatrix mAugmented = this.NewMatrix(m+1,m+2);
            //Create temporary storage
            double[] xSums = new double[(2 * m) + 1];
            //If the calling matrix is the correct size
            double xSum = 0;
            double dPow = 0;
            double ySum = 0;

            if (this.Rows > m && this.Cols == 2)
            {
                //Calculate the x-sums and store them in the temporary storage
                for (int i = 1; i <= xSums.Length; i++)
                {

                    for (int j = 1; j <= this.Rows; j++)
                    {
                        double xVal = this.GetElement(j, 1);
                        xSum += Math.Pow(xVal, dPow);
                    }
                    xSums[i - 1] = xSum;
                    xSum = 0;
                    dPow++;
                }
                //Calculate the y-sums and store them in the last column of the augmented matrix
                dPow = 0;
                for (int i = 1; i < mAugmented.Cols; i++)
                {
                    for (int j = 1; j <= this.Rows; j++)
                    {
                        double xVal = this.GetElement(j, 1);
                        double yVal = this.GetElement(j, 2);
                        double xSqr = Math.Pow(xVal, dPow);
                        ySum += yVal * xSqr;
                    }
                    mAugmented.SetElement(i, mAugmented.Cols, ySum);
                    ySum = 0;
                    dPow++;
                }
                //Load the x-sums from temporary storage into the augmented matrix (nested for loops)
                for (int row = 1; row <= mAugmented.Rows; row++)
                {
                    int xSumPos = row - 1;
                    for (int col = 1; col < mAugmented.Cols; col++)
                    {
                        mAugmented.SetElement(row, col, xSums[xSumPos]);
                        xSumPos++;
                    }
                }

            }
            //else
            else
            {
                //Throw an exception
                throw new ApplicationException("Not a valid size");
            }
            //return the augmented matrix after being solved by Gauss Jordan Elimination
            return mAugmented.GaussJordanElimination();
        }
        #endregion 


        #region ToString
        public override string ToString()
        {
            StringBuilder s = new StringBuilder();
            char cUL = (char)0x250C;
            char cUR = (char)0x2510;
            char cLL = (char)0x2514;
            char cLR = (char)0x2518;
            char cVLine = (char)0x2502;

            //build the top row
            s.Append(cUL);
            for (int j = 1; j <= this.Cols; j++)
            {
                s.Append("\t\t");
            }
            s.Append(cUR + "\n");

            //build the data rows
            for (int i = 1; i <= this.Rows; i++)
            {
                s.Append(cVLine);
                for (int j = 1; j <= this.Cols; j++)
                {
                    if (this.GetElement(i, j) >= 0)
                    {
                        s.Append(" ");
                    }
                    s.Append(String.Format("{0:0.000 e+00}", this.GetElement(i, j)) + "\t");

                }
                s.Append(cVLine + "\n");
            }
            //Build the bottom row
            s.Append(cLL);
            for (int j = 1; j <= this.Cols; j++)
            {
                s.Append("\t\t");
            }
            s.Append(cLR + "\n");
            return s.ToString();
        }
        #endregion

        public AMatrix GaussJordanElimination()
        {
            //A reference to the calling matrix
            AMatrix mSolution = null;
            //Multiplying Factor
            double dFactor = 0;
            //The current pivot element
            double dPivot = 0;

            //If the size is not correct for an augmented matrix, throw an exception
            if (this.Cols != this.Rows + 1)
            {
                throw new ApplicationException("Incorrect dimensions for an augmented matrix.");
            }
            //Create a copy of the calling matrix
            mSolution = (AMatrix)this.Clone();

            //Begin Gauss-Jordan Elimination

            //Loop through each row, where current row is the pivot row
            for (int i = 1; i <= mSolution.Rows; i++)
            {
                //Check to see if the system is still solveable
                mSolution.SystemSolveable(i);

                //Get the current pivot value
                dPivot = mSolution.GetElement(i, i);
                //For each element in the pivot row, divide by the dPivot (Get pivot to = 1).
                //For efficiency, we start at column i as everything to the left of the pivot
                //is 0.
                for (int j = i; j <= mSolution.Cols; j++)
                {
                    double dCurrent = mSolution.GetElement(i, j) / dPivot;
                    mSolution.SetElement(i, j, dCurrent);
                }
                //Reduce every other row other than the pivot (Get a 0 in the pivot column)
                for (int k = 1; k <= mSolution.Rows; k++)
                {
                    //Skip the pivot row
                    if (k != i)
                    {
                        //Get the multiply the factor
                        dFactor = -1 * mSolution.GetElement(k, i);
                        //Multiply the pivot row by dFactor and add tot he current row
                        //We do this column by column
                        for (int j = i; j <= mSolution.Cols; j++)
                        {
                            double dCurrent = mSolution.GetElement(k, j) +
                                mSolution.GetElement(i, j) * dFactor;
                            mSolution.SetElement(k, j, dCurrent);
                        }
                    }
                }
            }
            return mSolution;
        }

        /// <summary>
        /// Check the current pivot to see if it is zero. If it isn't, do nothing.
        /// If it is zero, look for a non-zero pivot in the rows below. If a non-zero pivot
        /// is found swap the rows, else throw an exception indicating the system is not solveable.
        /// </summary>
        /// <param name="i">The current pivot row/column</param>
        private void SystemSolveable(int i)
        {
            int iNext = i + 1;
            AMatrix mSolution = this;            
            if (mSolution.GetElement(i, i) == 0)
            {
                bool isFound = false;
                while (!isFound && iNext <= mSolution.Rows)
                {
                    double currentPivot = mSolution.GetElement(iNext, i);
                    if (currentPivot == 0)
                    {
                        iNext++;
                    }
                    else //Swap rows
                    {
                        isFound = true;
                        for (int j = 1; j <= mSolution.Cols; j++)
                        {
                            double row1 = mSolution.GetElement(i, j);
                            double row2 = mSolution.GetElement(iNext, j);
                            mSolution.SetElement(iNext, j, row1);
                            mSolution.SetElement(i, j, row2);
                        }
                    }
                }
                if (!isFound)
                {
                    throw new ApplicationException("System is not solveable");
                }
            }
        }

        public IMatrix Add(IMatrix mRight)
        {
            AMatrix LeftOp = this;
            AMatrix RightOp = (AMatrix)mRight;
            AMatrix Sum = null;

            if (LeftOp.Rows == RightOp.Rows && LeftOp.Cols == RightOp.Cols)
            {
                //Create a result matrix for the sum
                Sum = NewMatrix(LeftOp.Rows, LeftOp.Cols);
                //Loop through the operands and add corresponding elements
                for (int i = 1; i <= LeftOp.Rows; i++)
                {
                    for (int j = 1; j <= LeftOp.Cols; j++)
                    {
                        //double dVal = LeftOp.GetElement(i, j) + RightOp.GetElement(i, j);
                        //Using one line
                        Sum.SetElement(i, j, LeftOp.GetElement(i, j) + RightOp.GetElement(i, j));
                    }
                }

            }
            else
            {
                throw new ApplicationException("Operands must be the same size.");
            }
            return Sum;
        }
        public IMatrix Subtract(IMatrix mRight)
        {
            //Simply add the opposite
            return this.Add(mRight.ScalarMultiply(-1));
        }

        public IMatrix Multiply(IMatrix mRight)
        {
            AMatrix LeftOp = this;
            AMatrix RightOp = (AMatrix)mRight;
            AMatrix Result = null;
            double dSum = 0;

            //If the operands can be multiplied
            if (LeftOp.Cols == RightOp.Rows)
            {
                //Create a result matrix of the correct size
                Result = NewMatrix(LeftOp.Rows, RightOp.Cols);
                //for each row in the left operand
                for (int i = 1; i <= LeftOp.Rows; i++)
                {
                    //for each column in the right operand
                    for (int j = 1; j <= RightOp.Cols; j++)
                    {
                        //Calculate the sum of the products of corresponding elements int
                        //leftrow of leftOp and rightcol of RightOp
                        dSum = 0;
                        for (int k = 1; k <= RightOp.Rows; k++)
                        {
                            dSum += LeftOp.GetElement(i, k) * RightOp.GetElement(k, j);
                        }
                        //Set the sum of the products into the Product matrix
                        Result.SetElement(i, j, dSum);
                    }
                }
            }
                //Operands are not correct size, so throw an exception
            else
            {
                throw new ApplicationException("Matrices aren't proper size for multiplication");
            }
            return Result;
        }

        public IMatrix ScalarMultiply(double dScalar)
        {
            AMatrix Product = NewMatrix(this.Rows, this.Cols);
            for (int i = 1; i <= Product.Rows; i++)
            {
                for(int j = 1; j <= Product.Cols; j++)
                {
                    Product.SetElement(i,j, this.GetElement(i,j) * dScalar);
                }
            }
            return Product;
        }
    }
}
