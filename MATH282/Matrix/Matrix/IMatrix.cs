﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matrix
{
    public interface IMatrix
    {
        IMatrix Add(IMatrix mRight);
        IMatrix Subtract(IMatrix mLeft);
        IMatrix Multiply(IMatrix mRight);
        IMatrix ScalarMultiply(double dScalar);
    }
}
