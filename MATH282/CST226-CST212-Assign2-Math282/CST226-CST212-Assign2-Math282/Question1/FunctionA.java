package LO4Itegration;

/**
 *  Sample function for exploring integration<br>
 *  Implements function f(x) = 1/4 x^2 - 1/400 x^4<br>
 *
 * @author CST 226 CST 212
 */

public class FunctionA extends ACFunction
{

    /**
     *  Returns the value of the function f(x) = 1/4 x^2 - 1/400 x^4<br>
     *  for a given x, implementing the abstract method from ACFunction<br>
     *
     * @param  x  Value to evaluate function at
     * @return    Value of function f(x) = 1/4 x^2 - 1/400 x^4 at argument
     */
    public double a1calculate( double x )
    {
        return (1.0 / 4.0) * x * x - (1.0 / 400.0) * Math.pow(x, 4.0);
    }
    
}
