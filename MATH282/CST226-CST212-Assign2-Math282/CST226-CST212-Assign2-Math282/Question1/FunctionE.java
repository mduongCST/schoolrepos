package LO4Itegration;

/**
 *  Sample function for exploring integration<br>
 *  Implements function <br>
 *
 * @author  CST 226 CST 212
 */

public class FunctionE extends ACFunction
{

    /**
     *  Returns the value of a function<br>
     *  for a given x, implementing the abstract method from ACFunction<br>
     *
     * @param  x  Value to evaluate function at
     * @return    Value of function argument
     */
    public double a1calculate( double x )
    {
        return (1/Math.sqrt(2*Math.PI)) * (Math.pow(Math.E,-((Math.pow(x, 2))/2)));
    }
    

}
