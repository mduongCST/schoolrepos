package LO4Itegration;

/**
 *  Sample function for exploring integration<br>
 *  Implements function <br>
 *
 * @author  CST 226 CST 212
 */

public class FunctionB extends ACFunction
{

    /**
     *  Returns the value of a function<br>
     *  for a given x, implementing the abstract method from ACFunction<br>
     *
     * @param  x  Value to evaluate function at
     * @return    Value of function argument
     */
    public double a1calculate( double x )
    {
        return 0.2 * ((1.0/100) * (322 + 3 * x * (98 + x * (37 + x))) - 24 * (x/(1 + x * x)));
    }
    

}
