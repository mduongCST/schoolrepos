package LO4Itegration;


/**
 *  Abstract class for functions to be used for finding zeros, etc.<br>
 *
 * @author  CST 226	CST212
 */

public abstract class ACFunction implements IFunction
{
    /**
     *  Returns the value of the function at a chosen point - must implement<br>
     *  specific calculations in any function class extending ACFunction<br>
     *
     * @param  x  Value to evaluate function at
     * @return    Value of function at argument
     */
    public abstract double a1calculate( double x );
    
   
    
    /**
     *  Prints table of function values over specified range<br>
     *  by specified step - can paste into Excel and graph<br>
     *
     * @param  dStart  Starting value of x for table of function values
     * @param  dEnd    Ending value of x for table of function values
     * @param  dStep   Amount to increment x for table of function values
     * @return         Nothing
     * @throws         Starting value of range greater than ending value
     * @throws         Increment not positive
     */
    public void printTable( double dStart, double dEnd, double dStep )
            throws IllegalArgumentException
    {
        if ( dStart > dEnd )
        {
            throw new IllegalArgumentException( "Start must be <= End" );
        }
        if ( dStep <= 0 )
        {
            throw new IllegalArgumentException( "Step must be > 0" );
        }
        System.out.println( " x \t f(x) " );
        System.out.println( "===\t======" );
        for ( double x = dStart; x <= dEnd; x += dStep )
        {
            System.out.println( x + "\t" + this.a1calculate( x ) );
        }
        System.out.println( );
    }
    
    
    /**
     * Integrates the function using the left integration rule 
     * 
     * @param dLeft The left x value
     * @param dRight the right x value
     * @param dPrecision the amount of precision 
     * @param iMaxLoops	The number of time to attempt integration		
     * @return
     */  
    public double integrateLeft( double dLeft, double dRight,
            double dPrecision, int iMaxLoops )
    {
        // 1st estimate - one rectangle
        long numRectangles = 1;
        double totalWidth = Math.abs(dRight - dLeft);
        double estimate = this.a1calculate(dLeft) * totalWidth;
        System.out.println( "Estimate with 1 rectangle: " + estimate);
              
        int numLoops = 0;
        boolean keepGoing = true;
        
        while ( keepGoing )
        {
            numLoops++;
            double oldEstimate = estimate;
            
            // find a better approximation - 2x rectangles
            numRectangles = numRectangles * 2;
            double currentWidth = totalWidth / numRectangles;
            estimate = 0.0;
            
            for ( long i = 0; i < numRectangles; i++ )
            {
                double currentLeft = dLeft + i * currentWidth;
                double currentHeight = this.a1calculate( currentLeft );
                double currentArea = currentHeight * currentWidth;
                estimate += currentArea;
            }
            System.out.println( "Estimate with " + numRectangles
                    + " rectangles: " + estimate);
           
            // are we "close enough"?
            double error = Math.abs( (estimate - oldEstimate) / estimate);
            if ( error <= dPrecision )
            {
                keepGoing = false;
            }
            else if ( numLoops >= iMaxLoops )
            {
                System.out.println( "Did not converge" );
                keepGoing = false;
            }
        }
        return estimate;
    }
    
    /**
     * Integrates the function using the left integration rule with adjustments 
     * to make the integration more efficient 
     * 
     * @param dLeft The left x value
     * @param dRight the right x value
     * @param dPrecision the amount of precision 
     * @param iMaxLoops	The number of time to attempt integration		
     * @return
     */
    public double integrateLeftEfficient( double dLeft, double dRight,
            double dPrecision, int iMaxLoops )
    {
    	// Set numParts to 1
    	int numParts = 1;
    	// Get the total width 
    	double totalWidth = dRight - dLeft;
    	// Get the height 
    	double height = this.a1calculate( dLeft );
    	// Calculate the estimate
    	double estimate = height * totalWidth; 
    	// Set loopCount to 0
    	int loopCount = 0;
    	// Set keeGoing to true
    	boolean keepGoing = true;
    	// Loop while keepGoing is true
    	while(keepGoing)
    	{
    		// Increment the loopCount
    		loopCount ++; 
    		// Copy the estimate
    		double oldEstimate = estimate;
    		// Multiply the numParts by 2
    		numParts *= 2;
    		// Set width to totalWidth divided by numParts
    		double width = totalWidth/numParts; 
    		// Loop for numParts
    		for(int i = 1; i < numParts; i += 2)
    		{
    			// Add to height
    			height += this.a1calculate(dLeft + i*width);
    			// Calculate the new estimate 
    			estimate = height * width; 
    		}
    		// Calculate the error
    		double error = Math.abs((estimate - oldEstimate) / estimate); 
    		// If the error is less than or equal to the precision 
    		if(error <= dPrecision)
    		{
    			// Set keepGoing to false to drop out of loop 
    			keepGoing = false; 
    		}
    		// Else if the loop count has been exceeded, drop out of loop 
    		else if(loopCount >= iMaxLoops)
    		{
    			// Display an error message
    			System.out.println("Did not converge to desired precision within spacified maximum loops");
    			// Set keepGoing to false
    			keepGoing = false; 
    		}
    	}
    	// Return the value 
    	return estimate; 
    }
    
    /**
     * Integrates the function using the Trapezoid integration rule
     * 
     * @param dLeft The left x value
     * @param dRight the right x value
     * @param dPrecision the amount of precision 
     * @param iMaxLoops	The number of time to attempt integration		
     * @return
     */
    public double integrateTrapezoidRule( double dLeft, double dRight, double dPrecision, int iMaxLoops)
    {
    	int numTrap = 1; 
    	double totalWidth = dRight - dLeft;
    	double estimate = 0.5 * (this.a1calculate(dLeft) + this.a1calculate(dRight)) * totalWidth;
    	int numLoops = 0;
    	boolean keepGoing = true; 
    	
    	while(keepGoing)
    	{
    		numLoops ++;
    		double oldEstimate = estimate;
    		numTrap *= 2;
    		double width = totalWidth / numTrap; 
    		estimate = 0;
    		
    		for (int i = 0; i < numTrap; i++)
    		{
    			double currentLeft = dLeft + i * width;
    			double currentRight = currentLeft + width;
    			double currentArea = 0.5 * (this.a1calculate(currentLeft) + this.a1calculate(currentRight)) * width;
    			estimate += currentArea; 
    		}
    		double error = Math.abs((estimate - oldEstimate) / estimate);
    		
    		if(error <= dPrecision)
    		{
    			keepGoing = false;
    		}
    		else if (numLoops >= iMaxLoops)
    		{
    			System.out.println("Did not converge");
    			keepGoing = false;
    		}
    	}
    	return estimate;
    }
    
    /**
     * Integrates the function using the Trapezoid integration rule with adjustments 
     * to make the integration more efficient 
     * 
     * @param dLeft The left x value
     * @param dRight the right x value
     * @param dPrecision the amount of precision 
     * @param iMaxLoops	The number of time to attempt integration		
     * @return
     */
    public double integrateTrapezoidRuleEfficient( double dLeft, double dRight,
            double dPrecision, int iMaxLoops )
    {
    	int numTrap = 1; 
    	double totalWidth = dRight - dLeft;
    	double estimate = 0.5 * (this.a1calculate(dLeft) + this.a1calculate(dRight)) * totalWidth;
    	int numLoops = 0;
    	boolean keepGoing = true; 
    	
    	while(keepGoing)
    	{
    		numLoops ++;
    		double oldEstimate = estimate;
    		
    		numTrap *= 2;
    		double width = totalWidth / numTrap; 
    		estimate = 0;
    		
    		for (int i = 0; i < numTrap; i++)
    		{
    			double currentLeft = dLeft + i * width;
    			double currentRight = currentLeft + width;
    			double currentArea = (this.a1calculate(currentLeft) + this.a1calculate(currentRight));
    			estimate += currentArea; 
    		}
    		estimate *= 0.5 * width; 
    		double error = Math.abs((estimate - oldEstimate) / estimate);
    		
    		if(error <= dPrecision)
    		{
    			keepGoing = false;
    		}
    		else if (numLoops >= iMaxLoops)
    		{
    			System.out.println("Did not converge");
    			keepGoing = false;
    		}
    	}
    	return estimate; 
    }
    
    /**
     * Integrates the function using Simpsons Rule
     * 
     * @param dLeft The left x value
     * @param dRight the right x value
     * @param dPrecision the amount of precision 
     * @param iMaxLoops	The number of time to attempt integration		
     * @return
     */
    public double integrateSimpsonsRule( double dLeft, double dRight,
            double dPrecision, int iMaxLoops )
    {
    	int numTrap = 1; 
    	double totalWidth = dRight - dLeft;
    	double mid = dLeft + dRight / 2;
    	double estimate = ((this.a1calculate(dLeft) +  this.a1calculate(dRight) + 4*mid) * totalWidth) / 6;
    	int numLoops = 0;
    	boolean keepGoing = true; 
    	
    	while(keepGoing)
    	{
    		numLoops ++;
    		double oldEstimate = estimate;
    		
    		numTrap *= 2;
    		double width = totalWidth / numTrap; 
    		estimate = 0;
    		
    		for (int i = 0; i < numTrap; i++)
    		{
    			double currentLeft = dLeft + i * width;
    			double currentRight = currentLeft + width;
    			double currentArea = (this.a1calculate(currentLeft) + this.a1calculate(currentRight));
    			estimate += currentArea; 
    		}
    		estimate *= 0.5 * width; 
    		double error = Math.abs((estimate - oldEstimate) / estimate);
    		
    		if(error <= dPrecision)
    		{
    			keepGoing = false;
    		}
    		else if (numLoops >= iMaxLoops)
    		{
    			System.out.println("Did not converge");
    			keepGoing = false;
    		}
    	}
    	return estimate; 
    }
}























