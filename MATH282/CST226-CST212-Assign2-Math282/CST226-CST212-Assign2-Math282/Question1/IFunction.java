package LO4Itegration;

/**
 *  Interface for function class to be used for finding zeros, etc.<br>
 *
 * @author   CST 226 CST 212
  */

public interface IFunction
{
    /**
     *  Returns the value of the function at a chosen point<br>
     *
     * @param  x  Value to evaluate function at
     * @return    Value of function at argument
     */
    public double a1calculate( double x );


    /**
     *  Prints table of function values over specified range<br>
     *  by specified step - can paste into Excel and graph<br>
     *
     * @param  dStart  Starting value of x for table of function values
     * @param  dEnd    Ending value of x for table of function values
     * @param  dStep   Amount to increment x for table of function values
     * @return         Nothing
     * @throws         Starting value of range greater than ending value
     * @throws         Increment not positive
     */
    public void printTable( double dStart, double dEnd, double dStep )
            throws IllegalArgumentException;

    /**
     * Integrates the function using the left integration rule 
     * 
     * @param dLeft The left x value
     * @param dRight the right x value
     * @param dPrecision the amount of precision 
     * @param iMaxLoops	The number of time to attempt integration		
     * @return
     */
    public double integrateLeft( double dLeft, double dRight,
            double dPrecision, int iMaxLoops );
    
    /**
     * Integrates the function using the left integration rule with adjustments 
     * to make the integration more efficient 
     * 
     * @param dLeft The left x value
     * @param dRight the right x value
     * @param dPrecision the amount of precision 
     * @param iMaxLoops	The number of time to attempt integration		
     * @return
     */
    public double integrateLeftEfficient( double dLeft, double dRight,
            double dPrecision, int iMaxLoops );
    
    /**
     * Integrates the function using the Trapezoid integration rule
     * 
     * @param dLeft The left x value
     * @param dRight the right x value
     * @param dPrecision the amount of precision 
     * @param iMaxLoops	The number of time to attempt integration		
     * @return
     */
    public double integrateTrapezoidRule( double dLeft, double dRight,
            double dPrecision, int iMaxLoops );
    
    /**
     * Integrates the function using the Trapezoid integration rule with adjustments 
     * to make the integration more efficient 
     * 
     * @param dLeft The left x value
     * @param dRight the right x value
     * @param dPrecision the amount of precision 
     * @param iMaxLoops	The number of time to attempt integration		
     * @return
     */
    public double integrateTrapezoidRuleEfficient( double dLeft, double dRight,
            double dPrecision, int iMaxLoops );
    
    /**
     * Integrates the function using Simpsons Rule
     * 
     * @param dLeft The left x value
     * @param dRight the right x value
     * @param dPrecision the amount of precision 
     * @param iMaxLoops	The number of time to attempt integration		
     * @return
     */
    public double integrateSimpsonsRule( double dLeft, double dRight,
            double dPrecision, int iMaxLoops );
}














