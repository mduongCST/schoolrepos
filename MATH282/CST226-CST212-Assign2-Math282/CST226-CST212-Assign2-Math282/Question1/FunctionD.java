package LO4Itegration;

/**
 *  Sample function for exploring integration<br>
 *  Implements function <br>
 *
 * @author  CST 226 CST 212
 */

public class FunctionD extends ACFunction
{

    /**
     *  Returns the value of a function<br>
     *  for a given x, implementing the abstract method from ACFunction<br>
     *
     * @param  x  Value to evaluate function at
     * @return    Value of function argument
     */
    public double a1calculate( double x )
    {
        return (4/3) * Math.cos(x);
    }
    

}
