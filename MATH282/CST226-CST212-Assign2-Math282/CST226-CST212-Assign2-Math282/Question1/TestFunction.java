package LO4Itegration;

/**
 *  Class with main method for testing functions implementing ACFunction<br>
 *
 * @author  CST 226	CST 212
 */

public class TestFunction
{
    /**
     *  The main program for the TestFunction class<br>
     *
     * @param  args  the command line arguments
     */
    public static void main(String[] args)
    {
        // Test our example function for exploring integration
        IFunction fA = new FunctionA();
        IFunction fB = new FunctionB();
        IFunction fC = new FunctionC();
        IFunction fD = new FunctionD();
        IFunction fE = new FunctionE();
        
        double precision = 0.00001;
        int maxLoops = 30;
        
        //*********************** Function A **********************
        System.out.println("*****Function A******");
        double leftA = 3.0;
        double rightA = 9.0;

    	System.out.println("\nTrying simple left rectangle rule for equation 1(a)");
    	long time1 = System.currentTimeMillis();
    	double result = fA.integrateLeft(leftA, rightA, precision, maxLoops);
    	long time2 = System.currentTimeMillis();
    	System.out.println(result + " found in " + (time2 - time1));
        
    	System.out.println("\nTrying simple left rectangle rule Efficient implementation for equation 1(a)");
    	time1 = System.currentTimeMillis();
    	result = fA.integrateLeftEfficient(leftA, rightA, precision, maxLoops);
    	time2 = System.currentTimeMillis();
    	System.out.println(result + " found in " + (time2 - time1));
        
        System.out.println("\nTrying simple Trapezoid Rule for equation 1(a)");
    	time1 = System.currentTimeMillis();
    	result = fA.integrateTrapezoidRule(leftA, rightA, precision, maxLoops);
    	time2 = System.currentTimeMillis();
    	System.out.println(result + " found in " + (time2 - time1));
        
        System.out.println("\nTrying simple Trapezoid Rule Efficent for equation 1(a)");
    	time1 = System.currentTimeMillis();
    	result = fA.integrateTrapezoidRuleEfficient(leftA, rightA, precision, maxLoops);
    	time2 = System.currentTimeMillis();
    	System.out.println(result + " found in " + (time2 - time1));
    	
    	System.out.println("\nTrying simple Simpsons Rule Efficent for equation 1(a)");
    	time1 = System.currentTimeMillis();
    	result = fA.integrateSimpsonsRule(leftA, rightA, precision, maxLoops);
    	time2 = System.currentTimeMillis();
    	System.out.println(result + " found in " + (time2 - time1));
 
        //*********************** Function B **********************
        System.out.println("\n*****Function B******");
        double leftB = -2.0;
        double rightB = 2.0;
        
    	System.out.println("\nTrying simple left rectangle rule for equation 1(b)");
    	time1 = System.currentTimeMillis();
    	result = fB.integrateLeft(leftB, rightB, precision, maxLoops);
    	time2 = System.currentTimeMillis();
    	System.out.println(result + " found in " + (time2 - time1));
        
    	System.out.println("\nTrying simple left rectangle rule Efficient implementation for equation 1(b)");
    	time1 = System.currentTimeMillis();
    	result = fB.integrateLeftEfficient(leftB, rightB, precision, maxLoops);
    	time2 = System.currentTimeMillis();
    	System.out.println(result + " found in " + (time2 - time1));
        
        System.out.println("\nTrying simple Trapezoid Rule for equation 1(b)");
    	time1 = System.currentTimeMillis();
    	result = fB.integrateTrapezoidRule(leftB, rightB, precision, maxLoops);
    	time2 = System.currentTimeMillis();
    	System.out.println(result + " found in " + (time2 - time1));
        
        System.out.println("\nTrying simple Trapezoid Rule Efficent for equation 1(b)");
    	time1 = System.currentTimeMillis();
    	result = fB.integrateTrapezoidRuleEfficient(leftB, rightB, precision, maxLoops);
    	time2 = System.currentTimeMillis();
    	System.out.println(result + " found in " + (time2 - time1));
        
    	System.out.println("\nTrying simple Simpsons Rule Efficent for equation 1(b)");
    	time1 = System.currentTimeMillis();
    	result = fB.integrateSimpsonsRule(leftB, rightB, precision, maxLoops);
    	time2 = System.currentTimeMillis();
    	System.out.println(result + " found in " + (time2 - time1));
        //*********************** Function C **********************
        System.out.println("\n*****Function C******");
        double leftC = 2.0;
        double rightC = 10.0;
        
    	System.out.println("\nTrying simple left rectangle rule for equation 1(c)");
    	time1 = System.currentTimeMillis();
    	result = fC.integrateLeft(leftC, rightC, precision, maxLoops);
    	time2 = System.currentTimeMillis();
    	System.out.println(result + " found in " + (time2 - time1));
        
    	System.out.println("\nTrying simple left rectangle rule Efficient implementation for equation 1(c)");
    	time1 = System.currentTimeMillis();
    	result = fC.integrateLeftEfficient(leftC, rightC, precision, maxLoops);
    	time2 = System.currentTimeMillis();
    	System.out.println(result + " found in " + (time2 - time1));
        
        System.out.println("\nTrying simple Trapezoid Rule for equation 1(c)");
    	time1 = System.currentTimeMillis();
    	result = fC.integrateTrapezoidRule(leftC, rightC, precision, maxLoops);
    	time2 = System.currentTimeMillis();
    	System.out.println(result + " found in " + (time2 - time1));
        
        System.out.println("\nTrying simple Trapezoid Rule Efficent for equation 1(c)");
    	time1 = System.currentTimeMillis();
    	result = fC.integrateTrapezoidRuleEfficient(leftC, rightC, precision, maxLoops);
    	time2 = System.currentTimeMillis();
    	System.out.println(result + " found in " + (time2 - time1));
    	
    	System.out.println("\nTrying simple Simpsons Rule Efficent for equation 1(c)");
    	time1 = System.currentTimeMillis();
    	result = fC.integrateSimpsonsRule(leftC, rightC, precision, maxLoops);
    	time2 = System.currentTimeMillis();
    	System.out.println(result + " found in " + (time2 - time1));
        //*********************** Function D **********************
        System.out.println("\n*****Function D******");
        double leftD = 0.0;
        double rightD = Math.PI;
        
    	System.out.println("\nTrying simple left rectangle rule for equation 1(d)");
    	time1 = System.currentTimeMillis();
    	result = fD.integrateLeft(leftD, rightD, precision, maxLoops);
    	time2 = System.currentTimeMillis();
    	System.out.println(result + " found in " + (time2 - time1));
        
    	System.out.println("\nTrying simple left rectangle rule Efficient implementation for equation 1(d)");
    	time1 = System.currentTimeMillis();
    	result = fD.integrateLeftEfficient(leftD, rightD, precision, maxLoops);
    	time2 = System.currentTimeMillis();
    	System.out.println(result + " found in " + (time2 - time1));
        
        System.out.println("\nTrying simple Trapezoid Rule for equation 1(d)");
    	time1 = System.currentTimeMillis();
    	result = fD.integrateTrapezoidRule(leftD, rightD, precision, maxLoops);
    	time2 = System.currentTimeMillis();
    	System.out.println(result + " found in " + (time2 - time1));
        
        System.out.println("\nTrying simple Trapezoid Rule Efficent for equation 1(d)");
    	time1 = System.currentTimeMillis();
    	result = fD.integrateTrapezoidRuleEfficient(leftD, rightD, precision, maxLoops);
    	time2 = System.currentTimeMillis();
    	System.out.println(result + " found in " + (time2 - time1));
    	
    	System.out.println("\nTrying simple Simpsons Rule Efficent for equation 1(d)");
    	time1 = System.currentTimeMillis();
    	result = fD.integrateSimpsonsRule(leftD, rightD, precision, maxLoops);
    	time2 = System.currentTimeMillis();
    	System.out.println(result + " found in " + (time2 - time1));
        
        //*********************** Function E **********************
        System.out.println("\n*****Function E******");
        double leftE = -3.0;
        double rightE = 3.0;
        
    	System.out.println("\nTrying simple left rectangle rule for equation 1(e)");
    	time1 = System.currentTimeMillis();
    	result = fE.integrateLeft(leftE, rightE, precision, maxLoops);
    	time2 = System.currentTimeMillis();
    	System.out.println(result + " found in " + (time2 - time1));
        
    	System.out.println("\nTrying simple left rectangle rule Efficient implementation for equation 1(e)");
    	time1 = System.currentTimeMillis();
    	result = fE.integrateLeftEfficient(leftE, rightE, precision, maxLoops);
    	time2 = System.currentTimeMillis();
    	System.out.println(result + " found in " + (time2 - time1));
        
        System.out.println("\nTrying simple Trapezoid Rule for equation 1(e)");
    	time1 = System.currentTimeMillis();
    	result = fE.integrateTrapezoidRule(leftE, rightE, precision, maxLoops);
    	time2 = System.currentTimeMillis();
    	System.out.println(result + " found in " + (time2 - time1));
        
        System.out.println("\nTrying simple Trapezoid Rule Efficent for equation 1(e)");
    	time1 = System.currentTimeMillis();
    	result = fE.integrateTrapezoidRuleEfficient(leftE, rightE, precision, maxLoops);
    	time2 = System.currentTimeMillis();
    	System.out.println(result + " found in " + (time2 - time1));
    
    	System.out.println("\nTrying simple Simpsons Rule Efficent for equation 1(e)");
    	time1 = System.currentTimeMillis();
    	result = fE.integrateSimpsonsRule(leftE, rightE, precision, maxLoops);
    	time2 = System.currentTimeMillis();
    	System.out.println(result + " found in " + (time2 - time1));
    }
    
}
