/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.math.BigInteger;

/**
 * Error Demo:
 * */
public class Main
{
    public static int iFac(int number)
    {
        int fac = 1;
        for (int i = 2; i <= number; i++)
        {
            fac *= i;
        }
        return fac;
    }

    public static long lFac(int number)
    {
        long fac = 1;
        for (int i = 2; i <= number; i++)
        {
            fac *= i;
        }
        return fac;
    }
    
    public static BigInteger biFac(int number)
    {
        BigInteger fac = new BigInteger("1");
        for (int i = 2; i <= number; i++)
        {
            fac = fac.multiply(new BigInteger(Integer.toString(i)));
        }
        return fac;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        for (int i = 1; i <= 25; i++)
        {
            System.out.println(i + "\t" + iFac(i) + "\t" + lFac(i) + "\t" +
                    biFac(i));
        }
        
        double e1 = 0.0;
        for (int n = 0; n < 20; n++)
        {
            e1 += 1.0 / lFac(n);
            System.out.println(n + "\t" + e1);
        }
        
        double e2 = 0.0;
        for (int n = 19; n >= 0; n--)
        {
            e2 += 1.0 / lFac(n);
            System.out.println(n + "\t" + e2);
        }
        System.out.println(Math.E);
    }

}





