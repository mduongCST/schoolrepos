
public class Bisection 
{
	public static double BisecAlg(double x, double dPrecision)
	{
		double test = 0;
		double guess = 0;
		double upper = Math.max(1,x);
		double lower = Math.min(1,x);
		double precision = dPrecision;
		double result = 0;

		if( x < 0)
		{

		}
		else if(x == 0)
		{
			return result;
		}
		else
		{	
			boolean keepGoing = true;
			while(keepGoing)
			{
				guess = (lower + upper) / 2;
				test = guess * guess;

				if(test == x || upper - lower < precision)
				{
					keepGoing = false;
				}
				else
				{
					if(test > x)
					{
						upper = guess;
					}
					else if(test < x)
					{
						lower = guess;
					}
				}

			}
			result = guess;
		}
		return result;
	}



	public static void main( String[] args )
	{
		System.out.println( BisecAlg( 10, 0.1 ) );
		System.out.println( BisecAlg( 0.25, 0.01 ) );
		System.out.println( BisecAlg( -25 ,0.00001 ) );
	}
}
