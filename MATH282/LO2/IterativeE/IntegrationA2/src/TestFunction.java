/**
 *  Class with main method for testing functions implementing ACFunction<br>
 *
 * @author     MATH 282
 */

public class TestFunction
{
    /**
     *  The main program for the TestFunction class<br>
     *
     * @param  args  the command line arguments
     */
    public static void main(String[] args)
    {
        // Test our example function for exploring integration
        IFunction f3 = new FunctionExample();
        System.out.println( "Table of example function for integration," 
                + "f(x) = 1/4 x^2 - 1/400 x^4" );
        f3.printTable( 0.0, 10.0, 1.0 );
        
        // Test our speed function
        IFunction f4 = new FunctionSpeed();
        System.out.println( "Table of speed function," 
                + "f(x) = 1/4 x" );
        f4.printTable( 0.0, 8.0, 1.0 );
        
        
        // Estimate the integral of f3 from x=3 to x=9
        // using one rectangle with the left rectangle rule
        double left = 3.0;
        double right = 9.0;
        double precision = 0.01;
        
        System.out.println( "\nIntegral of f3 from 3 to 9 is   " + 
                f3.integrateLeft(left, right, precision, 30) + "\n");
 
        integrateLeftEfficient(f3, left, right, precision);
    }

    
    /**
     * Purpose: Integrate the given function over the given range
     * using the left rectangle rule (implemented efficiently)
     * 
     * @param f         function to integrate
     * @param left      value of x for the left edge of the range to integrate
     * @param right     value of x for the right edge of the range to integrate
     * @param precision level of precision (as relative error) for final result
     */
    public static void integrateLeftEfficient(IFunction f, double left, double right,
            double precision)
    {
        // Initial approximation with 1 rectangle
        double totalWidth = (right - left);
        double heights = f.calculate(left);
        double oldEstimate = heights * totalWidth;
        System.out.println( "Estimate with 1 rectangle: " + oldEstimate);
        
        // Estimate using many rectangles
    }
    
}
