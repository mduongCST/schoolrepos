/**
 *  Contains sample methods for iterative calculation of e.<br>
 *
 * @author  
 * @version    
 */
public class IterativeE
{

    /**
     *  Returns the factorial of a number as a double<br>
     *  Only fully accurate for numbers from 0 to ???<br>
     *  Will return Infinity for numbers above ???<br>
     *  Will return NaN for negative numbers<br>
     *
     * @param  n  Number to find factorial of
     * @return    Factorial of the argument
     */
    public static double dFact( int n )
    {
        double dResult = 1;

        if ( n < 0 )
        {
            dResult = Double.NaN;
        }
        else
        {
            for ( int i = 2; i <= n; i++ )
            {
                dResult *= i;
            }
        }
        return dResult;
    }


    /**
     *  Returns the factorial of a number as a float<br>
     *  Only fully accurate for numbers from 0 to ???<br>
     *  Will return Infinity for numbers above ???<br>
     *  Will return NaN for negative numbers<br>
     *
     * @param  n  Number to find factorial of
     * @return    Factorial of the argument
     */
    public static float fFact( int n )
    {
        float fResult = 1;

        if ( n < 0 )
        {
            fResult = Float.NaN;
        }
        else
        {
            for ( int i = 2; i <= n; i++ )
            {
                fResult *= i;
            }
        }
        return fResult;
    }


    /**
     *  Iterative method to find Euler's number e<br>
     *  Based on formula e = 1/0! + 1/1! + 1/2! + ...<br>
     *
     * @param  dPrecision  Desired absolute precision for e
     * @return             Approximate value of e
     */
    public static double findIterativeE( double dPrecision )
    {
        double oldEuler = 0.0;
        double newEuler = 1.0;  // initial approximation - one term
        int i = 1;

        // while we are not "close enough"
        // based on absolute error between successive calculations
        while ( Math.abs( newEuler - oldEuler ) > dPrecision )
        {
            oldEuler = newEuler;
            newEuler += 1.0 / dFact( i );  // add a term to get closer
            i++;
            //System.out.println( i + "\t" + newEuler );
            // Remove comments from line above for testing
            // and demonstration of how truncation error decreases
        }
        //System.out.println( "Terms:  " + i );
        //System.out.println( "Result: " + newEuler );
        return newEuler;
    }


    /**
     *  Returns the factorial of a number as an int<br>
     *  Only valid for numbers from 0 to ???<br>
     *
     * @param  n                        Number to find factorial of
     * @return                          Factorial of the argument
     * @throws     ArithmeticException  If n < 0
     */
    public static int iFact( int n ) throws ArithmeticException
    {
        if ( n < 0 )
        {
            throw new ArithmeticException();
        }

        int iResult = 1;

        for ( int i = 2; i <= n; i++ )
        {
            iResult *= i;
        }
        return iResult;
    }


    /**
     *  Returns the factorial of a number as a long<br>
     *  Only valid for numbers from 0 to ???<br>
     *
     * @param  n                        Number to find factorial of
     * @return                          Factorial of the argument
     * @throws     ArithmeticException  If n < 0
     */
    public static long lFact( int n ) throws ArithmeticException
    {
        if ( n < 0 )
        {
            throw new ArithmeticException();
        }

        long lResult = 1;

        for ( int i = 2; i <= n; i++ )
        {
            lResult *= i;
        }
        return lResult;
    }


    /**
     *  [description]
     *
     * @param args
     */
    public static void main( String[] args )
    {
        System.out.println( findIterativeE( 0.01 ) );
        System.out.println( findIterativeE( 0.001 ) );
        System.out.println( findIterativeE( 0.00001 ) );
    }
}

