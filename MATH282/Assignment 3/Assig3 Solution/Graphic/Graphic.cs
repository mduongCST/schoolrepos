﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MatrixNameSpace;

namespace Graphic
{
    public class Graphic
    {


        private Matrix grMatrix;
   
        /// <summary>
        /// Constructor for the grMatrix class
        /// </summary>
        /// <param name="m"></param>
        public Graphic(Matrix m)
        {
            //make sure we are working with a valid matrix
            if (m.Cols != 2)
            {
                throw new ApplicationException("The matrix can have only 2 colummns");
            }
            else
            {
                
                double[,] d2d = new double[m.Rows, 3];
               //Create a new array of Homogeneous coordinates from the passed matrix
                for (int r = 1; r <= m.Rows; r++)
                {
                    d2d[r-1,0] = m.GetElement(r,1);
                    d2d[r-1,1] =  m.GetElement(r,2);
                    d2d[r-1,2] = 1;
                }

                //Use the homogeneous array to assign to this matrix
                this.grMatrix = new Matrix(d2d);
            }

                
        }


        public void Translate(double dCx, double dCy)
        {

            //Display the matrix before calculation     
            Console.WriteLine("The matrix is BEFORE translation\n" + grMatrix.ToString());
            /*
             * create Translation matrix (set up the identity matrix
             * */
            Matrix transMx = TranslateHelper(dCx, dCy);

            //calculate the passed in matrixs' identity matrix
            grMatrix *= transMx;

            //display the identity matrix
            Console.WriteLine("The matrix is AFTER translation\n" + grMatrix.ToString());
        }

        /// <summary>
        /// Method to perform the transformation matrix for rotations calulations.
        /// </summary>
        /// <param name="dAngle">the angle that the graphic is being rotated by.</param>
        /// <param name="dCx">X coordinate of the graphic</param>
        /// <param name="dCy">Y coordinate fo the graphic</param>
        public void Rotate(double dAngle, double dCx, double dCy)
        {

            //Show the matrix before the transformation matrix for rotations
            Console.WriteLine("The matrix is BEFORE rotation\n" + grMatrix.ToString());

            //Calculate the radians from degrees and the Cos and Sin of the angle
            double rad = DegreeToRadian(dAngle);
            double cos = Math.Cos(rad);
            double sin = Math.Sin(rad);

            //Declare the transformation matrix of rotations that will be used in the calculation
            Matrix RotationMx = new Matrix(new double[,] 
                { { cos, -(sin), 0 },  { sin, cos, 0 },  { -dCx * cos - dCy * sin + dCx , 
                        dCx * (sin) - dCy * cos + dCy, 1 } });

            //Perform the rotational calculation
            grMatrix *= RotationMx;

            //Display the matrix after the rotational calculation is done. 
            Console.WriteLine("The matrix is AFTER rotation\n" + grMatrix.ToString());
        }


        /// <summary>
        /// Will do the scaler multiplication on the point that is being passed in. 
        /// </summary>
        /// <param name="dSx">the original X coordinate of the graphic</param>
        /// <param name="dSy">the Scaled value of X coordinate </param>
        /// <param name="dCx">the original Y coordinate</param>
        /// <param name="dCy">the Scaled value of Y coordinate</param>
        public void Scale(double dSx, double dSy, double dCx, double dCy)
        {
            //Display the martix before the calculation is done
            Console.WriteLine("The matrix is BEFORE scaling\n" + grMatrix.ToString());


            Matrix scaleMx = new
                //The matrix for the transformation matrix for scaling 
                Matrix(new double[,] { { dSx, 0, 0 }, { 0, dSy, 0 }, 
                { (-dCx * dSx ) + dCx, (-dCy * dSy ) + dCy, 1 } });

            //Do the Scale multiplication
            grMatrix *= scaleMx;

            //Show the new matrix after the calculation is done
            Console.WriteLine("The matrix is AFTER scaling\n" + grMatrix.ToString());
        }

        //Converts the degree's to radians
        private double DegreeToRadian(double angle)
        {
            return Math.PI * angle / 180.0;
        }

        //Translation matrix
        public Matrix TranslateHelper(double tranX, double tranY)
        {
            //create the identity matrix
            Matrix transMx = new Matrix(new double[,] { { 1, 0, 0 }, { 0, 1, 0 }, 
                { tranX, tranY, 1 } });

            return transMx;

        }





    }
}
