DROP TABLE A3Booking CASCADE CONSTRAINTS;
DROP TABLE A3Guest CASCADE CONSTRAINTS;
DROP TABLE A3Room CASCADE CONSTRAINTS;
DROP TABLE A3Hotel CASCADE CONSTRAINTS;
DROP SEQUENCE A3HotelNo;
DROP SEQUENCE A3GuestNo;
DROP SEQUENCE A3BookingNo;

CREATE TABLE A3Hotel
(hotelNo NUMBER(4) CONSTRAINT HotelPK PRIMARY KEY,
 hotelName VARCHAR2(25),
 city VARCHAR2(20)
);
CREATE TABLE A3Room
(roomNo NUMBER(4),
 hotelNo NUMBER(4) CONSTRAINT RoomHotelFK REFERENCES A3Hotel,
 type VARCHAR2(15),
 price NUMBER(6, 2),
 CONSTRAINT RoomPK PRIMARY KEY (roomNo, hotelNo)
);
CREATE TABLE A3Guest
(guestNo NUMBER(5) CONSTRAINT GuestPK PRIMARY KEY,
 guestName VARCHAR2(50),
 guestEmail VARCHAR2(50)
);
CREATE TABLE A3Booking
(bookingNo NUMBER(6) CONSTRAINT BookingPK PRIMARY KEY,
 hotelNo NUMBER(4) CONSTRAINT BookingHotelNN NOT NULL
  CONSTRAINT BookingHotelFK REFERENCES A3Hotel,
 guestNo NUMBER(5) CONSTRAINT BookingGuestNN NOT NULL
  CONSTRAINT BookingGuestFK REFERENCES A3Guest,
 dateFrom DATE CONSTRAINT BookingDateFromNN NOT NULL,
 dateTo DATE,
 roomNo NUMBER(4),
 CONSTRAINT BookingRoomFK FOREIGN KEY (roomNo, hotelNo) REFERENCES A3Room,
 CONSTRAINT BookingUK UNIQUE (hotelNo, guestNo, dateFrom)
);

CREATE SEQUENCE A3HotelNo
  START WITH 1
  MAXVALUE 9999;
CREATE SEQUENCE A3GuestNo
  START WITH 10001
  MAXVALUE 99999;
CREATE SEQUENCE A3BookingNo
  START WITH 100001
  MAXVALUE 999999;

INSERT INTO A3Hotel (hotelNo, hotelName, city) VALUES (A3HotelNo.NEXTVAL, 'Fall Snow Inn', 'Victoria');
INSERT INTO A3Hotel (hotelNo, hotelName, city) VALUES (A3HotelNo.NEXTVAL, 'Summer Fog Inn', 'Calgary');
INSERT INTO A3Hotel (hotelNo, hotelName, city) VALUES (A3HotelNo.NEXTVAL, 'Fall Lightning Motel', 'Montreal');
INSERT INTO A3Hotel (hotelNo, hotelName, city) VALUES (A3HotelNo.NEXTVAL, 'Summer Snow Inn', 'Regina');
INSERT INTO A3Hotel (hotelNo, hotelName, city) VALUES (A3HotelNo.NEXTVAL, 'Spring Snow Hotel', 'Vancouver');
INSERT INTO A3Hotel (hotelNo, hotelName, city) VALUES (A3HotelNo.NEXTVAL, 'Fall Wind Inn', 'Quebec City');
INSERT INTO A3Hotel (hotelNo, hotelName, city) VALUES (A3HotelNo.NEXTVAL, 'Winter Thunder Suites', 'St. John''s');
INSERT INTO A3Hotel (hotelNo, hotelName, city) VALUES (A3HotelNo.NEXTVAL, 'Winter Lightning Centre', 'Edmonton');
INSERT INTO A3Hotel (hotelNo, hotelName, city) VALUES (A3HotelNo.NEXTVAL, 'Spring Fog Suites', 'Saskatoon');
INSERT INTO A3Hotel (hotelNo, hotelName, city) VALUES (A3HotelNo.NEXTVAL, 'Winter Wind Inn', 'Fredricton');

INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (336, 1, 'Deluxe Suite', 150);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (638, 1, 'Single', 99);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (1204, 1, 'Double', 110);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (1214, 1, 'Single', 99);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (913, 2, 'Deluxe Suite', 159);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (1119, 2, 'Single', 100);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (1024, 3, 'Single', 100);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (1302, 3, 'Kitchenette', 125);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (1538, 3, 'Single', 100);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (712, 4, 'Deluxe Suite', 150);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (1302, 4, 'Deluxe Suite', 150);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (513, 5, 'Single', 99);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (1537, 5, 'Deluxe Suite', 145);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (319, 6, 'Single', 110);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (635, 6, 'Kitchenette', 140);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (938, 6, 'Single', 110);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (1326, 6, 'Double', 120);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (207, 7, 'Double', 99);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (831, 7, 'Kitchenette', 125);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (1003, 7, 'Single', 99);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (1120, 7, 'Double', 115);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (1532, 7, 'Kitchenette', 125);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (322, 8, 'Double', 125);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (1130, 8, 'Double', 130);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (312, 9, 'Double', 129);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (537, 9, 'Kitchenette', 145);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (803, 9, 'Single', 100);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (1112, 9, 'Kitchenette', 150);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (222, 10, 'Single', 99);
INSERT INTO A3Room (roomNo, hotelNo, type, price) VALUES (516, 10, 'Deluxe Suite', 145);

INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Melisa Pawlowski', 'm.pawlowski@hotmail.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Noreen Wernick', 'n.wernick@gmail.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Allan Callejas', 'a.callejas@mail.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Earnestine Lamacchia', 'e.lamacchia@mail.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Esmeralda Coger', 'e.coger@facebook.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Guy Bisignano', 'g.bisignano@yahoo.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Nannie Graffam', 'n.graffam@yahoo.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Cody Callejas', 'c.callejas@hotmail.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Odessa Esker', 'o.esker@yahoo.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Kelly Kepley', 'k.kepley@mail.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Darryl Jenny', 'd.jenny@yahoo.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Ashlee Rehn', 'a.rehn@yahoo.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Elinor Coger', 'e.coger@hotmail.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Earnestine Eifert', 'e.eifert@hotmail.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Neil Pizana', 'n.pizana@facebook.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Chandra Rehn', 'c.rehn@facebook.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Darryl Bevel', 'd.bevel@shaw.ca');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Lance Graffam', 'l.graffam@gmail.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Louisa Shover', 'l.shover@mail.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Kathrine Tyer', 'k.tyer@mail.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Marcie Sterns', 'm.sterns@facebook.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Marcie Lamacchia', 'm.lamacchia@facebook.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Ashlee Rippeon', 'a.rippeon@mail.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Lance Winegar', 'l.winegar@facebook.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Earnestine Granada', 'e.granada@gmail.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Tia Tyer', 't.tyer@mail.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Sofia Coghlan', 's.coghlan@facebook.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Darryl Drouin', 'd.drouin@shaw.ca');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Tabatha Obert', 't.obert@facebook.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Darryl Nunemaker', 'd.nunemaker@shaw.ca');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Earnestine Nygard', 'e.nygard@mail.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Annabelle Obert', 'a.obert@facebook.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Earnestine Socha', 'e.socha@shaw.ca');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Guy Pizana', 'g.pizana@facebook.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Guy Gaut', 'g.gaut@yahoo.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Emilia Seddon', 'e.seddon@shaw.ca');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Kathrine Hinnant', 'k.hinnant@mail.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Emilia Frenkel', 'e.frenkel@yahoo.com');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Darryl Tyer', 'd.tyer@shaw.ca');
INSERT INTO A3Guest (guestNo, guestName, guestEmail) VALUES (A3GuestNo.NEXTVAL, 'Alejandra Nygard', 'a.nygard@shaw.ca');

INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 7, 10001, '27-Jan-2010', '12-Feb-2010', 831);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 9, 10037, '31-Jan-2010', '13-Feb-2010', 1112);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 6, 10033, '01-Feb-2010', '02-Feb-2010', 635);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 7, 10003, '08-Feb-2010', '17-Feb-2010', 1003);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 6, 10007, '09-Feb-2010', '21-Feb-2010', 319);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 7, 10036, '18-Feb-2010', '28-Feb-2010', 1003);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 8, 10021, '17-Feb-2010', '05-Mar-2010', 322);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 8, 10030, '17-Feb-2010', '26-Feb-2010', 1130);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 6, 10039, '24-Feb-2010', '09-Mar-2010', 319);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 2, 10034, '03-Mar-2010', '08-Mar-2010', 1119);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 2, 10026, '09-Mar-2010', '12-Mar-2010', 1119);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 5, 10019, '07-Mar-2010', '17-Mar-2010', 513);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 1, 10026, '11-Mar-2010', '23-Mar-2010', 1214);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 7, 10014, '13-Mar-2010', '01-Apr-2010', 1003);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 7, 10025, '23-Mar-2010', '25-Mar-2010', 831);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 3, 10021, '23-Mar-2010', '26-Mar-2010', 1302);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 3, 10019, '28-Mar-2010', '08-Apr-2010', 1302);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 4, 10030, '27-Mar-2010', '29-Mar-2010', 1302);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 8, 10040, '28-Mar-2010', '02-Apr-2010', 322);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 2, 10002, '16-Apr-2010', '02-May-2010', 913);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 8, 10006, '24-Apr-2010', '02-May-2010', 1130);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 9, 10036, '06-May-2010', '20-May-2010', 537);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 9, 10022, '08-May-2010', '16-May-2010', 803);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 6, 10018, '13-May-2010', '16-May-2010', 635);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 9, 10014, '13-May-2010', '23-May-2010', 1112);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 1, 10027, '15-May-2010', '20-May-2010', 638);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 3, 10005, '23-May-2010', '28-May-2010', 1024);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 7, 10021, '27-May-2010', '29-May-2010', 831);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 8, 10009, '28-May-2010', '03-Jun-2010', 322);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 2, 10036, '01-Jun-2010', '16-Jun-2010', 1119);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 5, 10026, '13-Jun-2010', '14-Jun-2010', 1537);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 9, 10034, '26-Jun-2010', '03-Jul-2010', 312);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 7, 10010, '30-Jun-2010', '03-Jul-2010', 1532);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 1, 10024, '11-Jul-2010', '16-Jul-2010', 638);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 1, 10032, '20-Jul-2010', '22-Jul-2010', 1214);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 6, 10007, '04-Aug-2010', '24-Aug-2010', 319);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 9, 10039, '08-Aug-2010', '26-Aug-2010', 1112);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 8, 10012, '14-Aug-2010', '17-Aug-2010', 322);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 8, 10009, '18-Aug-2010', '22-Aug-2010', 322);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 7, 10020, '22-Aug-2010', '27-Aug-2010', 1532);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 3, 10009, '25-Aug-2010', '30-Aug-2010', 1538);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 2, 10008, '27-Aug-2010', '16-Sep-2010', 1119);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 3, 10038, '02-Oct-2010', '14-Oct-2010', 1538);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 5, 10015, '03-Oct-2010', '22-Oct-2010', 513);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 9, 10031, '09-Oct-2010', '29-Oct-2010', 537);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 1, 10022, '10-Oct-2010', '19-Oct-2010', 336);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 3, 10017, '15-Oct-2010', '18-Oct-2010', 1538);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 6, 10030, '29-Oct-2010', '06-Nov-2010', 319);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 3, 10014, '29-Oct-2010', '02-Nov-2010', 1024);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 7, 10006, '31-Oct-2010', '18-Nov-2010', 1532);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 7, 10023, '03-Nov-2010', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 3, 10017, '10-Nov-2010', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 2, 10007, '15-Nov-2010', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 1, 10004, '18-Nov-2010', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 7, 10015, '18-Nov-2010', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 9, 10019, '23-Nov-2010', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 9, 10039, '26-Nov-2010', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 7, 10007, '29-Nov-2010', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 7, 10018, '02-Dec-2010', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 1, 10033, '03-Dec-2010', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 4, 10028, '07-Dec-2010', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 9, 10027, '09-Dec-2010', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 9, 10027, '15-Dec-2010', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 8, 10008, '18-Dec-2010', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 1, 10028, '21-Dec-2010', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 7, 10006, '26-Dec-2010', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 8, 10019, '01-Jan-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 5, 10032, '17-Jan-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 6, 10024, '18-Jan-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 9, 10033, '21-Jan-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 6, 10028, '23-Jan-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 9, 10027, '07-Feb-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 9, 10030, '11-Feb-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 8, 10001, '19-Feb-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 2, 10006, '22-Feb-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 6, 10007, '25-Feb-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 1, 10011, '01-Mar-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 6, 10027, '14-Mar-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 1, 10025, '15-Mar-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 7, 10033, '26-Mar-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 5, 10037, '27-Mar-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 8, 10037, '31-Mar-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 8, 10025, '03-Apr-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 6, 10004, '06-Apr-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 1, 10019, '09-Apr-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 6, 10027, '17-Apr-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 7, 10021, '19-Apr-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 6, 10030, '28-Apr-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 1, 10034, '29-Apr-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 1, 10015, '06-May-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 4, 10017, '09-May-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 3, 10038, '12-May-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 9, 10008, '01-Jun-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 7, 10019, '06-Jun-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 7, 10026, '10-Jun-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 3, 10021, '10-Jun-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 7, 10009, '10-Jun-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 9, 10037, '21-Jun-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 5, 10040, '27-Jun-2011', null, null);
INSERT INTO A3Booking (bookingNo, hotelNo, guestNo, dateFrom, dateTo, roomNo) VALUES (A3BookingNo.NEXTVAL, 4, 10008, '30-Jun-2011', null, null);

COMMIT;