-- drop tables if they exist
DROP TABLE A4Customer CASCADE CONSTRAINTS;
DROP TABLE A4Employee CASCADE CONSTRAINTS;
DROP TABLE A4Order CASCADE CONSTRAINTS;
DROP TABLE A4OrderLine CASCADE CONSTRAINTS;
DROP TABLE A4Product CASCADE CONSTRAINTS;
DROP TABLE A4Purchase CASCADE CONSTRAINTS;
DROP TABLE A4PurchLine CASCADE CONSTRAINTS;
DROP TABLE A4Supplier CASCADE CONSTRAINTS;

-- drop the sequences if they exist
DROP SEQUENCE A4CustomerSeq;
DROP SEQUENCE A4EmployeeSeq;
DROP SEQUENCE A4OrderSeq;
DROP SEQUENCE A4ProductSeq;
DROP SEQUENCE A4PurchaseSeq;
DROP SEQUENCE A4SupplierSeq;

-- create the tables
CREATE TABLE A4Customer
(
  CustNo        NUMBER(10) CONSTRAINT A4Customer_CustNo_NN NOT NULL,
  CustFirstName VARCHAR2(20),
  CustLastName  VARCHAR2(30),
  CustStreet    VARCHAR2(30),
  CustCity      VARCHAR2(20),
  CustProv      VARCHAR2(2),
  CustPCode     VARCHAR2(10),
  CustBal       NUMBER(10, 2),
  CONSTRAINT A4Customer_PK PRIMARY KEY (CustNo)
);

CREATE TABLE A4Employee
(
  EmpNo        NUMBER(8) CONSTRAINT A4Employee_EmpNo_NN NOT NULL,
  EmpFirstName VARCHAR2(20),
  EmpLastName  VARCHAR2(30),
  EmpPhone     VARCHAR2(15),
  SupEmpNo     NUMBER(8),
  EmpCommRate  NUMBER(3, 2),
  CONSTRAINT A4Employee_PK PRIMARY KEY (EmpNo),
  CONSTRAINT A4Employee_SupEmpNo_FK
      FOREIGN KEY (SupEmpNo) REFERENCES A4Employee(EmpNo)
);

CREATE TABLE A4Order
(
  OrdNo     NUMBER(8) CONSTRAINT A4Order_OrdNo_NN NOT NULL,
  OrdDate   DATE,
  CustNo    NUMBER(8),
  EmpNo     NUMBER(8),
  OrdName   VARCHAR2(50),
  OrdStreet VARCHAR2(50),
  OrdCity   VARCHAR2(30),
  OrdProv   VARCHAR2(2),
  OrdPCode  VARCHAR2(10),
  CONSTRAINT A4Order_PK PRIMARY KEY (OrdNo),
  CONSTRAINT A4Order_CustNo_FK
      FOREIGN KEY (CustNo) REFERENCES A4Customer (CustNo),
  CONSTRAINT A4Order_EmpNo_FK
      FOREIGN KEY (EmpNo) REFERENCES A4Employee (EmpNo)
);

CREATE TABLE A4Supplier
(
  SuppNo       NUMBER(8) CONSTRAINT A4Supplier_SuppNo_NN NOT NULL,
  SuppName     VARCHAR2(20),
  SuppEmail    VARCHAR2(50),
  SuppPhone    VARCHAR2(14),
  SuppURL      VARCHAR2(50),
  SuppDiscount NUMBER,
  CONSTRAINT A4Supplier_PK PRIMARY KEY (SuppNo)
);

CREATE TABLE A4Product
(
  ProdNo           NUMBER(8) CONSTRAINT A4Product_SuppNo_NN NOT NULL,
  ProdName         VARCHAR2(30),
  SuppNo           NUMBER(8),
  ProdQOH          VARCHAR2(20),
  ProdPrice        NUMBER(8, 2),
  ProdNextShipDate DATE,
  CONSTRAINT A4Product_PK PRIMARY KEY (ProdNo),
  CONSTRAINT A4Product_SuppNo_FK
      FOREIGN KEY (SuppNo) REFERENCES A4Supplier (SuppNo)
);

CREATE TABLE A4OrderLine
(
  OrdNo  NUMBER(8) CONSTRAINT A4OrderLine_OrdNo_NN NOT NULL,
  ProdNo NUMBER(8) CONSTRAINT A4OrderLine_ProdNo_NN NOT NULL,
  Qty    NUMBER(10),
  CONSTRAINT A4OrderLine_PK PRIMARY KEY (OrdNo, ProdNo),
  CONSTRAINT A4OrderLine_OrdNo_FK
      FOREIGN KEY (OrdNo) REFERENCES A4Order (OrdNo),
  CONSTRAINT A4OrderLine_ProdNo_FK
      FOREIGN KEY (ProdNo) REFERENCES A4Product (ProdNo)
);

CREATE TABLE A4Purchase
(
  PurchNo        NUMBER(8) CONSTRAINT A4OrderLine_PurchNo_NN NOT NULL,
  PurchDate      DATE,
  SuppNo         NUMBER(8),
  PurchPayMethod VARCHAR2(6),
  PurchDelDate   DATE,
  CONSTRAINT A4Purchase_PK PRIMARY KEY (PurchNo),
  CONSTRAINT A4Purchase_SuppNo_FK
      FOREIGN KEY (SuppNo) REFERENCES A4Supplier (SuppNo)
);

CREATE TABLE A4PurchLine
(
  PurchNo       NUMBER(8) CONSTRAINT A4PurchLine_PurchNo_NN NOT NULL,
  ProdNo        NUMBER(8) CONSTRAINT A4PurchLine_ProdNo_NN NOT NULL,
  PurchQty      NUMBER(10),
  PurchUnitCost NUMBER(10, 2),
  CONSTRAINT A4PurchLine_PK PRIMARY KEY (PurchNo, ProdNo),
  CONSTRAINT A4PurchLine_PurchNo_FK FOREIGN KEY (PurchNo) REFERENCES A4Purchase (PurchNo),
  CONSTRAINT A4PurchLine_ProdNo_FK FOREIGN KEY (ProdNo) REFERENCES A4Product (ProdNo)
);

-- create the sequences
CREATE SEQUENCE A4CustomerSeq START WITH 1001;
CREATE SEQUENCE A4EmployeeSeq START WITH 101;
CREATE SEQUENCE A4OrderSeq START WITH 100001;
CREATE SEQUENCE A4ProductSeq START WITH 10001;
CREATE SEQUENCE A4PurchaseSeq START WITH 500001;
CREATE SEQUENCE A4SupplierSeq START WITH 5001;

-- populate the tables
INSERT INTO A4Customer VALUES (A4CustomerSeq.NEXTVAL, 'Cleo', 'Skeen', '390 Carling Ave', 'Ottawa', 'ON', 'K1Z 7B5', 230);
INSERT INTO A4Customer VALUES (A4CustomerSeq.NEXTVAL, 'John', 'Blanford', '1198 Rue De La Gare', 'St Felicien', 'QC', 'G8K 1B1', 200);
INSERT INTO A4Customer VALUES (A4CustomerSeq.NEXTVAL, 'Helen', 'Jiron', '3624 rue des Eglises Est', 'Fabre', 'QC', 'J0Z 1Z0', 500);
INSERT INTO A4Customer VALUES (A4CustomerSeq.NEXTVAL, 'Betty', 'Wise', '4879 St Jean Baptiste St', 'Chapais', 'QC', 'G0W 1H0', 200);
INSERT INTO A4Customer VALUES (A4CustomerSeq.NEXTVAL, 'Teena', 'Williams', '2157 Bay St', 'Toronto', 'ON', 'M5J 2R8', 0);
INSERT INTO A4Customer VALUES (A4CustomerSeq.NEXTVAL, 'James', 'Snyder', '116 Wallace St', 'Nanaimo', 'BC', 'V9R 3A8', 85);
INSERT INTO A4Customer VALUES (A4CustomerSeq.NEXTVAL, 'Roy', 'Hoffman', '2564 Davis Dr', 'Orillia', 'ON', 'L3V 1T4', 1500);
INSERT INTO A4Customer VALUES (A4CustomerSeq.NEXTVAL, 'Annie', 'Knapp', '190 Robson St', 'Vancouver', 'BC', 'V6B 3K9', 50);
INSERT INTO A4Customer VALUES (A4CustomerSeq.NEXTVAL, 'Pauline', 'Williams', '4267 Merton St', 'Toronto', 'ON', 'M1L 3K7', 100);
INSERT INTO A4Customer VALUES (A4CustomerSeq.NEXTVAL, 'Keith', 'Cordova', '3055 Water St', 'Kitchener', 'ON', 'N2H 5A5', 0);
INSERT INTO A4Customer VALUES (A4CustomerSeq.NEXTVAL, 'Elisha', 'Bradley', '1275 Victoria Park Ave', 'Toronto', 'ON', 'M2J 3T7', 250);
INSERT INTO A4Customer VALUES (A4CustomerSeq.NEXTVAL, 'Douglas', 'Voyles', '880 Bay St', 'Toronto', 'ON', 'M5J 2R8', 856);
INSERT INTO A4Customer VALUES (A4CustomerSeq.NEXTVAL, 'Ann', 'Morgan', '3279 Pitt St', 'Cornwall', 'ON', 'K6J 3R2', 0);
INSERT INTO A4Customer VALUES (A4CustomerSeq.NEXTVAL, 'Robert', 'Child', '823 Brew Creek Rd', 'Vananda', 'BC', 'V0N 3K0', 500);
INSERT INTO A4Customer VALUES (A4CustomerSeq.NEXTVAL, 'Mari', 'Peterson', '3201 Islington Ave', 'Toronto', 'ON', 'M8V 3B6', 150);
INSERT INTO A4Customer VALUES (A4CustomerSeq.NEXTVAL, 'William', 'Rupp', '1505 Roger St', 'Courtenay', 'BC', 'V9N 2J6', 1000);

ALTER TABLE A4Employee DISABLE CONSTRAINT A4Employee_SupEmpNo_FK;
INSERT INTO A4Employee VALUES (A4EmployeeSeq.NEXTVAL, 'Judith', 'Disanto', '(780) 951-9344', 103, 0.02);
INSERT INTO A4Employee VALUES (A4EmployeeSeq.NEXTVAL, 'Sandra', 'Chandler', '(780) 609-3210', 103, 0.02);
INSERT INTO A4Employee VALUES (A4EmployeeSeq.NEXTVAL, 'Kimberly', 'Pauli', '(780) 649-9220', 105, 0.04);
INSERT INTO A4Employee VALUES (A4EmployeeSeq.NEXTVAL, 'Colin', 'White', '(780) 221-4453', 105, 0.04);
INSERT INTO A4Employee VALUES (A4EmployeeSeq.NEXTVAL, 'Keith', 'Dodson', '(780) 337-7533', null, 0.05);
INSERT INTO A4Employee VALUES (A4EmployeeSeq.NEXTVAL, 'Jennifer', 'Berry', '(780) 677-5509', 103, 0.02);
INSERT INTO A4Employee VALUES (A4EmployeeSeq.NEXTVAL, 'Robert', 'Holiman', '(403) 934-4214', 105, null);
ALTER TABLE A4Employee ENABLE CONSTRAINT A4Employee_SupEmpNo_FK;

INSERT INTO A4Order VALUES (A4OrderSeq.NEXTVAL, TO_DATE('01/18/2011', 'MM/DD/YYYY'), 1001, 102, 'Cleo Skeen', '390 Carling Ave', 'Ottawa', 'ON', 'K1Z 7B5');
INSERT INTO A4Order VALUES (A4OrderSeq.NEXTVAL, TO_DATE('01/18/2011', 'MM/DD/YYYY'), 1011, 106, 'Elisha Bradley', '1275 Victoria Park Ave', 'Toronto', 'ON', 'M2J 3T7');
INSERT INTO A4Order VALUES (A4OrderSeq.NEXTVAL, TO_DATE('02/05/2011', 'MM/DD/YYYY'), 1013, null, 'Ann Morgan', '3279 Pitt St', 'Cornwall', 'ON', 'K6J 3R2');
INSERT INTO A4Order VALUES (A4OrderSeq.NEXTVAL, TO_DATE('01/04/2011', 'MM/DD/YYYY'), 1007, 104, 'Roy Hoffman', '2564 Davis Dr', 'Orillia', 'ON', 'L3V 1T4');
INSERT INTO A4Order VALUES (A4OrderSeq.NEXTVAL, TO_DATE('12/31/2010', 'MM/DD/YYYY'), 1012, 102, 'Colleen Waters', '3385 Glen Long Avenue', 'Toronto', 'ON', 'M6B 1J8');
INSERT INTO A4Order VALUES (A4OrderSeq.NEXTVAL, TO_DATE('01/18/2011', 'MM/DD/YYYY'), 1008, 102, 'Annie Knapp', '190 Robson St', 'Vancouver', 'BC', 'V6B 3K9');
INSERT INTO A4Order VALUES (A4OrderSeq.NEXTVAL, TO_DATE('02/06/2011', 'MM/DD/YYYY'), 1006, null, 'James Snyder', '116 Wallace St', 'Nanaimo', 'BC', 'V9R 3A8');
INSERT INTO A4Order VALUES (A4OrderSeq.NEXTVAL, TO_DATE('01/07/2011', 'MM/DD/YYYY'), 1003, 105, 'Helen Jiron', '3624 rue des Eglises Est', 'Fabre', 'QC', 'J0Z 1Z0');
INSERT INTO A4Order VALUES (A4OrderSeq.NEXTVAL, TO_DATE('01/09/2011', 'MM/DD/YYYY'), 1001, 101, 'Becky Potts', '3243 Eglinton Avenue', 'Toronto', 'ON',  'M4P 1A6');
INSERT INTO A4Order VALUES (A4OrderSeq.NEXTVAL, TO_DATE('01/18/2011', 'MM/DD/YYYY'), 1010, 106, 'Keith Cordova', '3055 Water St', 'Kitchener', 'ON', 'N2H 5A5');
INSERT INTO A4Order VALUES (A4OrderSeq.NEXTVAL, TO_DATE('01/08/2011', 'MM/DD/YYYY'), 1002, null, 'John Blanford', '1198 Rue De La Gare', 'St Felicien', 'QC', 'G8K 1B1');
INSERT INTO A4Order VALUES (A4OrderSeq.NEXTVAL, TO_DATE('01/10/2011', 'MM/DD/YYYY'), 1009, 103, 'Pauline Williams', '4267 Merton St', 'Toronto', 'ON', 'M1L 3K7');
INSERT INTO A4Order VALUES (A4OrderSeq.NEXTVAL, TO_DATE('01/06/2011', 'MM/DD/YYYY'), 1003, 101, 'Helen Jiron', '3624 rue des Eglises Est', 'Fabre', 'QC', 'J0Z 1Z0');
INSERT INTO A4Order VALUES (A4OrderSeq.NEXTVAL, TO_DATE('01/17/2011', 'MM/DD/YYYY'), 1004, 105, 'Paula Pompa', '4879 St Jean Baptiste St', 'Chapais', 'QC', 'G0W 1H0');
INSERT INTO A4Order VALUES (A4OrderSeq.NEXTVAL, TO_DATE('01/15/2011', 'MM/DD/YYYY'), 1015, 103, 'Donald Dishman', '698 Dufferin St', 'Toronto', 'ON', 'M6H 4B6');
INSERT INTO A4Order VALUES (A4OrderSeq.NEXTVAL, TO_DATE('01/18/2011', 'MM/DD/YYYY'), 1016, null, 'William Rupp', '1505 Roger St', 'Courtenay', 'BC', 'V9N 2J6');
INSERT INTO A4Order VALUES (A4OrderSeq.NEXTVAL, TO_DATE('02/14/2011', 'MM/DD/YYYY'), 1006, 102, 'James Snyder', '116 Wallace St', 'Nanaimo', 'BC', 'V9R 3A8');
INSERT INTO A4Order VALUES (A4OrderSeq.NEXTVAL, TO_DATE('01/11/2011', 'MM/DD/YYYY'), 1005, 104, 'Teena Williams', '2157 Bay St', 'Toronto', 'ON', 'M5J 2R8');
INSERT INTO A4Order VALUES (A4OrderSeq.NEXTVAL, TO_DATE('01/18/2011', 'MM/DD/YYYY'), 1015, null, 'Martha Kenney', '4168 Reserve St', 'Arden', 'ON', 'K0H 1B0');
INSERT INTO A4Order VALUES (A4OrderSeq.NEXTVAL, TO_DATE('02/06/2011', 'MM/DD/YYYY'), 1014, 106, 'Robert Child', '823 Brew Creek Rd', 'Vananda', 'BC', 'V0N 3K0');

INSERT INTO A4Supplier VALUES (A4SupplierSeq.NEXTVAL, 'Wireless Modules', 'custrel@wirelessmodules.com', '(709) 621-6503', 'www.wirelessmodules.com', 0.1);
INSERT INTO A4Supplier VALUES (A4SupplierSeq.NEXTVAL, 'Strong Connections', 'help@strongconnex.com', '(250) 205-9862', 'www.strongconnex.com', 0.12);
INSERT INTO A4Supplier VALUES (A4SupplierSeq.NEXTVAL, 'EthEssence', 'ordering@ethessence.com', '(705) 932-9743', 'www.ethessence.com', 0.05);
INSERT INTO A4Supplier VALUES (A4SupplierSeq.NEXTVAL, 'Inter Voice', 'orderdesk@intervoice.com', '(905) 883-9353', 'www.intervoice.com', 0.1);
INSERT INTO A4Supplier VALUES (A4SupplierSeq.NEXTVAL, 'Ultra Components', 'custserv@ultracomponents.com', '(604) 648-9201', 'www.ultracomponents.com', 0.08);
INSERT INTO A4Supplier VALUES (A4SupplierSeq.NEXTVAL, 'Loyalty CX', 'orderhelp@loyaltycx.com', '(604) 591-8308', 'www.loyaltycx.com', 0);

INSERT INTO A4Product VALUES (A4ProductSeq.NEXTVAL, '23" Widescreen LED Monitor', 5001, 12, 169, TO_DATE('02/15/2011', 'MM/DD/YYYY'));
INSERT INTO A4Product VALUES (A4ProductSeq.NEXTVAL, '27" Widescreen LCD Monitor', 5001, 10, 319, TO_DATE('02/15/2011', 'MM/DD/YYYY'));
INSERT INTO A4Product VALUES (A4ProductSeq.NEXTVAL, 'M9450 Multi Laser Printer', 5002, 5, 699, TO_DATE('01/17/2011', 'MM/DD/YYYY'));
INSERT INTO A4Product VALUES (A4ProductSeq.NEXTVAL, '10 Foot Pro USB Cable', 5003, 100, 12, null);
INSERT INTO A4Product VALUES (A4ProductSeq.NEXTVAL, '8-Outlet Surge Protector', 5004, 33, 14.99, null);
INSERT INTO A4Product VALUES (A4ProductSeq.NEXTVAL, 'PhotoFast C443 Printer', 5002, 8, 99, TO_DATE('01/17/2011', 'MM/DD/YYYY'));
INSERT INTO A4Product VALUES (A4ProductSeq.NEXTVAL, 'Color Ink Jet Cartridge', 5002, 24, 38, TO_DATE('01/17/2011', 'MM/DD/YYYY'));
INSERT INTO A4Product VALUES (A4ProductSeq.NEXTVAL, 'Mobile Color Scanner', 5005, 16, 199.99, TO_DATE('01/24/2011', 'MM/DD/YYYY'));
INSERT INTO A4Product VALUES (A4ProductSeq.NEXTVAL, 'Black Ink Jet Cartridge', 5002, 44, 25.69, null);
INSERT INTO A4Product VALUES (A4ProductSeq.NEXTVAL, 'Battery Back-up System', 5006, 12, 89, TO_DATE('01/27/2011', 'MM/DD/YYYY'));

INSERT INTO A4OrderLine VALUES (100001, 10005, 1);
INSERT INTO A4OrderLine VALUES (100002, 10001, 1);
INSERT INTO A4OrderLine VALUES (100002, 10005, 1);
INSERT INTO A4OrderLine VALUES (100003, 10002, 1);
INSERT INTO A4OrderLine VALUES (100004, 10008, 1);
INSERT INTO A4OrderLine VALUES (100005, 10006, 1);
INSERT INTO A4OrderLine VALUES (100005, 10009, 1);
INSERT INTO A4OrderLine VALUES (100005, 10010, 1);
INSERT INTO A4OrderLine VALUES (100006, 10001, 1);
INSERT INTO A4OrderLine VALUES (100006, 10005, 1);
INSERT INTO A4OrderLine VALUES (100006, 10008, 1);
INSERT INTO A4OrderLine VALUES (100007, 10005, 1);
INSERT INTO A4OrderLine VALUES (100007, 10006, 1);
INSERT INTO A4OrderLine VALUES (100008, 10002, 1);
INSERT INTO A4OrderLine VALUES (100008, 10005, 1);
INSERT INTO A4OrderLine VALUES (100009, 10001, 1);
INSERT INTO A4OrderLine VALUES (100009, 10004, 1);
INSERT INTO A4OrderLine VALUES (100009, 10006, 1);
INSERT INTO A4OrderLine VALUES (100010, 10008, 1);
INSERT INTO A4OrderLine VALUES (100010, 10010, 1);
INSERT INTO A4OrderLine VALUES (100011, 10004, 1);
INSERT INTO A4OrderLine VALUES (100011, 10006, 1);
INSERT INTO A4OrderLine VALUES (100011, 10007, 1);
INSERT INTO A4OrderLine VALUES (100012, 10005, 1);
INSERT INTO A4OrderLine VALUES (100012, 10010, 1);
INSERT INTO A4OrderLine VALUES (100013, 10001, 1);
INSERT INTO A4OrderLine VALUES (100013, 10010, 1);
INSERT INTO A4OrderLine VALUES (100014, 10004, 1);
INSERT INTO A4OrderLine VALUES (100014, 10005, 1);
INSERT INTO A4OrderLine VALUES (100014, 10006, 1);
INSERT INTO A4OrderLine VALUES (100014, 10007, 1);
INSERT INTO A4OrderLine VALUES (100014, 10009, 1);
INSERT INTO A4OrderLine VALUES (100015, 10001, 10);
INSERT INTO A4OrderLine VALUES (100016, 10006, 1);
INSERT INTO A4OrderLine VALUES (100016, 10009, 1);
INSERT INTO A4OrderLine VALUES (100017, 10004, 5);
INSERT INTO A4OrderLine VALUES (100017, 10006, 5);
INSERT INTO A4OrderLine VALUES (100017, 10007, 5);
INSERT INTO A4OrderLine VALUES (100017, 10009, 5);
INSERT INTO A4OrderLine VALUES (100018, 10003, 2);
INSERT INTO A4OrderLine VALUES (100018, 10004, 2);
INSERT INTO A4OrderLine VALUES (100018, 10005, 3);
INSERT INTO A4OrderLine VALUES (100019, 10003, 1);
INSERT INTO A4OrderLine VALUES (100019, 10004, 1);
INSERT INTO A4OrderLine VALUES (100019, 10005, 1);
INSERT INTO A4OrderLine VALUES (100020, 10002, 1);
INSERT INTO A4OrderLine VALUES (100020, 10003, 1);
INSERT INTO A4OrderLine VALUES (100020, 10008, 1);

INSERT INTO A4Purchase VALUES (A4PurchaseSeq.NEXTVAL, TO_DATE('01/29/2011', 'MM/DD/YYYY'), 5001, 'Credit',  TO_DATE('02/03/2011', 'MM/DD/YYYY'));
INSERT INTO A4Purchase VALUES (A4PurchaseSeq.NEXTVAL, TO_DATE('01/29/2011', 'MM/DD/YYYY'), 5006, 'PO',  TO_DATE('02/06/2011', 'MM/DD/YYYY'));
INSERT INTO A4Purchase VALUES (A4PurchaseSeq.NEXTVAL, TO_DATE('01/30/2011', 'MM/DD/YYYY'), 5002, 'PO',  TO_DATE('02/04/2011', 'MM/DD/YYYY'));
INSERT INTO A4Purchase VALUES (A4PurchaseSeq.NEXTVAL, TO_DATE('01/29/2011', 'MM/DD/YYYY'), 5003, 'PO',  TO_DATE('02/03/2011', 'MM/DD/YYYY'));
INSERT INTO A4Purchase VALUES (A4PurchaseSeq.NEXTVAL, TO_DATE('02/02/2011', 'MM/DD/YYYY'), 5005, 'PO',  TO_DATE('02/10/2011', 'MM/DD/YYYY'));

INSERT INTO A4PurchLine VALUES (500001, 10001, 10, 100);
INSERT INTO A4PurchLine VALUES (500001, 10002, 10, 200);
INSERT INTO A4PurchLine VALUES (500002, 10010, 10, 45);
INSERT INTO A4PurchLine VALUES (500003, 10003, 15, 450);
INSERT INTO A4PurchLine VALUES (500003, 10006, 10, 50);
INSERT INTO A4PurchLine VALUES (500003, 10007, 25, 21.95);
INSERT INTO A4PurchLine VALUES (500003, 10009, 25, 12.5);
INSERT INTO A4PurchLine VALUES (500004, 10004, 50, 6.5);
INSERT INTO A4PurchLine VALUES (500005, 10008, 15, 99);

COMMIT;
