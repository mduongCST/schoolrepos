SET SERVEROUTPUT ON;

/**
*Purpose: Finds the top 3 producing sales people on staff.
**/
--Declare the variables to be used within the block
DECLARE
  --The name of the employee
  name A4Employee.EmpFirstName%TYPE;
  
  --The total sales of the employee
  totalSales A4Product.ProdPrice%TYPE;
  
  --The total items sold by the employee
  totalItems A4OrderLine.Qty%TYPE;
  
  --Initialize the Cursor 
  CURSOR c_employees IS
    --Selects the first name, the total sales and total items sold by the employee
    SELECT e.EmpFirstName, SUM((p.ProdPrice * ol.Qty) * e.EmpCommRate) AS 
    "Total Sales", SUM(ol.Qty) AS "Total Items" FROM A4Employee e JOIN A4Order o 
    ON e.EmpNo = o.EmpNo
      JOIN A4OrderLine ol ON ol.OrdNo = o.OrdNo
      JOIN A4Product p ON p.ProdNo = ol.ProdNo 
      GROUP BY e.EmpFirstName ORDER BY "Total Sales" DESC;
      
  --Executes the code within the block
  BEGIN
    --Open the employees cursor
    OPEN c_employees;
    --Loop 3 times to find the top 3 employees
    FOR i IN 1..3 LOOP
      --Retrieve the name, total sales and total items and place them into the
      --following variables
      FETCH c_employees INTO name, totalSales, totalItems;
      --Output the results
      DBMS_OUTPUT.PUT_LINE('Name: ' || name || chr(10) || 'Total Sales: ' || 
      totalSales || chr(10) || 'Total Items: ' || totalItems || chr(10) || 
      '---------------------' );
      EXIT WHEN i = 3;
    END LOOP;
    --Close the Cursor
    CLOSE c_employees;
END;
  
    
  
  