SET SERVEROUTPUT ON

/**
*Purpose: Function should use paramters to ensure that there is enough of the 
*specific product in stock.
*Param: ProdNum(Number), Qty(Number) 
*Return: ret_bool(varchar2)
**/
CREATE OR REPLACE
FUNCTION A4OrderCheck
  (ProdNum IN NUMBER, Qty IN NUMBER)
  RETURN varchar2
IS
  --A boolean that will return true, false or null
  ret_bool VARCHAR2(5);
  
  --The quantity on hand
  qoh NUMBER;
  
  --Opens the cursor
  CURSOR c1 IS
    --Selects the Product Quantity On Hand from the A4Product table where Product
    --number is equal to the Product Number passed in
    SELECT ProdQOH FROM A4Product WHERE ProdNo = ProdNum;
--The section that runs the code
BEGIN
  --Open the cursor
  OPEN c1;
  --Fetches the cursor information and places it into the quantity on hand variable
  FETCH c1 INTO qoh;
  
  --If the cursor row exists
  IF c1%found THEN
    --If the quantity on hand is greater than or equal to the quantity passed in 
    IF qoh >= Qty THEN
      --Set the return value to true
      ret_bool := 'true';
    --Else
    ELSE
      --Set the return value to false
      ret_bool := 'false';
    END IF;
  --If a cursor row does not exists
  ELSIF c1%notfound then
    --Return null
    ret_bool := 'null';
  --End the if statement
  END IF;
  --Close the cursor
  CLOSE c1;
--Return value
RETURN ret_bool;
--End it
END;

--------Testing--------

--SELECT A4OrderCheck(10001,10) FROM A4Product;
DECLARE
resultBool VARCHAR2(5);

BEGIN
resultBool := a4ordercheck(10001,5);
DBMS_Output.put_line(resultBool);
END;
