SET SERVEROUTPUT ON

/**
*Purpose: Will execute an action when there is an insert, update or delete
*on the A4OrderLine table.
**/
--Create or replace trigger
CREATE OR REPLACE
TRIGGER OrderTrig
--Before an insert update or delete on A4OrderLine
BEFORE
  INSERT OR
  UPDATE OF OrdNo, ProdNo, Qty OR
  DELETE
ON A4OrderLine
--For each row
FOR EACH ROW
BEGIN
  CASE 
  --The case when inserting
  WHEN INSERTING THEN
      --Update the A4Product table to decrement the qty inserted into an order
      A4UpdateQOH(:new.ProdNo, :new.Qty * -1);
      --When updating an order update the ProdQOH to the new QTY
  WHEN UPDATING THEN
      A4UpdateQOH(:old.ProdNo, :new.Qty);
      --When deleting an order update the prodQOH to have the qty that was deleted
      --added
  WHEN DELETING THEN
      A4UpdateQOH(:old.ProdNo, :old.Qty);
END CASE;
END;

--Test to check if inserting an order will deduct the qty from ProdQOH
INSERT INTO A4OrderLine VALUES (100020, 10010, 5);
--Result: Initially ProdNo 10010 had 12 ProdQOH after the insert it now has 7
SELECT * FROM A4Product;

