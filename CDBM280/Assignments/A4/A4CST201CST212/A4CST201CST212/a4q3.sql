SET SERVEROUTPUT ON

-- Creating a procedure
/**
*Purpose: Procedure will take two parameters, a product number and an adjustment
*number that will modify the stock.
*Params: productNumber(Number), adjustmentAmount(Number)
**/
CREATE OR REPLACE
PROCEDURE A4UpdateQOH(productNumber IN NUMBER, adjustAmount IN NUMBER)
IS
--Variable to store Quantity of product on hand
qohNumber NUMBER;

--Setting cursor to return Quanity of product on hand based on the productNumber
--passed in
CURSOR c1 IS
  SELECT ProdQOH FROM A4Product WHERE ProdNo = productNumber;  
BEGIN
  --Retrieve the qohNumber
  OPEN c1;
  FETCH c1 INTO qohNumber;
  --If the adjust amount makes the quality on hand less than zero
  IF qohNumber + adjustamount < 0 THEN
  --Raise the exception for invalid quantity adjustment
  raise_application_error(-20302, 'Invalid Quantity Adjustment: ' || adjustAmount);
  --If the product was not found
  ELSIF qohNumber IS NULL THEN
  --Raise the exception for invalid product number
  raise_application_error(-20301, 'Invalid Product Number: ' || productNumber);
  --Else if the product has been found and there is a valid quantity
  ELSIF c1%found THEN
    --Update the product to the adjusted amount
    UPDATE A4Product SET ProdQOH = qohNumber + adjustAmount WHERE prodNo = productNumber;
END IF;
END;

--Testing to see if invalid product number exception is returned
EXECUTE A4UpdateQOH(3213, -31);
--Result: ORA-20301: Invalid Product Number: 3213

--Testing to see if invalid quantity adjustment
EXECUTE A4UpdateQOH(10001, -300);
--Result: ORA-20302: Invalid Quantity Adjustment: -300

--Testing to see if adjusting a 
--valid product with a valid adjustment will execute
EXECUTE A4UpdateQOH(10001, 10);
--Result: anonymous block completed
