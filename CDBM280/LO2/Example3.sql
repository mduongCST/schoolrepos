Create Table Library(LibraryID char(10),
                     LibraryName varchar(20),
                     Address varchar(30),
                     
                     Constraint PKLibrary Primary Key (LibraryID)
);

Create Table Book(BookID char(10),
                  Category varchar(20),
                  BookName varchar,
                  
                  Constraint PKBook Primary Key(BookID),
                  Constraint FKBookCat Foreign Key(BookName) References Catalogue
);

Create Table Catalogue(Category varchar(20),
                       LibraryID char(10),
                       
                       Constraint PKCategory Primary Key(Category),
                       Constraint FKLibraryID Foreign Key(LibraryID) References Library
);

Create Table Borrow(BorrowID char(10),
                    BookID char(10),
                    LibraryID char(10),
                    Category char(10),
                    
                    Constraint PKBorrowID Primary Key (BorrowID),
                    Constraint FKBorrow Foreign Key (BookID,LibraryID,Category) References Book, Library, Catalogue
);

Create Table Patron(PatronID char(10),
                    BorrowID char(10),
                    Name varchar(30),
                    
                    Constraint PKPatronID Primary Key (PatronID),
                    Constraint FKPatronBorrow Foreign Key (BorrowID) References Borrow
);
