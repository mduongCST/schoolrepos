Create Table Customer(CustomerID char(10),
                      FName varchar2(25),
                      LName varchar2(40),
                      Address varchar2,
                      
                      Constraint PKCustomerID Primary Key (CustomerID));

Create Table Account(AccountNum char(20),
                     CustomerID char(10) NOT NULL,
                     Balance    number(12,2),
                     
                     Constraint PKAccountNum Primary Key (AccountNum),
                     Constraint FKCustomerID Foreign Key (CustomerID) References Customer);
Create Table Card(CardNum number(12),
                  AccountNum number(10) NOT NULL,
                  ExpireDate Date,
                  PIN number(4),
                  
                  Constraint PKCardNum Primary Key (CardNum),
                  Constraint FKCardAccountNum Foreign Key (AccountNum) References Account);
                  
Create Table CreditCard(CCNum number(10) NOT NULL,
                        CCV number(3),
                        CCBalance number,
                        
                        Constraint PKCCNum Primary Key (CCNum),
                        Constraint FKCCNum Foreign Key (CCnum) References Card);
                        
Create Table DebitCard(DebNum number(10),

                       Constraint PKDebNum Primary Key (DebNum),
                       Constraint FKDebNum Foreign Key (DebNum) References Card);
                       
Create Table Charges(ChargeID char(20),
                     BillID char(20),
                     datePaid Date,
                     Amount Decimal(12,2),
                     CardNum char(20) NOT NULL,
                     
                     Constraint PKChargeID Primary Key (ChargeID)
                     Constraint PK);