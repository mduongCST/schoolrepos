CREATE CourseItem
(
        ModuleID        CHAR(10),
        CourseID        CHAR(10),
        PrecedingMod    VARCHAR(30),
        
        CONSTRAINTS PKModuleID  PRIMARY KEY (ModuleID),
        CONSTRAINTS FKCourseID  FOREIGN KEY (CourseID) REFERENCES Courses
        CONSTRAINTS FKPrecedingMod FOREIGN KEY (PrecedingMod) REFERENCES CourseItem
);

CREATE TABLE Instructor
(
        InstructorID    CHAR(10) NOT NULL,
        UserID          CHAR(10) NOT NULL,
        Office          INTEGER,
        
        CONSTRAINT PKInstructorID PRIMARY KEY (InstructorID),
        CONSTRAINT FKUserID FOREIGN KEY (UserID) REFERENCES SystemUsers
);

CREATE TABLE Assignments
(
        InstructorID    CHAR(10),
        CourseID        CHAR(10),
        Term            CHAR(10),
        Section         CHAR(10),
        AssDate         DATE,
        
        CONSTRAINT PKInstructorID PRIMARY KEY (InstructorID),
        CONSTRAINT PKCourseID PRIMARY KEY (CourseID),
        CONSTRAINT PKTerm PRIMARY KEY (Term),
        CONSTRAINT PKSection PRIMARY KEY (Section),
        CONSTRAINT FKInstructorID FOREIGN KEY (InstructorID) REFERENCES Instructor,
	CONSTRAINT FKCourseID FOREIGN KEY (CourseID) REFERENCES Courses,
	CONSTRAINT FKTerm FOREIGN KEY (Term) REFERENCES Courses,
	CONSTRAINT FKSection FOREIGN KEY (Section) REFERENCES Courses
);

CREATE TABLE Courses
(
        CourseID        CHAR(10),
        Term            INTEGER,
        Section         INTEGER,
        CourseName      VARCHAR(10),
        Description     VARCHAR(10),
        
        CONSTRAINT PKCourseID PRIMARY KEY (CourseID),
        CONSTRAINT PKTerm PRIMARY KEY (Term),
        CONSTRAINT PKSection PRIMARY KEY (Section)
);

CREATE TABLE SystemsUsers
(
        UserID          CHAR(10),
        FirstName       VARCHAR(30),
        LastName        VARCHAR(30),
        Password        VARCHAR(15),
        UserType        VARCHAR(10),
        CONSTRAINT PKUserID PRIMARY KEY (UserID)
        
);

CREATE TABLE Student
(
        StudentID       CHAR(10),
        Year            INTEGER,
        
);
CREATE TABLE Enrollment
(
	StudentID	CHAR(10),
	CourseID	CHAR(10),
        Term            INTEGER,
        Section         INTEGER,
        EnrollDate      DATE,
        Grade           INTEGER,
        CONSTRAINT PKStudentID PRIMARY KEY (StudentID),
        CONSTRAINT PKCourseID PRIMARY KEY (CourseID),
        CONSTRAINT PKTerm PRIMARY KEY (Term),
        CONSTRAINT PKSection PRIMARY KEY (Section),
        CONSTRAINT FKStudentID FOREIGN KEY (StudentID) REFERENCES Student,
	CONSTRAINT FKCourseID FOREIGN KEY (CourseID) REFERENCES Courses,
	CONSTRAINT FKTerm FOREIGN KEY (Term) REFERENCES Courses,
	CONSTRAINT FKSection FOREIGN KEY (Section) REFERENCES Courses

	
);