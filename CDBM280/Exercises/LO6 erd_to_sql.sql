DROP TABLE Major CASCADE CONSTRAINTS;
DROP TABLE Department CASCADE CONSTRAINTS;
DROP TABLE RoomCategories CASCADE CONSTRAINTS;
DROP TABLE Student CASCADE CONSTRAINTS;
DROP TABLE Faculty CASCADE CONSTRAINTS;
DROP TABLE Room CASCADE CONSTRAINTS;
DROP TABLE Grade CASCADE CONSTRAINTS;
DROP TABLE Section CASCADE CONSTRAINTS;
DROP TABLE Course CASCADE CONSTRAINTS;
DROP TABLE Term CASCADE CONSTRAINTS;

CREATE TABLE Major (
	majorID	NUMBER(3)	CONSTRAINT majorPK PRIMARY KEY, 
	name	VARCHAR2(30)
	);

CREATE TABLE Department (
	deptID	NUMBER(1)	CONSTRAINT departmentPK PRIMARY KEY,
	name 	VARCHAR2(25),
	deptHead	NUMBER(3)        
	    CONSTRAINT departmentDeptHeadNN NOT NULL
 	CONSTRAINT departmentDeptHeadUK UNIQUE
         /*CONSTRAINT departmentFacultyFK REFERENCES Faculty(facultyID)*/
	);
/*Note: because the Department and Faculty tables are both Parents and Children of each other at the same time, you must create one and then alter it later to add the FK constraint to the other. */

CREATE TABLE RoomCategories (
	roomType	CHAR(1) 	CONSTRAINT roomCategoriesPK PRIMARY KEY,	
	description VARCHAR2(9)
	);

CREATE TABLE Course (
	courseID	CHAR(6)	CONSTRAINT coursePK PRIMARY KEY, 
	title	VARCHAR2(25), 
	credits	NUMBER(1)	DEFAULT 3
		CONSTRAINT courseCreditsCC CHECK (credits >0)
	);

CREATE TABLE Term (
	termID	CHAR(4)	CONSTRAINT termPK PRIMARY KEY, 
	description	VARCHAR2(15), 
	startDate	DATE, 
	endDate	DATE
	);

CREATE TABLE Room (
	roomID	NUMBER(2)	CONSTRAINT roomPK PRIMARY KEY, 
	building	VARCHAR2(7), 
	roomNo	NUMBER(3), 
	capacity	NUMBER(2)      
               CONSTRAINT roomCapacityCC CHECK ( capacity > 0 ), 
	roomType 	CHAR(1)	
             CONSTRAINT roomRoomCategoriesFK REFERENCES 
		RoomCategories(roomType)
     	CONSTRAINT roomRoomTypeNN NOT NULL
	);
CREATE TABLE Faculty (
	facultyID	NUMBER(3)	CONSTRAINT facultyPK PRIMARY KEY, 
	firstName	VARCHAR2(10), 
	lastName	VARCHAR2(10), 
	phone	NUMBER(3), 
	deptID	NUMBER(1) CONSTRAINT facultyDeptIDNN NOT NULL
		CONSTRAINT facultyDepartmentFK REFERENCES Department(deptID), 
	roomID NUMBER(2) CONSTRAINT facultyRoomIDNN NOT NULL
		CONSTRAINT facultyRoomFK REFERENCES Room(roomID) 
	);

CREATE TABLE Student (
	studentID	CHAR(5)	CONSTRAINT studentPK PRIMARY KEY,
	firstName	VARCHAR2(10), 
	lastName	VARCHAR2(10), 
	street	VARCHAR2(20), 
	city	VARCHAR2(10), 
	province	CHAR(2)	DEFAULT 'SK', 
	pCode	CHAR(6), 
	phone	NUMBER(10), 
	birthdate	DATE, 
	advisorID 	NUMBER(3)
		CONSTRAINT studentFacultyFK REFERENCES Faculty(facultyID), 
	startTerm 	CHAR(4) 
		CONSTRAINT studentTermFK REFERENCES Term(termID)
		CONSTRAINT studentStartTermNN NOT NULL, 
	majorID 	NUMBER(3)
		CONSTRAINT studentMajorFK REFERENCES Major(majorID)
	);

CREATE TABLE Section (
	sectionID	NUMBER(4)	CONSTRAINT sectionPK PRIMARY KEY, 
	sectionNumber NUMBER(2), 
	day	CHAR(2)	
	  CONSTRAINT sectionDayCC CHECK ((day = 'MW') OR (day = 'TR') OR (day = 'F')), 
	startTime	DATE, 
	endTime	DATE, 
	capacity	NUMBER(2)	
 		CONSTRAINT sectionCapacityCC CHECK (capacity >= 0), 
	actualEnrollment NUMBER(2)
		CONSTRAINT sectionEnrollmentCC 
			CHECK (actualEnrollment >= 0), 
	termID	CHAR(4)	CONSTRAINT sectionTermIDNN NOT NULL
		CONSTRAINT sectionTermFK REFERENCES Term(termID),
	courseID 	CHAR(6)	CONSTRAINT sectionCourseIDNN NOT NULL
		CONSTRAINT sectionCourseFK REFERENCES Course(courseID), 
	facultyID	NUMBER(3)	
		CONSTRAINT sectionFacultyFK REFERENCES Faculty(facultyID), 
	roomID	NUMBER(2)	CONSTRAINT sectionRoomIDNN NOT NULL
		CONSTRAINT sectionRoomFK REFERENCES Room(roomID),
    CONSTRAINT Section_Time_cc CHECK(startTime < endTime)
	);
  /* the above check constraint allows us to compare two columns, must be defined
  at the end of the create table statement, or in an alter table statement */

CREATE TABLE Grade (
	sectionID	NUMBER(4)	
		CONSTRAINT gradeSectionFK REFERENCES Section(sectionID), 
	studentID	CHAR(5) 
		CONSTRAINT gradeStudentFK REFERENCES Student(studentID), 
	midterm	CHAR(1), 
	final	CHAR(1),
	CONSTRAINT gradePK PRIMARY KEY (sectionID, studentID)
	);

ALTER TABLE Department
ADD CONSTRAINT departmentDeptHeadFK FOREIGN KEY (deptHead) REFERENCES Faculty (facultyID);
