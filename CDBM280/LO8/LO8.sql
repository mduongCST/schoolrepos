--Create a view that displays the names of authors
--who live in Oakland CA and their books (titleID, title)

-- first step, figure out the query

SELECT auFName, auLName, t.titleID, t.title FROM Author A 
JOIN TitleAuthor TA ON A.auID = TA.auID
JOIN Title t ON T.titleID = TA.titleID
WHERE city = 'Oakland' AND state = 'CA';

-- now create a view
CREATE OR REPLACE VIEW Oaklander AS 
SELECT auFName, auLName, t.titleID, t.title FROM Author A 
JOIN TitleAuthor TA ON A.auID = TA.auID
JOIN Title t ON T.titleID = TA.titleID
WHERE city = 'Oakland' AND state = 'CA';

--using our new view
SELECT * FROM Oaklander;

SELECT * FROM Oaklander ORDER BY Title;

SELECT auLName FROM Oaklander GROUP BY auLName HAVING COUNT(*) > 1;

DESC Oaklander;

--change the view definition using column aliases in the CREATE statement
CREATE OR REPLACE VIEW Oaklander
(FirstName, LastName, titleID, title)
AS 
SELECT auFName, auLName, t.titleID, t.title FROM Author A 
  JOIN TitleAuthor TA ON A.auID = TA.auID
  JOIN Title t ON T.titleID = TA.titleID
  WHERE city = 'Oakland' AND state = 'CA';

SELECT * FROM OAKLANDER;

--Create aliases in the Select clause
CREATE OR REPLACE VIEW Oaklander AS 
SELECT auFName as First, auLName as Last, t.titleID, t.title FROM Author A 
  JOIN TitleAuthor TA ON A.auID = TA.auID
  JOIN Title t ON T.titleID = TA.titleID
  WHERE city = 'Oakland' AND state = 'CA';
DESC OAKLANDER;

--IS THIS VIEW UPDATEABLE
INSERT INTO Oaklander VALUES ('Mitch', 'Duong', 'OD100', 'OVERLORD DESTRUCTION');
-- NO this does not work for a number of reasons described in the course notes.
-- can we update this view??
UPDATE Oaklander set titleID = 'BU7832' where titleID= 'BU1032';

CREATE OR REPLACE VIEW Oaklander
AS
SELECT auFName AS First, auLName AS Last, ta.titleID, title
FROM Author a JOIN TitleAuthor ta ON a.auID = ta.auID
	JOIN Title t ON ta.titleID = t.titleID
WHERE city = 'Oakland'
	AND state = 'CA';

DESC Oaklander;
-- can we update this view??
UPDATE Oaklander set titleID = 'BU7832' where titleID= 'BU1032';
ROLLBACK;
--Delete a value
DELETE FROM Oaklander WHERE titleID = 'BU1032';
ROLLBACK;

CREATE OR REPLACE VIEW Oaklander
AS
SELECT ta.auID, auFName AS First, auLName AS Last, ta.titleID, title, ta.auOrder
FROM Author a JOIN TitleAuthor ta ON a.auID = ta.auID
	JOIN Title t ON ta.titleID = t.titleID
WHERE city = 'Oakland'
	AND state = 'CA';

--Try to insert into this view
INSERT INTO Oaklander(auID, titleID, auOrder)
VALUES ('213-46-8915', 'BU1111', 3);

SELECT * FROM Oaklander;

UPDATE Oaklander SET titleID ='BU7832' WHERE titleID='BU1032';
ROLLBACK;

DELETE FROM Oaklander WHERE titleID='BU1032';
ROLLBACK;

--using a view to simplify and do calculations
/* The accountant wants to run a query to find out to whom royalty check should
be written and for how much
*/
--step one write the query
SELECT A.auID, auLName, royaltyShare, t.titleID, title, price,
loRange, ytdSales, hiRange, royalty, ROUND(price*ytdSales*royalty*royaltyShare,2) AS "Royalty Cheque"
FROM Author A JOIN TitleAuthor TA ON A.auID = TA.auID 
JOIN Title T ON TA.titleID = T.titleID
JOIN RoySched RS ON T.titleID = RS.titleID 
WHERE ytdSales BETWEEN loRange AND hiRange;

--Make the view
CREATE OR REPLACE VIEW RoyaltyCheque AS
SELECT A.auID, auLName, royaltyShare, t.titleID, title, price,
loRange, ytdSales, hiRange, royalty, ROUND(price*ytdSales*royalty*royaltyShare,2) AS "Royalty Cheque"
FROM Author A JOIN TitleAuthor TA ON A.auID = TA.auID 
JOIN Title T ON TA.titleID = T.titleID
JOIN RoySched RS ON T.titleID = RS.titleID 
WHERE ytdSales BETWEEN loRange AND hiRange;

SELECT * FROM RoyaltyCheque;

--Write a query to figure out how much each individual author is owed.
-- require the author id, author last name and amount owed
SELECT A.auID, auLName, SUM(ROUND(price*ytdSales*royalty*royaltyShare,2)) AS "Royalty Cheque"
FROM Author A JOIN TitleAuthor TA ON A.auID = TA.auID 
JOIN Title T ON TA.titleID = T.titleID
JOIN RoySched RS ON T.titleID = RS.titleID 
WHERE ytdSales BETWEEN loRange AND hiRange GROUP BY A.auID, auLName;

