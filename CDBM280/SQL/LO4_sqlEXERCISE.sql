DROP TABLE SalesPeople CASCADE CONSTRAINTS;
DROP TABLE Customer CASCADE CONSTRAINTS;
DROP TABLE Orders CASCADE CONSTRAINTS;

CREATE TABLE SalesPeople(
      salesPersonNum NUMBER(4) CONSTRAINT salesPeople_salesPersonNum_pk PRIMARY KEY,
      salesPName VARCHAR2(10) CONSTRAINT salesPeople_salesPName_nn NOT NULL,
      salesPCity VARCHAR2(10),
      commission NUMBER(3,2) DEFAULT 0.10 
      CONSTRAINT salesPeople_commission_cc CHECK ((commission < 1) OR (commission > 0))
      );
      
INSERT INTO CST212.SALESPEOPLE (SALESPERSONNUM, SALESPNAME, SALESPCITY, COMMISSION) VALUES ('1001', 'Peel', 'London', '0.12');
INSERT INTO CST212.SALESPEOPLE (SALESPERSONNUM, SALESPNAME, SALESPCITY, COMMISSION) VALUES ('1002', 'Serres', 'San Jose', '0.13');
INSERT INTO CST212.SALESPEOPLE (SALESPERSONNUM, SALESPNAME, SALESPCITY, COMMISSION) VALUES ('1004', 'Motika', 'London', '0.11');
INSERT INTO CST212.SALESPEOPLE (SALESPERSONNUM, SALESPNAME, SALESPCITY, COMMISSION) VALUES ('1007', 'Rifkin', 'Barcelona', '0.15');
INSERT INTO CST212.SALESPEOPLE (SALESPERSONNUM, SALESPNAME, SALESPCITY, COMMISSION) VALUES ('1003', 'Axelrod', 'New York', '0.10');

CREATE TABLE Customer(
      customerNum NUMBER(4) CONSTRAINT customer_customerNum_pk PRIMARY KEY,
      cusName VARCHAR2(10) CONSTRAINT customer_cusName_nn NOT NULL,
      cusCity VARCHAR2(10),
      cusRating NUMBER(3),
      salesPersonNum NUMBER(4) CONSTRAINT customer_salesPersonNum_fk
                      REFERENCES SalesPeople (salesPersonNum)
      );
      
CREATE TABLE Orders(
      orderNum NUMBER(4) CONSTRAINT orders_orderNum_pk PRIMARY KEY,
      orderAmount NUMBER(9,2) CONSTRAINT orders_orderAmount_nn NOT NULL,
      orderDate DATE CONSTRAINT orders_orderDate_nn NOT NULL,
      customerNum NUMBER(4) CONSTRAINT orders_customerNum_fk REFERENCES Customer (customerNum),
      salesPersonNum NUMBER(4) CONSTRAINT salesPersonNum_fk
                      REFERENCES SalesPeople (salesPersonNum)
      );
      
ALTER TABLE SalesPeople DROP COLUMN commission;

ALTER TABLE SalesPeople ADD (maxSale NUMBER(8,2) DEFAULT 0);

ALTER TABLE SalesPeople DROP CONSTRAINT salesPeople_salesPName_nn;

ALTER TABLE Orders MODIFY (orderDate DEFAULT '01-JAN-2012');
      