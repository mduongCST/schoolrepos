--Create a view that displays the names of authors
--who live in Oakland CA and their books (titleID, title)

-- first step, figure out the query

SELECT auFName, auLName, t.titleID, t.title FROM Author A 
JOIN TitleAuthor TA ON A.auID = TA.auID
JOIN Title t ON T.titleID = TA.titleID
WHERE city = 'Oakland' AND state = 'CA';

-- now create a view
CREATE OR REPLACE VIEW Oaklander AS 
SELECT auFName, auLName, t.titleID, t.title FROM Author A 
JOIN TitleAuthor TA ON A.auID = TA.auID
JOIN Title t ON T.titleID = TA.titleID
WHERE city = 'Oakland' AND state = 'CA';

--using our new view
SELECT * FROM Oaklander;

SELECT * FROM Oaklander ORDER BY Title;

SELECT auLName FROM Oaklander GROUP BY auLName HAVING COUNT(*) > 1;

DESC Oaklander;

--change the view definition using column aliases in the CREATE statement
CREATE OR REPLACE VIEW Oaklander
(FirstName, LastName, titleID, title)
AS 
SELECT auFName, auLName, t.titleID, t.title FROM Author A 
  JOIN TitleAuthor TA ON A.auID = TA.auID
  JOIN Title t ON T.titleID = TA.titleID
  WHERE city = 'Oakland' AND state = 'CA';

SELECT * FROM OAKLANDER;

--Create aliases in the Select clause
CREATE OR REPLACE VIEW Oaklander AS 
SELECT auFName as First, auLName as Last, t.titleID, t.title FROM Author A 
  JOIN TitleAuthor TA ON A.auID = TA.auID
  JOIN Title t ON T.titleID = TA.titleID
  WHERE city = 'Oakland' AND state = 'CA';
DESC OAKLANDER;

--IS THIS VIEW UPDATEABLE
INSERT INTO Oaklander VALUES ('Mitch', 'Duong', 'OD100', 'OVERLORD DESTRUCTION');
-- NO this does not work for a number of reasons described in the course notes.
-- can we update this view??
UPDATE Oaklander set titleID = 'BU7832' where titleID= 'BU1032';

CREATE OR REPLACE VIEW Oaklander
AS
SELECT auFName AS First, auLName AS Last, ta.titleID, title
FROM Author a JOIN TitleAuthor ta ON a.auID = ta.auID
	JOIN Title t ON ta.titleID = t.titleID
WHERE city = 'Oakland'
	AND state = 'CA';

DESC Oaklander;
-- can we update this view??
UPDATE Oaklander set titleID = 'BU7832' where titleID= 'BU1032';
ROLLBACK;
--Delete a value
DELETE FROM Oaklander WHERE titleID = 'BU1032';
ROLLBACK;

CREATE OR REPLACE VIEW Oaklander
AS
SELECT ta.auID, auFName AS First, auLName AS Last, ta.titleID, title, ta.auOrder
FROM Author a JOIN TitleAuthor ta ON a.auID = ta.auID
	JOIN Title t ON ta.titleID = t.titleID
WHERE city = 'Oakland'
	AND state = 'CA';

--Try to insert into this view
INSERT INTO Oaklander(auID, titleID, auOrder)
VALUES ('213-46-8915', 'BU1111', 3);

SELECT * FROM Oaklander;

UPDATE Oaklander SET titleID ='BU7832' WHERE titleID='BU1032';
ROLLBACK;

DELETE FROM Oaklander WHERE titleID='BU1032';
ROLLBACK;

--using a view to simplify and do calculations
/* The accountant wants to run a query to find out to whom royalty check should
be written and for how much
*/
--step one write the query
SELECT A.auID, auLName, royaltyShare, t.titleID, title, price,
loRange, ytdSales, hiRange, royalty, ROUND(price*ytdSales*royalty*royaltyShare,2) AS "Royalty Cheque"
FROM Author A JOIN TitleAuthor TA ON A.auID = TA.auID 
JOIN Title T ON TA.titleID = T.titleID
JOIN RoySched RS ON T.titleID = RS.titleID 
WHERE ytdSales BETWEEN loRange AND hiRange;

--Make the view
CREATE OR REPLACE VIEW RoyaltyCheque AS
SELECT A.auID, auLName, royaltyShare, t.titleID, title, price,
loRange, ytdSales, hiRange, royalty, ROUND(price*ytdSales*royalty*royaltyShare,2) AS "RoyaltyCheck"
FROM Author A JOIN TitleAuthor TA ON A.auID = TA.auID 
JOIN Title T ON TA.titleID = T.titleID
JOIN RoySched RS ON T.titleID = RS.titleID 
WHERE ytdSales BETWEEN loRange AND hiRange;

SELECT * FROM RoyaltyCheque;

--Write a query to figure out how much each individual author is owed.
-- require the author id, author last name and amount owed
SELECT A.auID, auLName, SUM(ROUND(price*ytdSales*royalty*royaltyShare,2)) AS "RoyaltyCheck"
FROM Author A JOIN TitleAuthor TA ON A.auID = TA.auID 
JOIN Title T ON TA.titleID = T.titleID
JOIN RoySched RS ON T.titleID = RS.titleID 
WHERE ytdSales BETWEEN loRange AND hiRange GROUP BY A.auID, auLName;

SELECT auID, auLName, SUM(RoyaltyCheck) FROM RoyaltyCheque
GROUP BY auID, auLName;

/*
An executive of the company wants to know how much revenue each publisher is 
generating in each of the types. Display the publisher's info (pubID, name),
the type, and the total revenue (price * ytdSales), average price, and
average ytdSales
*/

SELECT P.pubID, P.pubName, T.type,
SUM(t.price * T.ytdSales) as revenue, ROUND(AVG(t.price),2) as avgPrice, 
AVG(ytdSales) AS avgSales FROM Title T JOIN Publisher P ON T.pubID = P.pubID 
GROUP BY P.pubID, P.pubName, T.type;

CREATE OR REPLACE VIEW CurrentInfo AS 
SELECT T.pubID, P.pubName, T.type,
SUM(t.price * T.ytdSales) as revenue, ROUND(AVG(t.price),2) as avgPrice, 
AVG(ytdSales) AS avgSales FROM Title T JOIN Publisher P ON T.pubID = P.pubID 
GROUP BY T.pubID, P.pubName, T.type;

SELECT * FROM CurrentInfo;
UPDATE CurrentInfo SET type='business' WHERE type='psychology';

-- demonstrating the effect of changing a table's definition on
-- the view that uses the table
CREATE TABLE AddOn(name CHAR(5), num number(2));
INSERT INTO AddOn VALUES ('one', 1);

-- We are going to create a view using the * operator in the select clause
CREATE OR REPLACE VIEW vAddOn AS
SELECT * FROM AddOn;
Desc Addon;
DESC vAddon

ALTER TABLE Addon ADD status CHAR(1);
INSERT INTO AddOn VALUES ('two', 2, 'X');
--DESC the two tables

SELECT * FROM vAddOn;

-- Make some changes to a coluimn name that is already in the view.
ALTER TABLE AddOn RENAME COLUMN name TO cName;
DESC Addon;
DESC vAddon;
SELECT * FROM vAddOn;

-- Change the definition of the view to refer to the new column name
CREATE OR REPLACE VIEW vAddOn AS
SELECT cname as name, num FROM AddOn;
SELECT * FROM vAddOn;

-- Creating a copy of a table
CREATE TABLE AuthorCopy AS SELECT * FROM Author;
DESC AUTHOR;
DESC AuthorCopy;
SELECT * FROM AuthorCopy;

-- Create views based on other views
CREATE OR REPLACE VIEW v1 AS SELECT auLName, auFName, Phone FROM AuthorCopy
WHERE zip LIKE '94%';
SELECT * FROM v1;

-- CREATE a view that restricts the first view by creating a second view
CREATE OR REPLACE VIEW v2 AS SELECT auLName, Phone FROM v1 where auLname > 'M';
SELECT * FROM v2;

CREATE OR REPLACE VIEW V3 AS SELECT auLName, Phone FROM v2 WHERE auLName = 'MacFeather';
SELECT * FROM v3;

-- from the above 3 views, write one select statement that gives the result of 
-- v3

SELECT auLName, Phone FROM AuthorCopy WHERE zip LIKE '94%' AND auLName = 'MacFeather' AND auLName > 'M';

-- kill the zip column from AuthorCopy
ALTER TABLE AuthorCopy DROP COLUMN zip;
DESC AuthorCopy;
SELECT * FROM V1;
-- update v1 to manage the fact that zip is now gone from the table
CREATE OR REPLACE VIEW v1 AS
SELECT auLName, auFName, phone FROM AuthorCopy 
WHERE city IN ('Berekely', 'San Francisco', 'Oakland');
-- v1 becomes valid, but v2 and v3 are not valid
-- but using the select forces the views to recompile
