/* This is a comment, we'll be creating a table */
CREATE TABLE Project (
  projectID NUMBER(4),
  name VARCHAR2(25),
  department VARCHAR2(100),
  maxHours NUMBER(6,1)
);

/* Show the structure of a table using DESCRIBE or DESC */
DESC Project;

/* Show the names of all tables in your schema by querying the data dictionary */
SELECT table_name FROM User_Tables;

/* Delete a table */
DROP TABLE Project; /* optional: CASCADE CONSTAINTS */

/* Recreate the table with constraints */
CREATE TABLE Project (
  projectID NUMBER(4) PRIMARY KEY,
  name VARCHAR2(25) UNIQUE NOT NULL,
  department VARCHAR2(100) CHECK ((department = 'CST') OR (department='CAD/CAM')),
  maxHours NUMBER(6,1) DEFAULT 100
);

/* view the data dictionary for the constraints in your schema */
DESC User_Constraints;
SELECT constraint_name, table_name, constraint_type FROM User_Constraints
WHERE table_name = 'PROJECT';

SELECT * FROM USER_OBJECTS;

/* recreate the table with named constraints */

DROP TABLE Project;
CREATE TABLE Project (
  projectID NUMBER(4) CONSTRAINT project_projectID_pk PRIMARY KEY,
  name VARCHAR2(25) CONSTRAINT project_name_uk UNIQUE
                    CONSTRAINT project_name_nn NOT NULL,
  department VARCHAR2(100) CONSTRAINT project_department_cc
                      CHECK ((department = 'CST') OR (department='CAD/CAM')),
  maxHours NUMBER(6,1) DEFAULT 100
);

/* requery the data dictionary for the constraints */

SELECT constraint_name, table_name, constraint_type FROM User_Constraints
WHERE table_name = 'PROJECT';

/* recreate the project table with constraints at the end */
DROP TABLE Project;
CREATE TABLE Project (
  projectID NUMBER(4),
  name VARCHAR2(25) CONSTRAINT project_name_nn NOT NULL,
  department VARCHAR2(100),
  maxHours NUMBER(6,1) DEFAULT 100,
  
  CONSTRAINT project_projectID_pk PRIMARY KEY(projectID),
  CONSTRAINT project_name_uk UNIQUE(name),
  CONSTRAINT project_department_cc CHECK ((department = 'CST') OR (department='CAD/CAM'))
);

/* Create a new table with a foreign key and a composite primary key */

CREATE TABLE Assignment(
    projectID NUMBER(4),
    employeeNumber NUMBER(3),
    hoursWorked NUMBER(5, 2) DEFAULT 10,
    CONSTRAINT Assignment_projectID_fk Foreign Key (projectID)
                        References Project (ProjectID),
    CONSTRAINT Assignment_projID_empNum_pk Primary Key (projectID, employeeNumber)
    );
/* Try dropping the project table now that there is a foreign key */
DROP TABLE Project;

/* Rename project to project1, then we'll rename it back to project */
RENAME Project TO Project1;
RENAME Project1 TO Project;

/* Create an employee table to use to demonstrate ALTER TABLE*/
CREATE TABLE Employee(
  employeeNumber NUMBER(3),
  firstName VARCHAR2(15) CONSTRAINT Employee_firstName_nn NOT NULL,
  lastName VARCHAR2(30) CONSTRAINT Employee_lastName_nn NOT NULL
  );
  
/* Add a new field to employee */
ALTER TABLE Employee ADD (middleName VARCHAR2(15) DEFAULT 'NA');
  
/* Remove the new field */
ALTER TABLE Employee DROP COLUMN middleName;
  
/* Add a new constraint - primary key to Employee table */
ALTER TABLE Employee ADD CONSTRAINT Employee_employeeNumber_pk PRIMARY KEY (employeeNumber);
  
/* Add a foreign key to assignment */
ALTER TABLE Assignment ADD CONSTRAINT Assignment_empNum_fk 
                  FOREIGN KEY (employeeNumber)
                  REFERENCES Employee(employeeNumber);
                  
/* Remove the not null constraint on firstName */
ALTER TABLE Employee DROP CONSTRAINT Employee_firstName_nn;

ALTER TABLE Employee MODIFY (lastName VARCHAR(50));