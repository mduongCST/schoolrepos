--SET SERVEROUTPUT ON
--DECLARE 
--  vNumber BINARY_INTEGER;
--BEGIN
--  vNumber := TO_NUMBER('&Enter_a_Number');
--  IF MOD(vNumber, 2) = 0 THEN
--    DBMS_OUTPUT.PUT_LINE('The number is even');
--  ELSIF vNumber < 5 THEN
--    DBMS_OUTPUT.PUT_LINE('The number is small');
--    ELSE
--    DBMS_OUTPUT.PUT_LINE('The number is is a large odd number');
--  END IF;
--END;

--DECLARE
--  x NUMBER:= 0;
--  counter NUMBER:= 0;
--BEGIN
--  FOR i IN 1..4 LOOP
--    x:=x+1000;
--    counter:= counter + 1;
--    dbms_output.put_line(x || ' ' || counter || ' in outer loop');
--      DECLARE
--        x NUMBER:= 0;
--      BEGIN
--        FOR i in 1..4 LOOP
--        x:=x+1;
--        counter:= counter +1;
--        DBMS_OUTPUT.PUT_LINE(x || ' ' || counter || ' in inner loop');
--        END LOOP;
--      END;
--  END LOOP;
--END;
  
--DECLARE
--  grade VARCHAR2(1) := UPPER('&grade');
--BEGIN
--  CASE grade
--  WHEN 'A' THEN
--    DBMS_OUTPUT.PUT_LINE('Excellent');
--  WHEN 'B' THEN
--    DBMS_OUTPUT.PUT_LINE('Very Good');
--  WHEN 'C' THEN
--    DBMS_OUTPUT.PUT_LINE('Good');
--  WHEN 'D' THEN
--    DBMS_OUTPUT.PUT_LINE('Fair');
--  WHEN 'F' THEN
--    DBMS_OUTPUT.PUT_LINE('Poor');
--  ELSE
--    DBMS_OUTPUT.PUT_LINE('No Such Grade');
--  END CASE;
--END;

--EXAMPLE 1: Insert a record into a table
--1.	Create a record based on the publisher table.  Set the columns of the record as:
--a.	Pubid: 300
--b.	Pubname: SIAST Publishers
--c.	Address: 1330 Idylwyld Dr
--d.	City: Saskatoon
--e.	State: SK
--2.	Insert the record into the publisher table

DECLARE
  publisher_rec publisher%ROWTYPE;
BEGIN
-- department_id, department_name, and location_id are the table columns
-- The record picks up these names from the %ROWTYPE
-- Using the %ROWTYPE means we can leave out the column list
-- (department_id, department_name, and location_id) from the INSERT statement
publisher_rec.pubid :=300;
publisher_rec.pubname := 'SIAST Publishers';
publisher_rec.Address := '1330 Idylwyld Dr';
publisher_rec.City := 'Saskatoon';
publisher_rec.State := 'SK';
INSERT INTO Publisher VALUES publisher_rec;
END;

SELECT * FROM Publisher;



--EXAMPLE 2: Update a table 
--1.	We are going to change the Address and the City of the record we just created. Create a record based on the publisher table.  Set the columns of the record as:
--a.	Pubid: 300
--b.	Pubname: SIAST Publishers
--c.	Address: Queen Street
--d.	City: Regina
--e.	State: SK
--2.	Update the record we created in Example 1


DECLARE 
  publisher_rec publisher%ROWTYPE;
BEGIN
publisher_rec.pubID :=300;
publisher_rec.pubName := 'SIAST Publishers';
publisher_rec.address := 'Queen Street';
publisher_rec.City := 'Regina';
publisher_rec.State := 'SK';


UPDATE Publisher SET ROW = publisher_rec WHERE pubID = 300;
END;


--
--EXAMPLE 3: Using an Unnamed Systems Exception
--
--Let�s consider the publisher table and Title table from sql joins. Here pubid is a primary key in the 
--publisher table and a foreign key in the title table. If we try to delete a pubid from the publisher
--table when it has child records in title table an exception will be thrown with oracle code number -2292. 
--We can provide a name to this exception and handle it in the exception section as given below.
--1.	Declare the exception
--2.	Delete a record from the publisher table
--a.	Attempt it with a record that has a child in the title table (example where pubid=1389)
--b.	Attempt it with a record that doesn�t have a child in the title table. (example where pubid=300)
--3.	In the Exception area of the code block, look for the exception that you declared and print to the
--    screen that �Child records are present for this publisher id.�);



DECLARE 
  Child_rec_exception EXCEPTION; 
  PRAGMA 
   EXCEPTION_INIT (Child_rec_exception, -2292); -- (-2292) is the 
BEGIN 
  Delete FROM publisher where pubid= 1389; 
  --1389 � a record that has an existing child
  --300 � a record that doesn't have an existing child
EXCEPTION 
   WHEN Child_rec_exception 
   THEN Dbms_output.put_line('Child records are present for this publisher id.'); 
END; 

--EXAMPLE 4: User-defined exceptions
--Let�s consider the salesdetail and title tables from sql joins to explain user-defined exception. 
--Let�s create a business rule that if the total number of units of any particular book sold is more than 50,
--then it is a huge quantity and a special discount should be provided. We should stop processing and exit the program
--1.	Declare your exception and any variables, cursors, and constants that you will need.
--2.	For each sale, if there has been more than 50 books sold
--� throw an exception stating: �The number shipped of : <book title> is at least 50.
--Special discounts should be provided.  Skipping the remaining records�. 
--3.	If there hasn�t been more than 50 books sold, print out to the screen that �The number of books is below the discount limit�

DECLARE
  huge_quantity EXCEPTION;
  CURSOR sales_quantity is
    SELECT salesdetail.qtyshipped, title.title FROM salesdetail, title WHERE 
  salesdetail.titleid=title.titleid; --create a cursor with 2 columns
  
  quantity salesdetail.qtyshipped%type; --create a record with 1 column
  up_limit CONSTANT salesdetail.qtyshipped%type := 50; --create a constant (variable)
  message VARCHAR2(150); --create a variable to hold our error message
  BEGIN
  --if the # of sales is greater than 50, then we are going to set the error
  --message and raise an error because we want to stop processing (business rule)
  FOR sales_rec in sales_quantity LOOP
    quantity := sales_rec.qtyshipped;
    IF quantity > up_limit THEN
      message := 'The number shipped of: ' || sales_rec.title || 
                ' is at least 50. special discounts should be provided. Skipping
                remaining records ';
      RAISE huge_quantity;
    ELSIF quantity < up_limit THEN
    message := 'the number of books is below the discount limit.';
    END IF;
    dbms_output.put_line(message);
  END LOOP;
EXCEPTION
  WHEN huge_quantity THEN
    dbms_output.put_line(message);
END;

--EXAMPLE 5: Use RAISE_APPLICATION_ERROR
--
--Modifying the above example slightly we can display a error message using RAISE_APPLICATION_ERROR.

DECLARE 
  huge_quantity EXCEPTION; 
  CURSOR sales_quantity is 
  	SELECT salesdetail.qtyshipped, title.title  FROM salesdetail, title where salesdetail.titleid=title.titleid; 
  quantity salesdetail.qtyshipped%type; 
  up_limit CONSTANT salesdetail.qtyshipped%type := 50; 
  message VARCHAR2(150); 
BEGIN 
  FOR sales_rec in sales_quantity LOOP 
    quantity := sales_rec.qtyshipped;
     IF quantity > up_limit THEN 
        RAISE huge_quantity;  
     ELSIF quantity < up_limit THEN 
      message:= 'The number of books is below the discount limit.'; 
     END IF; 
     dbms_output.put_line (message); 
  END LOOP; 
 EXCEPTION 
   WHEN huge_quantity THEN 
      raise_application_error(-20000, 'The number of unit is above the discount limit.'); 
END;

--EXAMPLE 6: Stored Procedure
--NOTE: A stored procedure can basically be like everything we�ve been creating in an anonymous code block.
--1.	Create a procedure for printing out all the book titles in the titles table.
--2.	Run the procedure on the command line. 
--(NOTICE: In the connections panel in SQL Developer, there is an area for procedures.  
--Once you run the code below, a new procedure will be created and will be accessible
--under the Procedure area to run as often as you like.  You can do this by right 
--clicking on the procedure name and choosing �Run� *though it only seems to print 
--to the log and not the output area. You can also choose to edit the item by right
--clicking on the procedure and choosing �Edit��).

CREATE OR REPLACE PROCEDURE title_details_proc
IS 
   CURSOR title_cur IS 
   	SELECT title, price FROM title; 
   title_rec title_cur%rowtype; 
BEGIN 
   IF NOT title_cur%ISOPEN THEN 
      OPEN title_cur; 
   END IF; 
   LOOP 
     FETCH title_cur INTO title_rec; 
     EXIT WHEN title_cur%NOTFOUND; 
     dbms_output.put_line(title_rec.title || ' ' ||title_rec.price); 
  END LOOP; 
	Close title_cur;
END;

execute title_details_proc;

--EXAMPLE 6: Functions
--NOTE: A function can basically be like everything we�ve been creating in 
--an anonymous code block but you now have the ability to pass in variables and you MUST return a value.
--1.	Create a function that will return the title name for only one book (hard code the book id into the function).  
--2.	Call the function from the command line. (NOTICE: In the connections
--panel in SQL Developer, there is an area for functions.  Once you run the code below,
--a new function will be created and will be accessible under the Function area.
--You can also choose to edit the item by right clicking on the procedure and choosing �Edit��).

--create or replace
CREATE OR REPLACE FUNCTION title_func
    RETURN VARCHAR2
IS 
    title_name VARCHAR2(80);
    
BEGIN 
	SELECT title INTO title_name		
		FROM title WHERE titleid = 'PC8888';
	RETURN title_name;
END;


--Then on command line run:


BEGIN
	dbms_output.put_line(title_func);
END;

--Example 6: NOW � modify the example above slightly, this time pass in the id for the book when you�re calling the function.

create or replace
FUNCTION title_func2 (bookid in CHAR)
    RETURN VARCHAR2
IS 
    title_name VARCHAR2(80);
    
BEGIN 
	SELECT title INTO title_name		
		FROM title WHERE titleid = bookid;
	RETURN title_name;
END;

--Then on command line run:
BEGIN
	dbms_output.put_line(title_func2('PC8888'));
end;







