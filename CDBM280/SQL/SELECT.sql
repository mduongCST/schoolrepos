/* show projects with maxHours over 100 */
SELECT * FROM Project WHERE maxHours > 100;

/* combine several expressions together with AND and OR */
SELECT * FROM Project WHERE department = 'Finance' AND maxHours > 100;

/* comparison operators */
/* show employees from the accounting, financing, or marketing departments */
SELECT * FROM Employee WHERE department IN ('Accounting', 'Finance', 'Marketing');

/* show opposite: everyone else */

SELECT * FROM Employee WHERE department NOT IN ('Accounting', 'Finance', 'Marketing');

/* range operators */
/* all employees with a number within 200 and 500 */

SELECT * FROM Employee Where EmpNumber BETWEEN 200 AND 500; /* BETTER */

SELECT * FROM Employee Where EmpNumber >= 200 AND EmpNumber <= 500;

/* LIKE comparison object */
SELECT * FROM Project WHERE prjName LIKE 'Q_ Portfolio Analysis';
SELECT * FROM Project WHERE prjName LIKE 'Q_ Port%';

SELECT * FROM Employee WHERE Phone LIKE '285%';
SELECT * FROM Employee WHERE Phone LIKE '285_____';

/* Checking for NULLs */
/* Employees with NULL phone numbers */
SELECT * FROM Employee WHERE phone IS NULL;
SELECT * FROM Employee WHERE phone IS NOT NULL;

/* calculations in the field list */
SELECT projectID, maxHours, maxHours * 1.1 FROM Project;
/* use an alias to change a field's name in the results */
SELECT projectID, maxHours, maxHours * 1.1 AS "Proposed Max" FROM Project; /* ALWAYS USE DOUBLE QUOTES*/

/* show name : department for each employee */
SELECT * FROM Employee;
SELECT empName || ' : ' || department AS "Dept Info" FROM Employee;

/* sorting results */
SELECT DISTINCT derpartment FROM Employee ORDER BY department;
SELECT DISTINCT derpartment FROM Employee ORDER BY derpartment DESC;

/*sort on multiple fields */

SELECT * FROM Employee Order By department, empName DESC;