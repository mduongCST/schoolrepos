/* single cell functions */ 
/* ex CEIL(number) */

SELECT CEIL(6.7) FROM DUAL;
select ceil(6.3) from dual;

/* ex ROUND(number [, decimal]) */
SELECT ROUND(6.7) FROM DUAL;
SELECT ROUND(6.3) FROM DUAL;
SELECT ROUND(6.74354534, 4) FROM DUAL;

/* character function examples */
/* LPAD(string, length, [, pad string]) */
SELECT LPAD('test', 9) FROM DUAL;
SELECT LPAD('test', 9, '*/') FROM DUAL;
SELECT RPAD('test', 9, '0') FROM DUAL;

/* functions ltrim, rtrim, trim */
SELECT TRIM ('        test           ') FROM DUAL;
SELECT LTRIM('        test           ') FROM DUAL;
SELECT RTRIM('        test           ') FROM DUAL;

SELECT RTRIM(LTRIM('ababababababababababtestabababababab', 'ab'), 'ab') FROM DUAL;

/* ex substr(string, star pos[, length]) note first character postition is 1  */
SELECT SUBSTR('abcdefghijk', 4, 4) FROM DUAL;

/* ex length returns a number value */
SELECT LENGTH('hello world') FROM DUAL;

/* date functions */
/* SYSDATE returns current system data */
SELECT SYSDATE FROM DUAL;

/* ex NEXT_DAY(d, week_day) */
/* next occurence of that week day on the calendar */
SELECT NEXT_DAY(SYSDATE, 'FRIDAY') FROM DUAL;

/* MONTHS BETWEEN */
/* number of months between two dates */
SELECT MONTHS_BETWEEN(SYSDATE, '25-DEC-13') FROM DUAL; 
SELECT MONTHS_BETWEEN(SYSDATE, TO_DATE('12-25-2013', 'MM-DD-YYYY')) FROM DUAL;

/* NVL and COALESCE */
SELECT * FROM Title; /* there's a null in the type field */
SELECT Title, NVL(type, 'missing') FROM Title;

/* GREATEST and LEAST */
SELECT Title, YTDSALES, ADVANCE, GREATEST(Advance, YtdSales) AS "Which is bigger" FROM Title;

/* Aggregate Functions */
/* find total cost of all books */
SELECT SUM(price) FROM Title;

/* find the total cost of all of the books we have sold */
/* pretend ytd sales is a quantity */
SELECT SUM(price * ytdSales) FROM Title;
/* EXAM RELATED */
SELECT TO_CHAR(SUM(price * ytdSales), '$999,999,999.99') AS "Total Sales For All Books" FROM Title;

/* max and min examples */
SELECT MAX(price) FROM Title;

/* multiple aggregates in one query */ 
SELECT MAX(price) AS "Max Price", MIN (price) AS "Min Price", AVG (price) AS "AVG Price" FROM Title;
SELECT MAX(price), MIN(price), ROUND(AVG(PRICE), 2) AS "Average Price", COUNT(*) FROM Title;

/* GROUP BY */
/* List the highest priced book from each publisher */
SELECT pubID, MAX(price) FROM Title GROUP BY pubID;
/* count all authors in each city, also display city and state */
SELECT city, state, COUNT(*) FROM Author GROUP BY City, State;
/* same as above, but only display cities that have more than one author */
SELECT city, state, COUNT(*) FROM Author GROUP BY City, State HAVING COUNT(*) > 1;
/* same as above, but only for cities in California. Sort by city. */
SELECT city, state, COUNT(*)
  FROM Author WHERE city = 'CA'
  GROUP BY city, state HAVING COUNT(*) > 1
  ORDER BY city;

SELECT NVL(type, 'n/a') AS "Book Type", SUM(NVL(ytdSales, 0)) FROM Title GROUP BY NVL(type, 'n/a');

/* Nulls and aggregates */
/* average of the advances for books */
SELECT AVG(NVL(Advance,0)) FROM Title;




/* EXERCISE SELECT SINGLE ROW */
/* Question 1 */
SELECT ONUM, AMT, ODATE FROM ORDERS;
/* Question 2 */
SELECT * FROM CUSTOMERS WHERE SNUM = 1001;
/* Question 3 */
SELECT * FROM SalesPeople ORDER BY city, sname, snum, comm;
/* Question 4 */
SELECT sname, city FROM SalesPeople WHERE comm= .11 AND city = 'London';
/* Question 5 */
SELECT * FROM Customers WHERE NOT rating <= 100 OR City = 'Rome';
/* Question 6 */
SELECT * FROM Orders WHERE (amt < 1000 OR NOT(oDate = TO_DATE('03-OCT-2000', 'DD-MON-YYYY') AND cNum > 2003));
/* Question 7 */
SELECT * FROM Orders WHERE NOT((oDate = TO_DATE('03-OCT-2000', 'DD-MON-YYYY') OR sNum > 1006) AND amt >= 1500);
/* Question 8 */
SELECT sNum, sName, city, comm FROM Salespeople WHERE comm BETWEEN .12 AND .14 AND comm IS NOT NULL;
/* Question 9 */
SELECT * FROM ORDERS WHERE oDate = '03-OCT-00';
SELECT * FROM ORDERS WHERE oDate = '04-OCT-00';
/* Question 10 */
SELECT cName FROM Customers WHERE cName >= 'A' AND cName < 'H';
SELECT cName FROM Customers WHERE SubStr(cName, 1, 1) BETWEEN 'A' AND 'G';
/* Question 11 */
SELECT * FROM Customers WHERE SubStr(cName, 1, 1) = 'C' OR SubStr(cName, 1, 1) = 'c';
/* Question 12 */
SELECT * FROM Orders WHERE amt IS NOT NULL OR amt = 0;

/* EXERCISE - Aggregate Functions, Update and Delete */

/* Question 1 */
SELECT COUNT(*) FROM Orders WHERE oDate = '03-OCT-00';
/* Question 2 */
SELECT MIN(amt) AS "Smallest Order", cNum FROM Orders Group By cNum;
/* Question 3 */
SELECT cName FROM CUSTOMERS WHERE SubStr(cName, 1, 1) = 'G' AND ROWNUM < 2;
/* Question 4 */
SELECT MAX(rating), city FROM Customers Group By City;
/* Question 5 */
SELECT COUNT(Distinct snum) AS snum, oDate FROM  Orders GROUP BY oDate;
/* Question 6 */
SELECT 'For the city ' || RTRIM(city) || ', ' || 'the highest rating is: ' || MAX(rating) AS "City Rating" FROM customers GROUP BY city;
/* Question 7 */
SELECT rating, cName FROM Customers ORDER BY rating DESC, cName;
/* Question 8 */
SELECT SUM(amt), oDate FROM Orders GROUP BY oDate ORDER BY oDate DESC;
/* Question 9 */
UPDATE Customers SET rating = NVL(rating, 0) + 100;
SELECT * FROM Customers;
ROLLBACK;
/* Question 10 */




/* JOINS */

/* Display the publisher of the books */ 
SELECT title, titleID, pubName, Title.pubID
  FROM Title JOIN Publisher ON Title.pubID = Publisher.pubID;
  
/* add aliases for the tables */
SELECT title, titleID, pubName, T.pubID
  FROM Title T JOIN Publisher P ON T.pubID = P.pubID;
  
/* show author, title, and publisher info */
SELECT auLName, title, pubName
  FROM Author A JOIN TitleAuthor TA ON A.auID = TA.auID
                JOIN Title T ON T.titleID = TA.titleID
                JOIN Publisher P ON T.pubID = p.pubID;
                
/* add a WHERE clause */
SELECT auLName, title, pubName
  FROM Author A JOIN TitleAuthor TA ON A.auID = TA.auID
                JOIN Title T ON T.titleID = TA.titleID
                JOIN Publisher P ON T.pubID = p.pubID
  WHERE T.price > 25;
  
/* add sorting */
SELECT auLName, title, pubName
  FROM Author A JOIN TitleAuthor TA ON A.auID = TA.auID
                JOIN Title T ON T.titleID = TA.titleID
                JOIN Publisher P ON T.pubID = p.pubID
  WHERE T.price > 25
  ORDER BY 2; /* ORDER BY title; would work too */
  
  /* show editor's first and last name, title of book edited */
SELECT edFName, edLName, title
  FROM Editor E JOIN TitleEditor TE ON E.edID = TE.edID
                JOIN Title T ON T.titleID = TE.titleID;
/* Outer Joins */
SELECT * FROM Project;
SELECT * FROM Assignment;

/* See All Projects */
/* This is the inner join */
SELECT empName, hoursWorked, prjName, P.projectID
  FROM Employee E JOIN Assignment A ON E.empnumber = A.empnumber
                  JOIN Project P ON A.projectID = P.projectID;
                  
/* as an outer join - show all projects even if unassigned */
SELECT empName, hoursWorked, prjName, P.projectID
  FROM Employee E RIGHT JOIN Assignment A ON E.empnumber = A.empnumber
                  RIGHT JOIN Project P ON A.projectID = P.projectID;

/* as a LEFT join */
SELECT empName, hoursWorked, prjNAme, P.projectID
  FROM Project P LEFT JOIN Assignment A ON P.projectID = A.projectID
                 JOIN Employee E ON A.empNumber = E.empNumber;
                 
/* Self Join */
/* ex: Editors and their bosses */
SELECT edID, edLName, edFName, edBoss
  FROM Editor;

/* selfjoin: */
SELECT E.edID, E.edLName, E.edFName, E.edBoss, B.edLName, B.edFName
  FROM Editor E JOIN Editor B ON E.edBoss = B.edID;
/* add an outer join to see employees without bosses: */
SELECT E.edID, E.edLName, E.edFName, E.edBoss, B.edLName, B.edFName
  FROM Editor E LEFT JOIN Editor B ON E.edBoss = B.edID;
/* see only the bosses (in other words, employees without a boss) */
SELECT E.edID, E.edLName, E.edFName, E.edBoss, B.edLName, B.edFName
  FROM Editor E LEFT JOIN Editor B ON E.edBoss = B.edID
  WHERE E.edBoss IS NULL;
  
/* Set Operators: UNION, INTERSECT, MINUS */
SELECT auFName AS FirstName, auLName, city
  FROM Author WHERE city IN ('Oakland', 'Berkeley');
UNION
SELECT edFName, edLName, city
  FROM Editor WHERE city IN ('Oakland', 'Berkeley')
  ORDER BY 3;
  
/* INTERSECT */
/* What cities do both authors and publishers have in common? */
SELECT city FROM Author
INTERSECT
SELECT city FROM Publisher;

/* cities from authors that don't appear in publishers */
SELECT city FROM Author
MINUS
SELECT city FROM Publisher;

/* publisher cities without authors */
SELECT city FROM Publisher
MINUS
SELECT city FROM Author;

/* Using UNION as an 'IF' statement */
/* You need to produce a list of books showing discount percent and new prices 
Books under $15 are reduced by 20 percent, those between $15 and $25 are reduced by 10 percent,
and those above $25 are reduced by 30 percent*/
SELECT '20% Off' AS Discount, title AS Book, price AS oldPrice, 
          price * 0.80 AS NewPrice 
          FROM Title WHERE price < 15;
          
SELECT '10% Off' AS Discount, title AS Book, price AS oldPRice,
          price * 0.90 AS NewPrice 
          FROM Title WHERE price BETWEEN 15 AND 20;
          
SELECT '30% Off' AS Discount, title AS Book,
          price AS oldPRice, price * 0.70 AS NewPrice
          FROM Title WHERE price > 25;

/* Join and Set Exercises */
/* 1 */
SELECT oNum, cName FROM Orders O JOIN Customers C ON O.cnum = C.cnum;

/* 2 */
SELECT cName, sName, comm FROM Salespeople S JOIN Customers C ON S.snum = C.snum
WHERE comm > 0.12;

/* 3 */
SELECT comm * amt AS , sName 
      FROM Salespeople S JOIN Customers C ON S.snum = C.snum
      S
WHERE C.rating > 100 GROUP BY sName;




/* Sub Queries */
/* Subquery returning a single value */
/* a: */
SELECT City FROM Author WHERE auLName = 'Karsen' AND aufName = 'Livia';
/* b: */
SELECT * FROM Author WHERE city = 'Oakland';
/* c: */
SELECT * FROM Author WHERE city = (SELECT city FROM Author
                            WHERE auLName = 'Karsen' AND auFName = 'Livia');
                            
/* Subquery returning multiple rows */
/* Publisher names of publishers publishing business books */
/* a: Where is the type of book stored? */
SELECT * FROM Title WHERE type ='business';
/* b: 1389, 0736 */
/* c: */
SELECT * FROM Publisher WHERE pubID IN ('1389', '0736');

/* d: */
SELECT * FROM Publisher WHERE pubID IN (
            SELECT pubID FROM Title WHERE type = 'business');
            
/* Example 2: */
/* a: find B&N's pubID */
SELECT pubID FROM Publisher WHERE pubName = 'Binnet and Hardley';
/* b: write the query with hard coded pubID */
SELECT title, ytdSales FROM title WHERE ytdSales > 15000 AND pubID = '0877';
/* c: rewrite as a subquery */
SELECT title, ytdSales FROM title WHERE ytdSales > 15000
  AND pubID = (SELECT pubID FROM Publisher WHERE pubName = 'Binnet and Hardley');

/* Example 3 */
/* List the books that have a price greater or equal tot he price of the book
Straight Talk About Computers - titleID is BU7832 */
/* a) Find "Straight Talk About Computers" price. */
SELECT price FROM Title WHERE TitleID = 'BU7832';
/* b) Find all the books with a price of >= 29.99 */
SELECT * FROM Title WHERE price >= (
             SELECT price FROM Title WHERE TitleID = 'BU7832');
             
/* Example 4 */
/* What if I didn't know that BU7832 was the titleID of Straight Talk About Computers?
Change the subquery in Example 3 */
SELECT * FROM Title WHERE price >= (
             SELECT price FROM Title WHERE TitleID = (
             SELECT titleID FROM Title WHERE title = 'Straight Talk About Computers'));
             
/* Example 5 */
/* List the books that have a price less thant he average price of all books. */
/* Find the average book price */
SELECT AVG(price) FROM Title;
/* Find the books with a price less than the average */
SELECT * FROM Title WHERE price < (SELECT AVG(price) FROM Title);

/* Example 6 */
/* Give the names of the authors that live in states where more than one Author is listed? */
/* Find how many Authors are in each state. */
/* Find which are greater than 1 */
/* Get the names of the authors that live in those states */
/* a */
SELECT state, COUNT(*) FROM Author GROUP BY state;
/* b */
SELECT state, COUNT(*) FROM Author HAVING COUNT(*) > 1 GROUP BY state; 
/* c */
SELECT auFName, auLName, state FROM Author 
WHERE state IN (SELECT state FROM Author GROUP BY state HAVING COUNT(*) > 1);


/* Correlated Subqueries */
/* Example 1 */
SELECT pubName
  FROM Publisher P
  WHERE EXISTS
    (SELECT * 
    FROM Title t
    WHERE t.pubID = p.pubID
      AND type = 'business');


/* Example 2 */
/* Retrieve the name, publisher ID, and ytdSales, of any book whose
ytdSales is above average for that book's publisher. */
SELECT title, pubID, ytdSales FROM Title TOuter
  WHERE ytdSales > (SELECT AVG(ytdSales) FROM Title TInner WHERE TOuter.pubID = TInner.pubID);
  
/* Example 3 */
/* addpubName instead of pubID to ex 2 */

SELECT title, pubName, ytdSales from Title TOuter
  WHERE ytdSales > (SELECT)AVG(ytdSales) FROM Title TInner WHERE ytdSales > (SELECTAVG(ytdS TOuter.pubID = T.Inner.pubID);
  
  
/* join exampel */
/* Search for the names of publishers located in the same city as an author. */
/* we can do this with INTERSECT */
/* as a join */
SELECT DISTINCT pubName FROM Publisher P JOIN Author A ON P.city = A.city;
/* as a simple subquery */
SELECT pubName FROM Publisher WHERE city IN (SELECT city FROM Author);
SELECT city FROM Author;

/* CHALLENGE: write this as a correlated subquery */

/* If we need to see author's names, we need to join a join */
SELECT pubNAme, auFname, auLName
FROM Publisher P JOIN Author A ON P.City = A.city;

/* Example: What publishers have published books that cost more than $35.00
as a join */
SELECT DISTINCT pubName FROM Publisher P JOIN Title T ON P.pubID = T.pubID 
WHERE price > 35;

/* as a subquery */

SELECT DISTINCT pubName FROM Publisher 
WHERE pubID IN (SELECT pubID FROM Title WHERE price > 35);

/* More Examples */
/* What is the name of the book with the highest price? */
SELECT title FROM Title WHERE price IN 
(SELECT max(price) FROM Title) GROUP BY title;

/* Would like to know the names of the books that Eleanore Himmel edited. */
/* as a join */
SELECT title FROM Title T JOIN TitleEditor TE ON T.titleID = TE.titleid JOIN Editor E ON E.edID = TE.edID
WHERE edLNAme = 'Himmel' AND edFName = 'Eleanore';

/* as a subquery */
SELECT title FROM Title WHERE titleID IN 
            (SELECT titleID FROM TitleEditor WHERE edID IN 
            (SELECT edID FROM Editor WHERE edLName = 'Himmel' AND edFName = 'Eleanore'));
            
/* What are the 3 lowest priced books */
SELECT title, price FROM Title ORDER BY Price;

/* STUDY */
SELECT title, price
FROM Title TOut WHERE 3 > 
  (SELECT COUNT(*) FROM Title TIn WHERE TIn.price < TOut.Price) AND price IS NOT NULL;
/* STUDY */


/* STUDY */
/* Salespeople with the highest commission in their city */
SELECT * FROM Salespeople SOut WHERE comm = 
  (SELECT max(comm) FROM Salespeople SIn WHERE SOut.city = SIn.city );
/* STUDY */

/* Testing Nonexistence */
/* Find the names of all the publishers that do not publish business books */
SELECT title FROM Title WHERE type = 'business';

SELECT pubName FROM Publisher;

SELECT pubName FROM Publisher POut WHERE NOT EXISTS
      (SELECT title FROM Title TIn WHERE type = 'business' AND TIn.pubID = POut.pubID);
      
SELECT pubName FROM Publisher POut WHERE 0 =
      (SELECT COUNT(*) FROM Title TIn WHERE type = 'business' AND TIn.pubID = POut.pubID);      
      
/* Find the names of all second authors who live in California
and receive less than 30 percent of the royalties on the books they coauthor */

SELECT * FROM PP.Part;