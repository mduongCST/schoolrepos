CREATE OR REPLACE VIEW A3RoomDetails AS
SELECT H.hotelNo AS hotelNum, H.hotelName AS hotelName, H.city AS location, 
R.roomNo AS roomNum, R.price AS dailyRate, R.price - R.price*0.1 AS discountRate
FROM A3Hotel H JOIN A3Room R ON H.hotelNo = R.hotelNo;

CREATE OR REPLACE VIEW A3RoomsByHotel AS
SELECT H.hotelNo AS hotelNum, H.hotelName AS HotelName, COUNT(R.roomNO) AS RoomNumber
FROM A3Hotel H JOIN A3Room R ON H.hotelNo = R.hotelNo GROUP BY H.hotelNo, hotelName
ORDER BY ROOMNUMBER DESC;

SELECT * FROM A3RoomDetails;
SELECT * FROM A3RoomsByHotel;

DELETE FROM A3RoomDetails WHERE hotelNum = 2;