CREATE TABLE Major(
      majorID NUMBER(3) CONSTRAINT Major_ID_pk PRIMARY KEY,
      name VARCHAR(30) CONSTRAINT Major_name_nn NOT NULL
      );
      
CREATE TABLE Department(
      deptID NUMBER(1) CONSTRAINT Department_deptID_pk PRIMARY KEY,
      name VARCHAR(30) CONSTRAINT Department_name_nn NOT NULL,
      deptHead NUMBER(3) CONSTRAINT Department_deptHead_nn NOT NULL
      );
      
CREATE TABLE RoomCategory(
      roomType VARCHAR(1) CONSTRAINT RoomCategory_roomType_pk PRIMARY KEY,
      description VARCHAR(20) CONSTRAINT RoomCategory_description_nn NOT NULL
      );
      
CREATE TABLE Student(
      studentID NUMBER(5) CONSTRAINT Student_studentID_pk PRIMARY KEY,
      firstName VARCHAR2(30) CONSTRAINT Student_firstName_nn NOT NULL,
      lastName VARCHAR2(30) CONSTRAINT Student_lastName_nn NOT NULL,
      street VARCHAR2(40),
      city VARCHAR2(30),
      province VARCHAR(2),
      pCode VARCHAR(2),
      phone NUMBER(10),
      startTerm VARCHAR(4) CONSTRAINT Student_startTerm_nn NOT NULL
                           CONSTRAINT Student_startTerm_fk REFERENCES Term (termID),
      birthdate DATE,
      advisorID NUMBER(3) CONSTRAINT Student_advisorID_fk REFERENCES Faculty (facultyID),
      majorID NUMBER(3) CONSTRAINT Student_majorID_fk REFERENCES Major (majorID)
      );