/* INSERT */
/* Insert into Projects table */
DESC PROJECT;
/* full syntax */
INSERT INTO Project (projectID, prjName, department, maxHours)
            VALUES (5000, 'Q3 Prep', 'CST', 75);
            
/* quicker syntax, if you know the order of the columns */
INSERT INTO Project VALUES(6000, 'Q4 Development', 'CST', 150);
SELECT * FROM Project;

/* insert into only some of the columns */
INSERT INTO Project (projectID, prjName, maxHours)
            VALUES (7000, 'Q1 Prep', 75);
            
ROLLBACK; /* undoes changes since last commit */   
          
INSERT INTO Project (projectID, maxHours)
            VALUES (7000, 75);
            
SELECT * FROM Project;            
/* use the default value of a column */
INSERT INTO Project (projectID, prjName, department)
            VALUES(5000, 'Q3 Taxes', 'CAD/CAM');

INSERT INTO Project (projectID, prjName, department, maxHours)
            VALUES(8000, 'Q4 Taxes', 'CAD/CAM', DEFAULT);
            
ROLLBACK;

/* Add a null values to a column in a row */
/* if there isn't a default and you leave it out a column will default to null */
/* if there is a default use the NULL keyword */

INSERT INTO Project (projectID, prjName, department, maxHours)
            VALUES(9000, 'Q1 Taxes', 'CAD/CAM', NULL);
            
SELECT * FROM Project;

ROLLBACK;

/* UPDATE and DELETE */
/* Change the city and state for all Publishers in the table - with no WHERE
clause all rows are affected */
UPDATE Publisher SET city='Atlanta', state='GA';
SELECT * FROM Publisher;
ROLLBACK;

/* change only certain rows - use a WHERE clause */
UPDATE Publisher SET Address='521 5th St.', City = 'Los Angeles'
                  WHERE pubID = 1389;
ROLLBACK;

/* The new value that is updated can be the result of an expression */
/* Increase all royalties by 10%. */
SELECT * FROM RoySched;
UPDATE RoySched SET Royalty = Royalty + (Royalty * 0.10);
ROLLBACK;

/* DELETE with where - all rows affected */
DELETE FROM Publisher; /* can't - integrity constraint */
DELETE FROM RoySched;
ROLLBACK;

/* Delete specific rows using the WHERE clause */
DELETE FROM RoySched WHERE royalty > 0.1;
ROLLBACK;

/* Inserting into tables using a select statement */
/* Two ways: */
CREATE TABLE HiPRicedTitles(
  titleID CHAR(6),
  title VARCHAR2(80),
  price NUMBER(9,2)
  );

INSERT INTO HiPricedTitles SELECT titleID, title, price FROM Title WHERE price > 35;

SELECT * FROM HiPricedTitles;

ROLLBACK;
/* alternate way: */
CREATE TABLE HiPricedTitles2 AS SELECT titleID, title, price FROM Title WHERE price > 35;
SELECT * FROM HiPricedTitles2;

/* using sequences to generate surrogate PKs */
/* create a sequence */
CREATE SEQUENCE testSeq START WITH 100 INCREMENT BY 10;
/* view a sequence */
SELECT * FROM user_Sequences;

/* use a sequence */
CREATE TABLE SequenceTest (testID NUMBER(3) PRIMARY KEY);
INSERT INTO SequenceTest VALUES (testSeq.NEXTVAL);
SELECT * FROM SequenceTest;
