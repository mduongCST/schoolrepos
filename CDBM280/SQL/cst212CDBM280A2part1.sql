CREATE TABLE Historical_Conflict(
      conflictID    NUMBER(4) 
      CONSTRAINT HConflict_conflictID_pk PRIMARY KEY,
      
      startDate     DATE
      CONSTRAINT HConflict_startDate_nn NOT NULL,
      
      conflictName  VARCHAR2(40)
      CONSTRAINT HConflict_conflictName_nn NOT NULL,
      
      prevConflict  NUMBER(4) 
      CONSTRAINT HConflict_prevConflict_fk 
      REFERENCES Historical_Conflict (conflictID),
      
      theatre       VARCHAR2(40) DEFAULT 'N/A'
      CONSTRAINT HConflcit_theatre_nn NOT NULL
      );    
      
DROP TABLE Historical_Conflict;       
          
CREATE TABLE Participant(
      fName         VARCHAR2(30),
      
      lName         VARCHAR2(30),
      
      conflictID    NUMBER(4),

      Participant_type VARCHAR2(1)
      CONSTRAINT Participant_PType_nn NOT NULL,
      
      CONSTRAINT Participant_primaryKeys_pk PRIMARY KEY (fName,lName,conflictID),
      CONSTRAINT Participant_conflictID_fk FOREIGN KEY (conflictID) REFERENCES Historical_Conflict (conflictID)   
      );
      
DROP TABLE Participant;  

ALTER TABLE Participant ADD CONSTRAINT Participant_PType_cc 
CHECK ((Participant_Type = 'L') OR (Participant_Type = 'S'));