--Part 2 Queries Question 1
SELECT customer_Number, C.last || ', ' || C.first AS "Customer Name",
       S.last || ', ' || S.first AS "Sales Rep Name"
       FROM PP.Sales_Rep S JOIN PP.Customer C ON S.slsrep_Number = C.slsrep_Number;
       
--Part 2 Queries Question 2
SELECT O.order_number, C.customer_number, 
       C.last || ', ' || C.first AS "Customer Name",
       OL.number_ordered * OL.quoted_price AS "Total Value"
       FROM PP.Customer C JOIN PP.Orders O ON C.customer_number = O.customer_Number
       JOIN PP.Order_Line OL ON O.order_number = OL.order_number
       WHERE O.order_Date = '05-Sep-2005';
      
--Part 2 Queries Question 3
SELECT S.slsrep_number, O.order_date, P.part_number, 
       S.commission_rate * OL.number_ordered * OL.quoted_price 
       AS "Commission Amount" FROM PP.Sales_Rep S JOIN PP.Customer C 
       ON S.slsrep_number = C.slsrep_number JOIN PP.Orders O 
       ON C.customer_Number = O.customer_Number JOIN PP.Order_Line OL 
       ON O.order_number = OL.order_number JOIN PP.Part P
       ON OL.part_number = P.part_number WHERE P.part_description = 'Gas Grill';
       
--Part 2 Queries Question 4
SELECT DISTINCT OL.part_Number, P.part_description, P.unit_price, OL.quoted_price
       FROM PP.Part P LEFT JOIN PP.Order_Line OL ON P.part_Number = OL.part_Number
       WHERE OL.number_ordered IS NULL OR OL.number_ordered IS NOT NULL;
       
--Part 2 Queries Question 5
SELECT P.part_Number, P.part_Description FROM PP.Part P
       LEFT JOIN PP.Order_Line OL ON P.part_Number = OL.part_Number
       WHERE P.part_Number IN (SELECT P.part_Number FROM PP.Part
       WHERE OL.number_ordered IS NULL);
       
--Part 2 Queries Question 6       
SELECT P.part_Description, P.part_Number, OL.order_Number, O.order_date
       FROM PP.Part P JOIN PP.Order_Line OL ON p.part_number = ol.part_number
       JOIN PP.Orders O ON OL.order_number = O.order_number
       JOIN PP.Customer C ON O.customer_number = C.customer_number
       WHERE O.customer_number IN (SELECT C.customer_number FROM PP.Customer
       WHERE C.last = 'Nelson' AND C.first = 'Mary')
       AND O.customer_Number IN 
       (SELECT C.customer_number FROM PP.Customer WHERE OL.Order_Number != '12504');
                     
       