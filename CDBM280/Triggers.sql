CREATE TABLE title_Price_History
  (title_id char(6), title_name varchar2(80), publisher_id char(4), price number(9,2));
  
CREATE OR REPLACE TRIGGER price_history_trigger
BEFORE UPDATE OF price
ON title
FOR EACH ROW
BEGIN
  INSERT INTO title_price_history VALUES
    (:old.titleid, :old.title, :old.pubid, :old.price);
END;
/

UPDATE title SET price=20 WHERE titleid='PS3333';

SELECT  * FROM title_Price_History;


CREATE TABLE trigger_messages
  (message varchar2(50), current_date DATE);
  
CREATE OR REPLACE TRIGGER before_update_stat_title
  BEFORE
  UPDATE ON title
BEGIN
  INSERT INTO trigger_messages
    VALUES('Before update, statement level', SYSDATE);
END;
/

  
CREATE OR REPLACE TRIGGER before_update_row_title
  BEFORE
  UPDATE ON title
  FOR EACH ROW
BEGIN
  INSERT INTO trigger_messages
    VALUES('Before update, row level', SYSDATE);
END;
/

  
CREATE OR REPLACE TRIGGER after_update_stat_title
  AFTER
  UPDATE ON title
BEGIN
  INSERT INTO trigger_messages
    VALUES('After update, row level', SYSDATE);
END;
/
  
CREATE OR REPLACE TRIGGER after_update_stat_title
  AFTER
  UPDATE ON title
  FOR EACH ROW
BEGIN
  INSERT INTO trigger_messages
    VALUES('After update, statement level', SYSDATE);
END;
/

UPDATE title SET price = 80
  WHERE titleid IN ('PS3333', 'BU1111');
  
SELECT * FROM TRIGGER_MESSAGES;

SELECT * FROM SalesDetail;

SELECT * FROM Title;

CREATE OR REPLACE TRIGGER insert_salesdetail
  AFTER
  INSERT ON SalesDetail
  BEGIN
  
  
