﻿/*
 * Author:  Rob Miller
 * Date:   October 6, 2011
 * Description:  This class is a container class that stores the key and the value
 *               as a pair.  The hashtable will store KeyValue objects rather than
 *               the key and value as two separate objects.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hashtable
{
    public class KeyValue<K,V> where V : IComparable<V>
        where K : IComparable <K>
    {
        //Purpose: Store the AccessCount
        int accessCount;

        //Purpose: Store the key for the value.
        K kKey;

        //Purpose:  Store the value.
        V vValue;

        public string Colour = "ff6699";

        //Purpose:Constructor for the KeyValue class
        //Parameters:  key --> The key for the value.
        //             value --> The value to be stored.
        //             accessCount --> value of how many times object was accessed. Will reset whenever it is re added.
        public KeyValue(K key, V value, int accessCount = 0)
        {
            kKey = key;
            vValue = value;
            this.accessCount = accessCount;
        }

        //Purpose: Property for the key.
        //Return: The key.
        public K Key
        {
            get{return kKey;}
        }

        //Purpose: Property for the value.
        //Return: The value.
        public V Value
        {
            get { return vValue;}
        }
        //Purpose: Property for the value.
        //Return: The value.
        public int AccessCount
        {
            get { return accessCount; }
            set { accessCount = value; }
        }

        //Purpose:  Must override Equals so that the KeyValue pair can be compared to
        //          another KeyValue pair.  Especially important for the ArrayList.Contains()
        //          method that calls the stored objects Equals method for comparison.
        //Parameters:  obj --> The object to compare to for equaltity with 'this'.
        //Returns:  bool --> True if equal, else false.
        public override bool Equals(object obj)
        {
            KeyValue<K, V> kv = (KeyValue<K, V>)obj;
            return this.Key.CompareTo(kv.Key) == 0;
        }
    }
}
