﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hashtable
{
    /// <summary>
    /// This class serves as a marker for values removed from the hashtable.
    /// The purpose is to make sequences continuous within the hashtable.
    /// </summary>
    internal class Tombstone
    {

    }
}
