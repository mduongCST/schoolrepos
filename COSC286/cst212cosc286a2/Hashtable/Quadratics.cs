﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hashtable
{
    class Quadratics<K,V>: A_OpenAdressing<K,V>
        where V : IComparable<V>
        where K : IComparable<K>
    {
        public override int GetIncrement(int iAttempt, K key)
        {
            double c1 = 0.5;
            double c2 = 0.5;
            return (int)(iAttempt * c1 + Math.Pow(iAttempt, 2) * c2);
        }

        public Quadratics()
        {
            this.dLoadFactor = 0.50;
        }
    }
}
