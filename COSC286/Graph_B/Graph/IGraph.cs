﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Graph
{
    public delegate void VisitorDelegate<T>(T data);

    public interface IGraph<T> where T: IComparable<T>
    {
        #region Properties
        int NumVertices { get; }
        int NumEdges { get; }
        #endregion

        #region Methods to work with vertices
        void AddVertex(T data);
        bool HasVertex(T data);
        Vertex<T> GetVertex(T data);
        void RemoveVertex(T data);

        IEnumerable<Vertex<T>> EnumerateVertices();

        IEnumerable<Vertex<T>> EnumerateNeighbours(T data);
        #endregion

        #region Methods to work with edges
        void AddEdge(T from, T to);
        void AddEdge(T from, T to, double weight);
        bool HasEdge(T from ,T to);
        Edge<T> GetEdge(T from, T to);
        void RemoveEdge(T from, T to);
        #endregion

        #region Implementations of Graph Algorithms
        void DepthFirstTraversal(T start, VisitorDelegate<T> whatToDo);
        void BreadthFirstTraversal(T start, VisitorDelegate<T> whatToDo);
        IGraph<T> ShortestWeightedPath(T start, T end);
        IGraph<T> MinimumSpanningTree();

        #endregion
    }
}
