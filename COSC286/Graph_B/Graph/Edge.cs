﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Graph
{
    public class Edge<T>: IComparable where T : IComparable<T>
    {
        #region Attributes
        private Vertex<T> from;
        private Vertex<T> to;
        private double weight;
        private bool isWeighted;
        #endregion

        #region Constructors
        public Edge(Vertex<T> from, Vertex<T> to)
            : this(from, to, double.PositiveInfinity, false)
        {

        }

        public Edge(Vertex<T> from, Vertex<T> to, double weight)
            : this(from, to, weight, true)
        {

        }


        private Edge(Vertex<T> from, Vertex<T> to, double weight, bool isWeighted)
        {
            this.from = from;
            this.to = to;
            this.weight = weight;
            this.isWeighted = isWeighted;
        }
        #endregion

        #region Properties
        public Vertex<T> From
        {
            get { return from; }
        }

        public Vertex<T> To
        {
            get { return to; }
        }

        public double Weight
        {
            get { return weight; }
        }

        public bool IsWeighted
        {
            get { return isWeighted; }
        }
        #endregion

        public int CompareTo(object obj)
        {
            Edge<T> other = (Edge<T>)obj;
            int result = Weight.CompareTo(other.Weight);
            if (result == 0)
            {
                result = From.CompareTo(other.From);
                if (result == 0)
                {
                    result = To.CompareTo(other.To);
                }
            }
            return result;
        }



        public override string ToString()
        {
            return From + " To " + To +
                (isWeighted ? " , W = " + weight : "");
        }
    }
}