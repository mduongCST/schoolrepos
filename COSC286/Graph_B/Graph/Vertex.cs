﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Graph
{
    public class Vertex<T> where T: IComparable<T>
    {
        #region Attributes
        private T data;
        private int index;


        #endregion

        #region Constructor
        public Vertex(int index, T data)
        {
            this.data = data;
            this.index = index;
        }
        #endregion

        #region Properties

        public int Index
        {
            get { return index; }
            set { index = value; }
        }

        public T Data
        {
            get { return data; }
        }
        #endregion

        #region Other functionality
        public int CompareTo(Vertex<T> other)
        {
            return Index.CompareTo(other.Index);
        }

        public override string ToString()
        {
            return "[" + data + "]";
        }

        #endregion
    }
}
