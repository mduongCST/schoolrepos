﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Graph
{
    public abstract class AGraph<T>: IGraph<T> where T: IComparable<T>
    {
        #region Attributes
        //A collection to store our vertices
        protected List<Vertex<T>> vertices;
        //A hashtable used to store the index of a data item in the vertices collection.
        //this is done to speed up retrievals for a particular index
        protected Dictionary<T, int> revLookup;
        protected int numEdges;
        protected bool isDirected;
        protected bool isWeighted;

        #endregion

        #region Abstract Methods
        //When a vertex is added the child class will have to make room for its edges
        protected abstract void AddVertexAdjustEdges(Vertex<T> v);
        //Called when the child class is told to add an edge and store it.
        protected abstract void AddEdge(Edge<T> e);
        protected abstract Edge<T>[] GetAllEdges();
        public abstract IEnumerable<Vertex<T>> EnumerateNeighbours(T data);
        public abstract bool HasEdge(T from, T to);
        public abstract Edge<T> GetEdge(T from, T to);
        public abstract void RemoveEdge(T from, T to);
        #endregion 
        
        public AGraph()
        {
            vertices = new List<Vertex<T>>();
            revLookup = new Dictionary<T,int>();
            numEdges = 0;
        }

        #region Properties
        public int NumVertices
        {
            get { return vertices.Count; }
        }

        public virtual int NumEdges
        {
            get { return numEdges; }
        }
        #endregion

        //Can implement here.
        public void AddVertex(T data)
        {
            if (HasVertex(data))
            {
                throw new ApplicationException("Vertex already exists");
            }
            Vertex<T> v = new Vertex<T>(vertices.Count, data);
            vertices.Add(v);
            //Also have to put it into the reverse lookup dictionary
            revLookup.Add(data, v.Index);
            //Account for edges that could be added to the vertex
            AddVertexAdjustEdges(v);
        }

        //Can implement here.
        //Looks for the existing data item in the dictionary.
        public bool HasVertex(T data)
        {
            return revLookup.ContainsKey(data);
        }

        //Returns the vertex if it is in the vertices array, else throws an

        public Vertex<T> GetVertex(T data)
        {
            //if the item does not exist as a vertex in the vertices list
            if (!HasVertex(data))
            {
                throw new ApplicationException("No such vertex");
            }
            //Get the index from the dictionary of the vertex in the lookup
            int index = revLookup[data];
            //Index into the vertices list and return the vertex
            return vertices[index];
        }

        //Can implement here.
        public void RemoveVertex(T data)
        {
            throw new NotImplementedException();
        }


        public IEnumerable<Vertex<T>> EnumerateVertices()
        {
            return vertices;
        }

        // if the user adds an unweighted edge, then in the future only unweighted
        // edges will be allowed for this graph.s
        public virtual void AddEdge(T from, T to)
        {
            //if this is the first edge
            if (numEdges == 0)
            {
                isWeighted = false;
            }
            else if(isWeighted)//If it is already declared as weighted, why would you add an unweighted edge?
            {
                throw new ApplicationException(
                    "Can't add unweighted edge to weighted graph");
            }
            Edge<T> e = new Edge<T>(GetVertex(from), GetVertex(to));
            AddEdge(e);
        }

        public virtual void AddEdge(T from, T to, double weight)
        {
            //if this is the first edge
            if (numEdges == 0)
            {
                isWeighted = true;
            }
            else if (!isWeighted)//If it is already declared as unweighted, why would you add a weighted edge?
            {
                throw new ApplicationException(
                    "Can't add weighted edge to an unweighted graph");
            }
            Edge<T> e = new Edge<T>(GetVertex(from), GetVertex(to), weight);
            AddEdge(e);
        }

        public void DepthFirstTraversal(T start, VisitorDelegate<T> whatToDo)
        {
            Vertex<T> vCurrent;
            Vertex<T> vStart = GetVertex(start);
            //The dictionary is used to track vertices already processed.
            Dictionary<T, T> visitedVertices = new Dictionary<T, T>();
            //The Stack used for DFT instead of recursion
            Stack<Vertex<T>> verticesRemaining = new Stack<Vertex<T>>();
            //Push the start vertex on the stack
            verticesRemaining.Push(vStart);
            //loop while there are vertices on the call stack
            while (verticesRemaining.Count > 0)
            {
                //Get a reference to the current vertex
                vCurrent = verticesRemaining.Pop();
                if(!visitedVertices.ContainsKey(vCurrent.Data))
                {
                    //Process the data
                    whatToDo(vCurrent.Data);
                    //Add to the dictinary
                    visitedVertices.Add(vCurrent.Data, vCurrent.Data);
                    //You can call reverse if you want the exact same traversal as the recursive solution
                    foreach (Vertex<T> v in EnumerateNeighbours(vCurrent.Data).Reverse())
                    {
                        verticesRemaining.Push(v);
                    }
                }
            }
            
        }

        //public void DepthFirstTraversal(T start, VisitorDelegate<T> whatToDo)
        //{
        //    Vertex<T> vStart = GetVertex(start);
        //    //The dictionary is used to track vertices already processed.
        //    Dictionary<T, T> visitedVertices = new Dictionary<T, T>();
        //    //Recursive method to process the neighborus of the current vertex
        //    RecDFT(vStart, whatToDo, visitedVertices);
        //}

        //private void RecDFT(Vertex<T> vCurrent, VisitorDelegate<T> whatToDo, Dictionary<T, T> visitedVertices)
        //{
        //    //Process the current Vertex
        //    whatToDo(vCurrent.Data);
        //    //Indicate that this vertex has been visited
        //    visitedVertices.Add(vCurrent.Data, vCurrent.Data);
        //    //For every neigbour of the current vertex
        //    foreach (Vertex<T> v in EnumerateNeighbours(vCurrent.Data))
        //    {
        //        //If the current neighbour is visited
        //        if (!visitedVertices.ContainsKey(v.Data))
        //        {
        //            //Recurse the neighbour
        //            RecDFT(v, whatToDo, visitedVertices);
        //        }
        //    }
        //}

        public void BreadthFirstTraversal(T start, VisitorDelegate<T> whatToDo)
        {
            Vertex<T> vCurrent;
            Vertex<T> vStart = GetVertex(start);
            //The dictionary is used to track vertices already processed.
            Dictionary<T, T> visitedVertices = new Dictionary<T, T>();
            //The Queue used for BFT
            Queue<Vertex<T>> verticesRemaining = new Queue<Vertex<T>>();
            //Enqueue the start vertex ont he queue
            verticesRemaining.Enqueue(vStart);
            //loop while there are vertices on the call stack
            while (verticesRemaining.Count > 0)
            {
                //Get a reference to the current vertex
                vCurrent = verticesRemaining.Dequeue();
                if (!visitedVertices.ContainsKey(vCurrent.Data))
                {
                    //Process the data
                    whatToDo(vCurrent.Data);
                    //Add to the dictinary
                    visitedVertices.Add(vCurrent.Data, vCurrent.Data);
                    //You can call reverse if you want the exact same traversal as the recursive solution
                    foreach (Vertex<T> v in EnumerateNeighbours(vCurrent.Data))
                    {
                        verticesRemaining.Enqueue(v);
                    }
                }
            }            
        }

        #region Shortest Weighted Path
        public IGraph<T> ShortestWeightedPath(T start, T end)
        {
            //Create an array of vertexData objects, one for each vertice
            VertexData[] vTable = new VertexData[vertices.Count];
            //Get index of the start node
            int iStartIndex = GetVertex(start).Index;

            //Create a vertexData object, one for each vertice
            for(int i = 0; i < vertices.Count; i++)
            {
                vTable[i] = new VertexData(vertices[i], false, double.PositiveInfinity, null);
            }           

            //Set the start vertexData to a distance of 0
            vTable[iStartIndex].Distance = 0;

            //Create the priority Queue
            PriorityQueue pq = new PriorityQueue();

            //Add the start vertex to the queue
            pq.Enqueue(vTable[iStartIndex]);

            while (!pq.IsEmpty())
            {
                //Get the next vertex with lowest cost
                VertexData v = pq.Dequeue();
                //If the shortest path to the vertex is still unknown
                if (!v.Known)
                {
                    //Set the current vertex to known
                    v.Known = true;
                    //For each neighbour of the current vertex
                    foreach (Vertex<T> wV in EnumerateNeighbours(v.vVertex.Data))
                    {
                        //Get the vertexData (dpk) for the current vertex
                        VertexData w = vTable[wV.Index];
                        //Get the edge conencting v and w so we can get the cost or weight
                        Edge<T> eEdge = GetEdge(v.vVertex.Data, w.vVertex.Data);
                        //Calculate the proposed distance for w to the start through V
                        double dProposed = v.Distance + eEdge.Weight;
                        //Compare the proposed to the current distance for w.
                        if (w.Distance > dProposed)
                        {
                            w.Distance = dProposed;
                            w.vPrevious = v.vVertex;
                            pq.Enqueue(w);
                        }
                    }
                }
            }
            return BuildGraph(GetVertex(end), vTable);
        }

        private IGraph<T> BuildGraph(Vertex<T> vEnd, VertexData[] vTable)
        {
            //Get an instance of the child implementation of the graph
            //This is one way to do this using C#
            //IGraph<T> result = (IGraph<T>)GetType().Assembly.CreateInstance(GetType().FullName);
            AGraph<T> result = (AGraph<T>)GetType().Assembly.CreateInstance(GetType().FullName);

            //Working backwards through vTable
            result.AddVertex(vEnd.Data);

            VertexData vDataLast = vTable[vEnd.Index];
            Vertex<T> vPrevious = vDataLast.vPrevious;

            //While vPrevious is not null                
            while (vPrevious != null)
            {
                //Add the previous vertex
                result.AddVertex(vPrevious.Data);
                //Get the edge between last and previous
                Edge<T> eEdge = GetEdge(vDataLast.vVertex.Data, vPrevious.Data);
                //Add the edge to the graph
                result.AddEdge(eEdge.From.Data, eEdge.To.Data);
                //result.AddEdge(vPrevious.Data, vDataLast.vVertex.Data);
                //Move vDataLast to point to vPrevious
                vDataLast = vTable[vPrevious.Index];
                //Set previous to vDataLast's previous
                vPrevious = vDataLast.vPrevious;
            }
            return result;

        }

        internal class VertexData : IComparable
        {
            public Vertex<T> vVertex;
            public bool Known;
            public double Distance;
            public Vertex<T> vPrevious;

            public VertexData(Vertex<T> vVertex, bool Known, double Distance, Vertex<T> vPrevious)
            {
                this.vVertex = vVertex;
                this.Known = Known;
                this.Distance = Distance;
                this.vPrevious = vPrevious;
            }

            public override string ToString()
            {
                return "Vertex : " + vVertex + " Distance: " + Distance + " Previous: " + vPrevious.Data;
            }

            public int CompareTo(object obj)
            {
                return this.Distance.CompareTo(((VertexData)obj).Distance);
            }
        }

        internal class PriorityQueue
        {
            private List<VertexData> sl;

            public PriorityQueue()
            {
                sl = new List<VertexData>();
            }

            internal void Enqueue(VertexData vData)
            {
                sl.Add(vData);
                sl.Sort();
            }

            internal VertexData Dequeue()
            {
                VertexData RetVal = sl[0];
                sl.RemoveAt(0);
                return RetVal;
            }

            public bool IsEmpty()
            {
                if (sl.Count > 0)
                {
                    return false;
                }
                return true;
            }

            //For testing
            public void DisplayQueue()
            {
                foreach (VertexData v in sl)
                {
                    Console.WriteLine(v.ToString());
                }
                Console.WriteLine();
            }
        }
        #endregion

        #region Minimum Spanning Tree
        /// <summary>
        /// Determine the minimum spanning tree using Kruskal's algorithm
        /// Assumes that the calling graph is connected.
        /// </summary>
        /// <returns>A graph representing the minimum spanning tree</returns>
        public IGraph<T> MinimumSpanningTree()
        {

            //Create an array to store graphs, one for each vertex            
            AGraph<T>[] forest = new AGraph<T>[vertices.Count];            
            //Get a sorted edge list from original (calling) graph
            Edge<T>[] sortedEdges = GetSortedArrayOfEdges();
            //Index of current edge to process
            int iEdge = 0;
            //Index of the "to" graph
            int iTo = 0;
            //Index of the "from" graph
            int iFrom = 0;

            //for each location in the forest, add a graph 
            for(int i = 0; i < forest.Length; i++)
            {
                //Create a new instance of the child graph using reflection (see example
                	//in BuildGraph code of ShortestWeightedPath
                AGraph<T> currGraph = (AGraph<T>)GetType().Assembly.CreateInstance(GetType().FullName);
                //Add the corresponding vertex from the vertices array to the current graph
                currGraph.AddVertex(vertices[i].Data);
                //Add the graph to the forest
                forest[i] = currGraph;
            }
            //While the forest has more than one graph and there are still edges to process
            while (forest.Length > 1 && sortedEdges.Length > 0)
            {
                //Get the current edge from the sorted list of edges
                Edge<T> currentEdge = sortedEdges[iEdge];
                //Locate the indexs of To and From trees in the forest
                iTo = FindTree(currentEdge.To, forest);
                iFrom = FindTree(currentEdge.From, forest);
                //If the current To and From are from different graphs
                if (iTo != iFrom)
                {
                    //Merge the two graphs together     
                    forest[iTo].MergeGraphs(forest[iFrom]);
                    //Add the current edge to the resultant graph
                    forest[iTo].AddEdge(currentEdge.From.Data, currentEdge.To.Data, currentEdge.Weight);
                    //Cut tree From from the forest
                    forest = Timber(iFrom, forest);                    
                }
                //Increment to the next edge
                iEdge++;              
            }
            //If there is still more than one graph in the forest, must have been disconnected
            if (forest.Length > 1)
            {
                //Throw an exception
                throw new ApplicationException("Must have been disconnected");
            }
            //Return the graph at forest location 0.                            
            return forest[0];
        }
        /// <summary>
        /// Given a forest of graphs and a vertex, determine the index of the graph
        /// that contains the vertex.
        /// </summary>
        /// <param name="vertex">Vertex to find</param>
        /// <param name="forest">forest of graphs</param>
        /// <returns>Index of graph containing the vertex</returns>
        private int FindTree(Vertex<T> vertex, AGraph<T>[] forest)
        {
            int i = 0;
            //Check each tree in the forest if it contains the vertex
            IGraph<T> currentGraph = forest[i];
            //While current graph does not contain the vertex AND i < forests length
            while(!currentGraph.HasVertex(vertex.Data) && i < forest.Length)
            {
                //Get next graph in the forest                
                i++;
                currentGraph = forest[i];                
            } 
            //If we reach the end of the forest and the vertex was not found
            if(i == forest.Length && !currentGraph.HasVertex(vertex.Data))
            {
                //Throw an exception
                throw new ApplicationException("Vertex was not found.");
            }
            
            //Return index of tree in the forest
            return i;
        }

        /// <summary>
        /// Get all of the edges and sort them
        /// </summary>
        /// <returns>A sorted edge array</returns>
        private Edge<T>[] GetSortedArrayOfEdges()
        {
            //Call GetAllEdges() to get an array of edges
            Edge<T>[] edgeArray = GetAllEdges();
            //Call Array.Sort() on the edge array to sort it.            
            Array.Sort(edgeArray);
            //Return the sorted array of edges;
            return edgeArray;
        }

        /// <summary>
        /// Merge all vertices from GraphFrom to GraphTo
        /// Also, add all edges in GraphFrom to GraphTo
        /// </summary>
        /// <param name="GraphFrom">Graph containing vertices/edges to add to "To"</param>
        private void MergeGraphs(AGraph<T> GraphFrom)
        {
            //Get a reference to the destination graph (this)
            IGraph<T> GraphTo = this;
            //Add vertices from GraphFrom to GraphTo (foreach loop)
            foreach(Vertex<T> vertex in GraphFrom.vertices)
            {
                GraphTo.AddVertex(vertex.Data);
            }
            //Get an array of all edges from GraphFrom
            Edge<T>[] edgeArray = GraphFrom.GetAllEdges();
            //Foreach edge in the edge array
            foreach(Edge<T> edge in edgeArray)
            {
                //Add the edge to GraphTo
                GraphTo.AddEdge(edge.From.Data, edge.To.Data, edge.Weight);
            }

        }

        /// <summary>
        /// Given a forest of graphs, return a new forest that has all graphs
        /// except for the one to remove.
        /// </summary>
        /// <param name="iGraph">Index of the graph to remove</param>
        /// <param name="forest">The original forest of graphs</param>
        /// <returns>A new forest, one size smaller than the one passed in.</returns>
        private AGraph<T>[] Timber(int iGraph, AGraph<T> [] forest)
        {
            //Instatiate a new forest of graphs
            AGraph<T>[] newForest = new AGraph<T>[forest.Length - 1];
            //Initialize a counter to index into the new forest
            int j = 0;
            //For i = 0 to forest's current length
            for(int i = 0; i < forest.Length; i++)
            {
                //if the graph is NOT the tree to cut
                if(i != iGraph)
                {
                    //Add current graph in forest to new forest
                    newForest[j] = forest[i];
                    //increment the new forest counter
                    j++;
                }
            }                            
            //Return the new forest
            return newForest;
        }


        
        #endregion        
        //To string method
        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            // loop through each vertex and add to the result
            foreach (Vertex<T> v in EnumerateVertices())
            {
                result.Append(v + "\t");
            }

            return GetType().Name + "\nvertices: " + result + "\n";
        }

    }
}
