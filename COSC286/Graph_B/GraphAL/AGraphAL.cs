﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Graph;

namespace GraphAL
{
    public abstract class AGraphAL<T> : AGraph<T> where T : IComparable<T>
    {

        #region attributes
        //private
        protected List<Edge<T>>[] listArray;
        #endregion

        public AGraphAL()
        {
            //Initialize with a List of size 0
            listArray = new List<Edge<T>>[0];
        }

        protected override void AddVertexAdjustEdges(Vertex<T> v)
        {
            //Set up a temporary array List
            List<Edge<T>>[] oldArray = listArray;
            //create the new listArray
            listArray = new List<Edge<T>>[oldArray.Length + 1];
            for(int i = 1; i < oldArray.Length; i++)
            {
                listArray[i] = oldArray[i];
            }                                    
            listArray[listArray.Length] = new List<Edge<T>>();
        }

        protected override void AddEdge(Edge<T> e)
        {
            int indexFrom = e.From.Index;
            int indexTo = e.To.Index;

            if (this.isDirected)
            {
                
            }
            else
            {
                if (!listArray[indexFrom].Contains(e))
                {
                    listArray[indexFrom].Add(e);
                }
                else
                {
                    throw new ApplicationException("Edge already exists.");
                }
            }
        }

        protected override Edge<T>[] GetAllEdges()
        {
            Edge<T>[] allEdges;
            int currIndex = 0;
            if (this.isDirected)
            {
                allEdges = new Edge<T>[NumEdges];

            }
            else
            {
                allEdges = new Edge<T>[NumEdges / 2];
                T[] visitedVertices = new T[NumVertices];
                for (int i = 0; i < listArray.Length; i++)
                {
                    for (int j = 0; j < listArray[i].Count; j++)
                    {
                        if (!visitedVertices.Contains<T>(listArray[i][j].To.Data))
                        {
                            allEdges[currIndex++] = listArray[i][j];
                        }
                        visitedVertices[i] = vertices[i].Data;
                    }
                }
            }
            // Not working
            return allEdges;
        }

        public override IEnumerable<Vertex<T>> EnumerateNeighbours(T data)
        {
            throw new NotImplementedException();
        }

        public override bool HasEdge(T from, T to)
        {
            throw new NotImplementedException();
        }

        public override Edge<T> GetEdge(T from, T to)
        {
            throw new NotImplementedException();
        }

        public override void RemoveEdge(T from, T to)
        {
            throw new NotImplementedException();
        }
    }
}
