﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Graph;

namespace GraphMatrix
{
    public abstract class AGraphMatrix<T> : AGraph<T> where T: IComparable<T>
    {
        #region Attributes
        protected Edge<T>[,] matrix;
        #endregion

        #region Constructor
        public AGraphMatrix()
        {
            matrix = new Edge<T>[0, 0];
        }
        #endregion

        protected override void AddVertexAdjustEdges(Vertex<T> v)
        {
            //Get a reference to the existing array
            Edge<T>[,] oldMatrix = matrix;
            //Make a larger matrix with room for edges of new vertex
            matrix = new Edge<T>[NumVertices, NumVertices];
            //Copy the existing edges to the new matrix
            for (int r = 0; r < oldMatrix.GetLength(0); r++)
            {
                for (int c = 0; c < oldMatrix.GetLength(1); c++)
                {
                    matrix[r, c] = oldMatrix[r, c];
                }
            }
        }

        protected override void AddEdge(Edge<T> e)
        {
            //if the edge already exists in the matrix
            if (matrix[e.From.Index, e.To.Index] != null)
            {
                //Throw an exception
                throw new ApplicationException("Edge already exists");
            }
            matrix[e.From.Index, e.To.Index] = e;
            numEdges++;
        }

        protected override Edge<T>[] GetAllEdges()
        {
            Edge<T>[] edges;
            int i = 0;

            //If the graph is directed, loop through the entire matrix
            if (this.isDirected)
            {
                edges = new Edge<T>[numEdges];
                // loop through each row and column
                for (int r = 0; r < matrix.GetLength(0); r++)
                {
                    for (int c = 0; c < matrix.GetLength(1); c++)
                    {
                        Edge<T> e = matrix[r, c];
                        if (e != null)
                        {
                            edges[i++] = e;
                        }
                    }
                }
            }
            //If the graph is undirected, loop through above the diagonal of the matrix
            //to avoid duplicating edges.
            else
            {
                edges = new Edge<T>[numEdges / 2];
                // loop through each row and column above main diagonal
                for (int r = 0; r < matrix.GetLength(0); r++)
                {
                    for (int c = r + 1; c < matrix.GetLength(1); c++)
                    {
                        Edge<T> e = matrix[r, c];
                        if (e != null)
                        {
                            edges[i++] = e;
                        }
                    }
                }
            }
            return edges;

        }

        //For a particular data item, find all vertices that are its immediate neighbours.
        public override IEnumerable<Vertex<T>> EnumerateNeighbours(T data)
        {
            //Get the vertex associated with the data item passed in, Recall that
            //the vertex also stores its index in the vertices array, as well as the edge array
            Vertex<T> v = GetVertex(data);
            //Make a list of vertices to return from the method
            List<Vertex<T>> neighbours = new List<Vertex<T>>();
            //Loop through the EDGE matrix row associated with this vertex, looking for edges
            for (int c = 0; c < matrix.GetLength(1); c++)
            {
                //If there is an edge at the current position
                if (matrix[v.Index, c] != null)
                {
                    //Add the "To" portion of the Edge to the list
                    neighbours.Add(matrix[v.Index, c].To);
                }
            }
            return neighbours;
        }

        public override bool HasEdge(T from, T to)
        {
            //Get the index for the from and to, index into the array and see if it is null or an edge.
            return matrix[GetVertex(from).Index, GetVertex(to).Index] != null;            
        }

        public override Edge<T> GetEdge(T from, T to)
        {
            if (!HasEdge(from, to))
            {
                throw new ApplicationException("No such edge");
            }
            return matrix[GetVertex(from).Index, GetVertex(to).Index];
        }

        public override void RemoveEdge(T from, T to)
        {
            if (HasEdge(from, to))
            {
                matrix[GetVertex(from).Index, GetVertex(to).Index] = null;
            }
            else
            {
                throw new ApplicationException("No such edge");
            }
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder("\nEdge matrix:\n");
            // loop for each row and column
            for (int r = 0; r < matrix.GetLength(0); r++)
            {
                Vertex<T> v = vertices[r];
                result.Append(v.Data.ToString() + "\t");
                for (int c = 0; c < matrix.GetLength(1); c++)
                {
                    result.Append((matrix[r, c] == null ? " -- " :
                        matrix[r, c].To.ToString()) + "\t");
                }
                //result.Remove(result.Length - 2, 2);
                result.Append("\n");
            }
            return base.ToString() + result;
        }

    }
}
