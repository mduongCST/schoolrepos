﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphMatrix
{
    class UGraphMatrix<T> : AGraphMatrix<T> where T: IComparable<T>
    {
        public UGraphMatrix()
        {
            isDirected = false;
        }
        public override int NumEdges
        {
            get
            {
                return base.NumEdges / 2;
            }
        }
        public override void AddEdge(T from, T to)
        {
            base.AddEdge(from, to);
            base.AddEdge(to, from);
        }

        public override void AddEdge(T from, T to, double weight)
        {
            base.AddEdge(from, to, weight);
            base.AddEdge(to, from, weight);
        }
    }
}
