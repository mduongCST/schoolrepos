﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Graph;

namespace GraphMatrix
{
    class Program
    {
        static void DemoShortestWeightedPath()
        {
            UGraphMatrix<String> ug = new UGraphMatrix<String>();
            ug.AddVertex("PA");
            ug.AddVertex("S");
            ug.AddVertex("Ch");
            ug.AddVertex("16/2");
            ug.AddVertex("K");
            ug.AddVertex("R");
            ug.AddVertex("MJ");
            ug.AddVertex("Y");

            ug.AddEdge("PA", "S", 145);
            ug.AddEdge("PA", "16/2", 145);
            ug.AddEdge("S", "16/2", 75);
            ug.AddEdge("S", "K", 80);
            ug.AddEdge("Ch", "MJ", 90);
            ug.AddEdge("16/2", "Ch", 150);
            ug.AddEdge("Ch", "R", 90);
            ug.AddEdge("MJ", "R", 75);
            ug.AddEdge("16/2", "Y", 260);
            ug.AddEdge("K", "Ch", 120);

            String s = "S";
            String e = "MJ";
            Console.WriteLine(ug.ShortestWeightedPath(s, e));

        }
        static void PrintData(String s)
        {
            Console.WriteLine(s);
        }

        static void DemoBFT()
        {
            UGraphMatrix<String> ug = new UGraphMatrix<String>();
            ug.AddVertex("PA");
            ug.AddVertex("S");
            ug.AddVertex("Ch");
            ug.AddVertex("16/2");
            ug.AddVertex("K");
            ug.AddVertex("R");
            ug.AddVertex("MJ");
            ug.AddVertex("Y");

            ug.AddEdge("PA", "S", 145);
            ug.AddEdge("PA", "16/2", 145);
            ug.AddEdge("S", "16/2", 75);
            ug.AddEdge("S", "K", 80);
            ug.AddEdge("Ch", "MJ", 90);
            ug.AddEdge("16/2", "Ch", 150);
            ug.AddEdge("Ch", "R", 90);
            ug.AddEdge("MJ", "R", 75);
            ug.AddEdge("16/2", "Y", 260);
            ug.AddEdge("K", "Ch", 120);

            String s = "S";
            Console.WriteLine("Breadth First Traversal starting at " + s + " is:");
            ug.BreadthFirstTraversal(s, PrintData);
        }
        static void DemoDFT()
        {
            UGraphMatrix<String> ug = new UGraphMatrix<String>();
            ug.AddVertex("PA");
            ug.AddVertex("S");
            ug.AddVertex("Ch");
            ug.AddVertex("16/2");
            ug.AddVertex("K");
            ug.AddVertex("R");
            ug.AddVertex("MJ");
            ug.AddVertex("Y");

            ug.AddEdge("PA", "S", 145);
            ug.AddEdge("PA", "16/2", 145);
            ug.AddEdge("S", "16/2", 75);
            ug.AddEdge("S", "K", 80);
            ug.AddEdge("Ch", "MJ", 90);
            ug.AddEdge("16/2", "Ch", 150);
            ug.AddEdge("Ch", "R", 90);
            ug.AddEdge("MJ", "R", 75);
            ug.AddEdge("16/2", "Y", 260);

            String s = "Y";
            Console.WriteLine("Depth First Traversal starting at " + s + " is:");
            ug.DepthFirstTraversal(s, PrintData);
        }

        static void DemoGetNeighbours()
        {
            UGraphMatrix<String> ug = new UGraphMatrix<String>();
            ug.AddVertex("PA");
            ug.AddVertex("S");
            ug.AddVertex("Ch");
            ug.AddVertex("16/2");
            ug.AddVertex("K");
            ug.AddVertex("R");
            ug.AddVertex("MJ");
            ug.AddVertex("Y");

            ug.AddEdge("PA", "S", 145);
            ug.AddEdge("PA", "16/2", 145);
            ug.AddEdge("S", "16/2", 75);
            ug.AddEdge("S", "K", 80);
            ug.AddEdge("K", "Ch", 90);
            ug.AddEdge("16/2", "Ch", 150);
            ug.AddEdge("Ch", "R", 90);
            ug.AddEdge("MJ", "R", 75);
            ug.AddEdge("16/2", "Y", 260);

            String s = "16/2";
            Console.WriteLine("The neighbours of " + s + " are:");

            foreach (Vertex<string> v in ug.EnumerateNeighbours(s))
            {
                Console.WriteLine(v.Data.ToString());
            }

        }
        static void DemoUndirectedGraph()
        {
            UGraphMatrix<String> ug = new UGraphMatrix<String>();
            ug.AddVertex("PA");
            ug.AddVertex("S");
            ug.AddVertex("Ch");
            ug.AddVertex("16/2");
            ug.AddVertex("K");

            ug.AddEdge("PA", "S", 145);
            ug.AddEdge("PA", "16/2", 145);
            ug.AddEdge("S", "16/2", 75);
            ug.AddEdge("S", "K", 80);
            ug.AddEdge("K", "Ch", 90);
            ug.AddEdge("16/2", "Ch", 150);

            Console.WriteLine(ug.ToString());
            Console.WriteLine("Number of edges is " + ug.NumEdges);
        }

        static void DemoDirectedGraph()
        {
            DGraphMatrix<String> dg = new DGraphMatrix<String>();
            dg.AddVertex("PA");
            dg.AddVertex("S");
            dg.AddVertex("Ch");
            dg.AddVertex("16/2");
            dg.AddVertex("K");
            dg.AddVertex("R");
            dg.AddVertex("MJ");
            dg.AddVertex("Y");

            dg.AddEdge("PA", "S", 145);
            dg.AddEdge("PA", "16/2", 145);
            dg.AddEdge("S", "16/2", 75);
            dg.AddEdge("S", "K", 80);
            dg.AddEdge("Ch", "MJ", 55);
            dg.AddEdge("16/2", "Ch", 150);
            dg.AddEdge("Ch", "R", 90);
            dg.AddEdge("MJ", "R", 75);
            dg.AddEdge("16/2", "Y", 260);

            Console.WriteLine(dg.ToString());
            Console.WriteLine("Number of edges is " + dg.NumEdges);
        }

        static void DemoMinimumSpanningTree()
        {
            UGraphMatrix<String> ug = new UGraphMatrix<string>();
            ug.AddVertex("PA");
            ug.AddVertex("S");
            ug.AddVertex("16/2");
            ug.AddVertex("Ch");
            ug.AddVertex("K");
            ug.AddVertex("Y");
            ug.AddVertex("MJ");
            ug.AddVertex("R");

            ug.AddEdge("PA", "S", 145);
            ug.AddEdge("PA", "16/2", 145);
            ug.AddEdge("S", "16/2", 75);
            ug.AddEdge("S", "K", 90);
            ug.AddEdge("K", "Ch", 90);
            ug.AddEdge("R", "Y", 190);
            ug.AddEdge("16/2", "Ch", 150);
            ug.AddEdge("16/2", "Y", 260);
            ug.AddEdge("Ch", "MJ", 55);
            ug.AddEdge("MJ", "R", 75);
            ug.AddEdge("Ch", "R", 90);

            Console.WriteLine(ug.MinimumSpanningTree().ToString());
        }

        static void Main(string[] args)
        {
            //DemoGetNeighbours();
            //DemoDirectedGraph();
            //DemoDFT();
            //DemoBFT();
            //DemoShortestWeightedPath();
            DemoMinimumSpanningTree();
        }
    }
}
