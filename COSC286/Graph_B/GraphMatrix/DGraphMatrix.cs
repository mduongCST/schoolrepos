﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphMatrix
{
    public class DGraphMatrix<T> : AGraphMatrix<T> where T: IComparable<T>
    {
        public DGraphMatrix()
        {
            isDirected = true;
        }

        public override int NumEdges
        {
            get
            {
                return base.NumEdges/2;
            }
        }
    }
}
