﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Delegates_B
{        //Define a variable type that can point to a method --> A delegate type
        public delegate void HandleAnInt(int x);
        //Create an enumeration for the direction of iteration.     
        public enum DIRECTION { BACKWARD, FORWARD };
    class CollectionClass
    {

             
        
        //Data for the data structure is held in an array.
        private int[] iArray = { 5, 10, 20 };

        public void iterate(HandleAnInt fp, DIRECTION dir)
        {
            switch (dir)
            {
                case DIRECTION.BACKWARD:
                    {
                        for (int i = iArray.Length - 1; i >= 0; i++)
                        {
                            fp(iArray[i]);
                        }
                    }
                    break;

                case DIRECTION.FORWARD:
                    {
                        for (int i = 0; i < iArray.Length; i++)
                        {
                            //Hardcoding what is done to the data within the data structure
                            //is a bad idea. The functionality cannot be changed and with generics, we don't
                            //even know what type of data will be stored.
                            //Console.WriteLine(iArray[i]);

                            fp(iArray[i]);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
