﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Delegates_B
{
    class Program
    {
        //Write a suitable function to pass in to the iterate method. It must
        //match the signature of the delegate type.

        public static void PrintData(int x)
        {
            Console.WriteLine("The value is " + x);
        }

        public static void DisplaySquare(int x)
        {
            Console.WriteLine("the square of the value is " + Math.Pow(x, 2));
        }

        //This method cannot be passsed in to iterate as it does not match the
        //delegate signature.
        public static int ReturnTheInt(int x)
        {
            return x;
        }

        static void Main(string[] args)
        {
            CollectionClass c = new CollectionClass();
            c.iterate(PrintData, DIRECTION.BACKWARD);
            //c.iterate(DisplaySquare);

        }
    }
}
