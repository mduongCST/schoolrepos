﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sorting
{
    public class InsertionSorter<T> : ASorter<T>
        where T : IComparable<T>
    {
        public InsertionSorter(T[] array)
             :base(array)
        {

        }

        public override void Sort()
        {            
            for (int i = 1; i <= Length - 1; i++)
            {
                int j = i;
                while (j > 0 && array[j].CompareTo(array[j - 1]) == -1)
                {
                    Swap(j, j - 1);
                    j--;
                }
            }
        }
    }
}
