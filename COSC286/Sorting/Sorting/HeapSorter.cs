﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sorting
{
    public class HeapSorter<T> : ASorter<T>
        where T : IComparable<T>
    {
        public HeapSorter(T[] array)
            : base(array)
        {

        }

        public override void Sort()
        {
            //Create the initial heap
            Heapify();

            //For each location in the array from last to first + 1
            for (int i = array.Length - 1; i > 0; i--)
            {
                //Swap first with last element of the heap
                Swap(0, i);
                //Sift the swapped element at position 0 down the heap
                SiftDown(0, i - 1);
            }
                
                
        }

        /// <summary>
        /// Moves an element down the heap until it is in its correct position
        /// </summary>
        /// <param name="iParent">Element to move down</param>
        /// <param name="iLast">End of the heap</param>
        private void SiftDown(int iParentIndex, int iLastPosition)
        {
            T tLeft = default(T);
            T tRight = default(T);
            T tParent = array[iParentIndex];

            //Get the index of each child
            int iIndexLeftChild = GetLeftChildIndex(iParentIndex);
            int iIndexRightChild = GetRightChildIndex(iParentIndex);

            //if the right child exists, so does the left
            if (iIndexRightChild <= iLastPosition)
            {
                //Get a reference to the left and right child
                tLeft = array[iIndexLeftChild];
                tRight = array[iIndexRightChild];

                if (tLeft.CompareTo(tParent) > 0 && tLeft.CompareTo(tRight) >= 0)
                {
                    //Swap left with the parent
                    Swap(iParentIndex, iIndexLeftChild);
                    //Recursively sift the keft child down the heap

                    SiftDown(iIndexLeftChild, iLastPosition);
                }
                else if (tRight.CompareTo(tParent) > 0 && tRight.CompareTo(tLeft) > 0)
                {
                    Swap(iParentIndex, iIndexRightChild);
                    SiftDown(iIndexRightChild, iLastPosition);
                }

            }
            //else if only the left child exists
            else if (iIndexLeftChild <= iLastPosition) 
            {
                //Get a reference to the left child
                tLeft = array[iIndexLeftChild];
                if (tLeft.CompareTo(tParent) > 0)
                {
                    Swap(iIndexLeftChild, iParentIndex);
                }
                
            }
            //else, it must be a leaf so do nothing
        }

        private void Heapify()
        {
            //Get the index of the last parent in the heap
            int iLastParent = GetParentIndex(array.Length - 1);

            //for each parent in the heap starting from the bottom moving to the top
            for (int i = iLastParent; i >= 0; i--)
            {
                //Sift the current element down
                SiftDown(i, array.Length - 1);
            }
        }

        /// <summary>
        /// Given a child index find its parent
        /// </summary>
        /// <param name="index">Index of child</param>
        /// <returns>Index of parent</returns>
        int GetParentIndex(int index)
        {
            return (index - 1) / 2;
        }

        /// <summary>
        /// Return the left child of a parent
        /// </summary>
        /// <param name="index">Index of the parent</param>
        /// <returns>Index of the left child</returns>
        int GetLeftChildIndex(int index)
        {
            return (index * 2) + 1;
        }

        /// <summary>
        /// Return the right child of a parent
        /// </summary>
        /// <param name="index">Index of the parent</param>
        /// <returns>Index of the left child</returns>
        int GetRightChildIndex(int index)
        {
            return (index * 2) + 2;
        }
    }
}