﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sorting
{
    class QuickSorterMedianOf3<T>: QuickSorterRandom<T>
        where T : IComparable<T>
    {
        public QuickSorterMedianOf3(T[] array)
            : base(array)
        {
        }

        protected override int FindPivot(int left, int right)
        {
            //find the middle position
            int mid = (left + right) / 2;
            //Compare the left with the middle and swap if necessary
            if (array[left].CompareTo(array[mid]) > 0)
            {
                Swap(left, mid);
            }
            //Compare first with last
            if (array[left].CompareTo(array[right - 1]) > 0)
            {
                Swap(left, right - 1);
            }
            //Compare mid with last
            if(array[mid].CompareTo(array[right - 1]) > 0)
            {
                Swap(mid, right - 1);
            }

            return mid;
        }
    }
}
