﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sorting
{
    class QuickSorterRandom<T>: QuickSorter<T>
        where T : IComparable<T>
    {
        private Random r;

        public QuickSorterRandom(T[] array)
            : base(array)
        {
            r = new Random(DateTime.Now.Millisecond);
        }

        protected override int FindPivot(int left, int right)
        {
            return r.Next(left, right + 1);
        }
    }
}
