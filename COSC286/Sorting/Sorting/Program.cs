﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sorting
{
    class Program
    {
        static void Main(string[] args)
        {
            StartTest();
        }
        private static void StartTest()
        {
            //Get size of array from the user
            Console.WriteLine("Enter number of elements: ");
            String input = Console.ReadLine();
            int arraySize = Int32.Parse(input);

            //Allocate the array and fill with random numbers
            int[] array = new int[arraySize];
            Random r = new Random(DateTime.Now.Millisecond);
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = r.Next(array.Length * 100);
            }

            //call the sort test code            
            //TestSorter(new BubbleSorter<int>(array));
            //TestSorter(new BubbleSorter<int>(array));
            //TestSorter(new InsertionSorter<int>(array));
            //TestSorter(new InsertionSorter<int>(array));
            //TestSorter(new QuickSorter<int>(array));
            //TestSorter(new QuickSorterRandom<int>(array));
            //TestSorter(new QuickSorterMedianOf3<int>(array));
            TestSorter(new HeapSorter<int>(array));
        }

        private static void TestSorter(ASorter<int> sorter)
        {
            Console.WriteLine(sorter.GetType().Name + " with " +
                sorter.Length + " elements");
            if (sorter.Length <= 50)
            {
                Console.WriteLine("Before the sort:\n" + sorter);
            }
            //Start timing
            long startTime = Environment.TickCount;
            sorter.Sort();
            long endTime = Environment.TickCount;

            //Print the array again after sorting
            if (sorter.Length <= 50)
            {
                Console.WriteLine("After the sort:\n" + sorter);
            }

            Console.WriteLine("Sort took " + (endTime - startTime) + " milliseconds.");
        }
    }
}
