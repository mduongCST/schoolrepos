﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sorting
{
    public class BubbleSorter<T> : ASorter<T>
        where T: IComparable<T>
    {
        //constructor
        public BubbleSorter(T[] array)
            : base(array)
        {

        }
        public override void Sort()
        {
            //code the bubble sort here according to the algorithm
            for (int i = Length - 1; i >= 1; i--)
            {
                for (int j = 0; j <= i - 1; j++)
                {
                    if (array[j].CompareTo(array[j+1]) == 1)
                    {
                        Swap(j, j + 1);
                    }
                }
            }
        }
    }
}
