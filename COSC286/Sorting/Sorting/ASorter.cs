﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sorting
{
    public abstract class ASorter<T>
        where T : IComparable<T>
    {
        #region Attributes
        protected T[] array;
        #endregion

        #region Constructors
        public ASorter(T[] array)
        {
            this.array = array;
        }
        #endregion

        #region Abstract Methods
        public abstract void Sort();
        #endregion

        #region Helper Methods
        protected void Swap(int first, int second)
        {
            T temp = array[first];
            array[first] = array[second];
            array[second] = temp;
        }

        public int Length
        {
            get { return array.Length; }
        }

        //An example of an indexer
        public T this [int index]
        {
            get
            {
                return array[index];
            }
            set
            {
                array[index] = value;
            }
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder("[");
            foreach (T val in array)
            {
                result.Append(val);
                result.Append(", ");
            }
            if (result.Length > 1)
            {
                result.Remove(result.Length - 2, 2);
            }
            result.Append("]");
            return result.ToString();    
        }
        #endregion
    }
}

