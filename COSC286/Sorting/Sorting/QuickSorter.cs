﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sorting
{
    public class QuickSorter<T> : ASorter<T>
        where T: IComparable<T>
    {
        public QuickSorter(T[] array)
            : base(array)
        {

        }

        public override void Sort()
        {
            QuickSortRecursive(0, array.Length - 1);
        }

        private void QuickSortRecursive(int left, int right)
        {
            //If there is at least two elements in the array, sort
            if (left < right)
            {
                //Find the pivot location
                int pivotIndex = FindPivot(left, right);
                //Partition the array between left and right inclusive
                pivotIndex = Partition(left, right, pivotIndex);
                //Quicksort the left of the current pivot
                QuickSortRecursive(left, pivotIndex - 1);
                //Quicksort the right of the current pivot
                QuickSortRecursive(pivotIndex + 1, right);

            }
        }

        /// <summary>
        /// Partitions the array between left and right inclusive
        /// </summary>
        /// <param name="left">left bound of the array</param>
        /// <param name="right">right bound of the array</param>
        /// <param name="pivotIndex">initial index of the pivot</param>
        /// <returns>Final index of the pivot</returns>
        private int Partition(int left, int right, int pivotIndex)
        {
            T pivotValue = array[pivotIndex];
            //move the pivot to the rightmost position
            Swap(pivotIndex, right);
            //Left and right pointers
            int iLeft = left;
            int iRight = right - 1;
                                                                                                                                    
            //While left has not crossed over right
            while (iLeft <= iRight)
            {
                //While the left pointer points to an element less than the pivot
                while (iLeft <= iRight && array[iLeft].CompareTo(pivotValue) <= 0)
                {
                    //increment left pointer
                    iLeft++;
                }
                //While the right points to an element greater than the pivot
                while (iLeft <= iRight && array[iRight].CompareTo(pivotValue) > 0)
                {
                    //decrement right pointer
                    iRight--;
                }
                //if left is less than right
                if (iLeft < iRight)
                {
                    //swap elements at left and right
                    Swap(iLeft, iRight);
                }                                                                                     
            }
            //swap the pivot value with the value where left points   
            Swap(iLeft, right);
            return iLeft;
        }

        protected virtual int FindPivot(int left, int right)
        {
            return (left + right) / 2;

        }
    }
}
