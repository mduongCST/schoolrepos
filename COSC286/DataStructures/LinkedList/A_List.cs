﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataStructuresCommon;

namespace LinkedList
{
    public abstract class A_List<T> : A_Collection<T>, I_List<T> where T: IComparable<T>
    {

        public T ElementAt(int index)
        {
            int count = 0;
            //Get an enumerator
            IEnumerator<T> myEnum = GetEnumerator();
            myEnum.Reset();
            //loop until index is found or there are no more elements
            while (myEnum.MoveNext() && count != index)
            {
                count++;
            }
            //At this point, we are at the correct index or we ran out of elements
            if (count != index)
            {
                //We went out of bounds
                throw new IndexOutOfRangeException("Invalid index " + index);
            }
            return myEnum.Current;
        }

        public int IndexOf(T data)
        {
            int foundIndex = -1;
            IEnumerator<T> myEnum = GetEnumerator();
            myEnum.Reset();
            //Index that we are currently checking
            int currCount = 0;
            //Loop while not found and there are still items to check
            while (foundIndex < 0 && myEnum.MoveNext())
            {
                if (myEnum.Current.Equals(data))
                {
                    foundIndex = currCount;
                }
                currCount++;
            }
            return foundIndex;
        }

        #region Abstract Methods
        public abstract void Insert(int index, T data);
        public abstract T RemoveAt(int index);
        public abstract T ReplaceAt(int index, T data);
        #endregion

    }
}
