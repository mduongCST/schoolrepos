﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BinarySearchTree
{
    public class AVLT<T>: BST<T> where T: IComparable<T>
    {
        #region Balance Algorithm
        internal override Node<T> Balance(Node<T> nCurrent)
        {
            Node<T> nNewRoot = nCurrent;
            int diff = 0;
            int rightDiff = 0;
            int leftDiff = 0;
            if (nCurrent != null)
            {
                diff = GetHeightDifference(nCurrent);
                if (diff < -1)
                {
                    rightDiff = GetHeightDifference(nCurrent.Right);
                        if(rightDiff > 0 )
                        {
                            nNewRoot = DoubleLeft(nCurrent);
                        }
                        else
                        {
                            nNewRoot = SingleLeft(nCurrent);
                        }
                }
                else if( diff > 1)
                {
                    leftDiff = GetHeightDifference(nCurrent.Left);
                        if(leftDiff < 0)
                        {
                            nNewRoot = DoubleRight(nCurrent);
                        }
                        else
                        {
                            nNewRoot = SingleRight(nCurrent);
                        }

                }
            }
            return nNewRoot;
        }

        #endregion

        #region Rotation Methods
        /// <summary>
        /// Pass in the old root, do the rotaitions return the new root
        /// </summary>
        /// <param name="nOldRoot">The old root</param>
        /// <returns>The new Root</returns>
        private Node<T> SingleLeft(Node<T> nOldRoot)
        {
            //Step 1 
            Node<T> nNewRoot = nOldRoot.Right;
            //Step 2
            nOldRoot.Right = nNewRoot.Left;
            //Step 3
            nNewRoot.Left = nOldRoot;

            return nNewRoot;
        }

        private Node<T> SingleRight(Node<T> nOldRoot)
        {
            //Step 1
            Node<T> nNewRoot = nOldRoot.Left;  
            //Step 2
            nOldRoot.Left = nNewRoot.Right;
            //Step 3
            nNewRoot.Right = nOldRoot;

            return nNewRoot;
        }

        private Node<T> DoubleLeft(Node<T> nOldRoot)
        {
           nOldRoot.Right = SingleRight(nOldRoot.Right);
           return SingleLeft(nOldRoot);            
        }

        private Node<T> DoubleRight(Node<T> nOldRoot)
        {
            nOldRoot.Left = SingleLeft(nOldRoot.Left);
            return SingleRight(nOldRoot);
        }


        #endregion

        #region Height Difference
        /// <summary>
        /// Returns the height difference. A negative indicates right heavy
        /// and a positive indicates left heavy
        /// </summary>
        /// <param name="nCurrent"></param>
        /// <returns></returns>
        private int GetHeightDifference(Node<T> nCurrent)
        {
            int iHeightLeft = -1;
            int iHeightRight = -1;
            int iHeightDiff = 0;

            if (nCurrent.Right != null)
            {
                iHeightRight = RecHeight(nCurrent.Right);
            }
            if (nCurrent.Left != null)
            {
                iHeightLeft = RecHeight(nCurrent.Left);
            }

            return iHeightDiff = iHeightLeft - iHeightRight;

        }
        #endregion


    }
}
