﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BinarySearchTree
{
    class Program
    {
        //A method to pass into the Iterate method
        public static void PrintInt(int x)
        {
            Console.Write(x + " ");
        }
        public static void TestRandom(I_BST<int> iTree)
        {
            const int numElements = 25000;
            Random r = new Random(DateTime.Now.Millisecond);
            long startTime = Environment.TickCount;

            for (int i = 0; i < numElements; i++)
            {
                iTree.Add(r.Next(0,1000));
            }
            long endTime = Environment.TickCount;
            Console.WriteLine("Tree type is " + iTree.GetType().ToString());
            Console.WriteLine("Add Time " + (endTime - startTime) + " milliseconds");
            Console.WriteLine("Height is " + iTree.Height());
            Console.WriteLine("\n" + iTree.ToString());
        }

        static void Main(string[] args)
        {
#region Assignment 1 Testing
            BST<int> question1Tree = new BST<int>();
            question1Tree.Add(42);
            question1Tree.Add(12);
            question1Tree.Add(53);
            question1Tree.Add(8);
            question1Tree.Add(16);
            question1Tree.Add(60);
            question1Tree.Add(2);
            question1Tree.Add(22);
            question1Tree.Add(57);
            question1Tree.Add(65);
            question1Tree.Add(19);

            question1Tree.Remove(42);

            question1Tree.iterate(PrintInt, TRAVERSALORDER.PRE_ORDER);

            Console.WriteLine("\n");
            BST<int> question1Subtree = new BST<int>();
            question1Subtree.Add(12);
            question1Subtree.Add(8);
            question1Subtree.Add(16);

            Console.WriteLine("\n");
            question1Subtree.iterate(PrintInt, TRAVERSALORDER.PRE_ORDER);

            if (question1Tree.IsSubtree(question1Subtree))
            {
                Console.WriteLine("Is a subtree!");
            }

            BST<int> question2Tree = new BST<int>();
            question2Tree.Add(5);
            question2Tree.Add(2);
            question2Tree.Add(18);
            question2Tree.Add(-4);
            question2Tree.Add(3);


            Console.Write(question2Tree.PrintLevel(3) + "\n");
            Console.Write("\nIs there any nodes on the fourth level? " +  question2Tree.PrintLevel(4) + "\n");

            BST<int> question3Tree = new BST<int>();
            question3Tree.Add(8);
            question3Tree.Add(11);
            question3Tree.Add(6);
            question3Tree.Add(1);
            question3Tree.Add(4);
            question3Tree.Add(2);
            question3Tree.Add(9);
            question3Tree.Add(7);
            question3Tree.Add(5);
            question3Tree.Add(3);
            question3Tree.Add(10);

            Console.WriteLine("\n");

            question3Tree.iterate(PrintInt, TRAVERSALORDER.POST_ORDER);
            
#endregion

        }
    }
}
