﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;


namespace Hashtable
{
    class Program
    {
        public static void TestAdd()
        {
            HT_Chaining<int, string> ht = new HT_Chaining<int, string>();
            ht.Add(675890765, "Rob Miller");
            ht.Add(654893210, "Terry Peckham");
            ht.Add(765890564, "Ron New");
            ht.Add(893657374, "Rick Caron");
            ht.Add(488488585, "Bryce Barrie");

        }

        /// <summary>
        /// Loads data from a file into a hashtable
        /// </summary>
        /// <param name="ht"></param>
        static void LoadDataFromFile(A_Hashtable<Person, Person> ht)
        {
            StreamReader sr = new StreamReader(File.Open("PeopleSmall.txt", FileMode.Open));
            string sInput = "";

            try
            {
                //Read a line from the file
                while ((sInput = sr.ReadLine()) != null)
                {
                    try
                    {
                        char[] cArray = { ' ' };
                        string[] sArray = sInput.Split(cArray);
                        int iSSN = Int32.Parse(sArray[0]);
                        Person p = new Person(iSSN, sArray[2], sArray[1]);
                        ht.Add(p, p);

                    }
                    catch (ApplicationException ae)
                    {
                        Console.WriteLine("Exception: " + ae.Message);
                    }

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            sr.Close();
        }

        static void TestHT(A_Hashtable<Person, Person> ht)
        {
            LoadDataFromFile(ht);
            Console.WriteLine(ht.ToString());
            Console.WriteLine("Hash table type " + ht.GetType().ToString());
            Console.WriteLine("# of People = " + ht.Count);
            Console.WriteLine("Number of collisions: " + ht.NumCollisions + "\n");
        }
        static void Main(string[] args)
        {            
            HT_Chaining<Person, Person> ht = new HT_Chaining<Person, Person>();            
            Linear<Person, Person> htLin = new Linear<Person, Person>();
            Quadratics<Person, Person> htQuad = new Quadratics<Person, Person>();
            DoubleHash<Person, Person> htDoub = new DoubleHash<Person, Person>();         
        }
    }
}
