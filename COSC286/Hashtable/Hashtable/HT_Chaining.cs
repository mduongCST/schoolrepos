﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Hashtable
{
    public class HT_Chaining<K,V>: A_Hashtable<K,V>
        where V: IComparable<V>
        where K: IComparable<K>
    {
        //Initalize size of oDataArray
        private const int iInitialSize = 4;
        //How many buckets (locations in the array) are occupied by ArrayLists
        private int iBucketCount = 0;

        public HT_Chaining()
        {
            //Set up the Array
            this.oDataArray = new object[iInitialSize];
        }

        public HT_Chaining(int iInitialzeSize, double dLoadFactor)
        {
            this.oDataArray = new object[iInitialSize];
            this.dLoadFactor = dLoadFactor;
           
        }

        public override V Get(K key)
        {
            throw new NotImplementedException();
        }

        public override void Add(K key, V vValue)
        {
            //hash code <-- hash of key
            int iHashCode = HashFunction(key);
            KeyValue<K, V> kv = new KeyValue<K, V>(key, vValue, 0);
            //if location at hash code is null
            if (oDataArray[iHashCode] == null)
            {
                //Array at location <-- new arrayList
                oDataArray[iHashCode] = new ArrayList();
                //increment bucketCount 
                iBucketCount++;
            }
            //else
            else
            {
                //increment collision count
                iNumCollisions++;
            }
            //Get a reference to the arrayList
            ArrayList alCurrent = (ArrayList) oDataArray[iHashCode];
            //if key arrayList at hashLocation contains Key
            if (alCurrent.Contains(kv))
            {
                //throw an exception
                throw new ApplicationException("Object already exists in the hashtable");
            }
            //else
            else
            {
                //add the key value pair to the array list
                alCurrent.Add(kv);
                //increment count
                iCount++;
            }
            if (IsOverLoaded())
            {
                ExpandHashTable();
            }

        }

        private void ExpandHashTable()
        {
            //oldArray <-- dataArray
            object[] oOldaArray = oDataArray;     
       
            //dataArray <-- new Array twice the size of oldArray
            oDataArray = new object[HTSize * 2];

            //reset count, bucketcount, numcollisions
            iCount = 0;
            iBucketCount = 0;
            iNumCollisions = 0;
            //for each bucket in the data array
            for (int i = 0; i < oOldaArray.Length; i++)
            {
                //if bucket not null not (an arraylist)                
                //for each key value pair in the arraylist
                if (oOldaArray[i] != null)
                {
                    ArrayList alCurrent = (ArrayList)oOldaArray[i];
                    foreach (KeyValue<K, V> kv in alCurrent)
                    {
                        //add the key and value to current hashtable
                        Add(kv.Key, kv.Value);
                    }
                }
            }


        }

        private bool IsOverLoaded()
        {
            //If the percentage full is greater than or equal to the load factor, we are overloaded
            return dLoadFactor <= ((double)iBucketCount / HTSize);
        }

        public override void Remove(K key)
        {
            
            int iHashCode = HashFunction(key);

            KeyValue<K, V> kv = new KeyValue<K, V>(key, default(V), 0);            
            ArrayList alCurrent = (ArrayList)oDataArray[iHashCode];
            //Don't worry about shrinking the array
            //Throw an exception if value is not in the hastable
            if (alCurrent != null)
            {
                if (alCurrent.Contains(kv))
                {
                    alCurrent.Remove(kv);
                    iCount--;
                }
                //If an array list is empty, set that location in the array to null
                if (alCurrent.Count == 0)
                {
                    oDataArray[iHashCode] = null;
                    iBucketCount--;
                }

            }
            else
            {
                throw new ApplicationException("Key was not found");
            }
        }

        public override IEnumerator<V> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            //Loop through each bucket
            for (int i = 0; i < HTSize; i++)
            {
                //Add the bucket number to the string
                sb.Append("Bucket " + i.ToString() + ": ");
                //check if an arraylist exists at this location
                if (oDataArray[i] != null)
                {
                    ArrayList alCurrent = (ArrayList)oDataArray[i];
                    foreach (KeyValue<K, V> kv in alCurrent)
                    {
                        sb.Append(kv.Value.ToString() + " --> ");
                    }
                    sb.Remove(sb.Length - 5, 5);
                }
                sb.Append("\n");

            }
            return sb.ToString();
        }
    }
}
