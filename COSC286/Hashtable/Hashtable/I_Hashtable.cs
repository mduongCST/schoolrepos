﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hashtable
{
    //K--> Generic value representing the type of the key.
    public interface I_Hashtable<K, V>: IEnumerable<V>
        where V: IComparable<V>
        where K: IComparable<K>
    {
        /// <summary>
        /// Return a value from a hashtable 
        /// </summary>
        /// <param name="key">key --> the key of the value to return</param>
        /// <returns>V --> The value item returned</returns>
        V Get(K key);

        /// <summary>
        /// Add the key and value as a key-value pair to the hashtable
        /// </summary>
        /// <param name="key">key --> used to determine the location in the array</param>
        /// <param name="vValue">value --> the data value to be stored</param>
        void Add(K key, V vValue);

        /// <summary>
        /// Remove the value from the hastable based on the key
        /// </summary>
        /// <param name="key">key --> used to determine the location of the key-value pair to remove</param>
        void Remove(K key);

        /// <summary>
        /// Remove all values from the hashtable to initialize to the default array size
        /// </summary>
        void Clear();

    }

}
