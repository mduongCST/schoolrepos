﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hashtable
{
    public abstract class A_Hashtable<K,V>: I_Hashtable<K,V>
        where V: IComparable<V>
        where K: IComparable<K>
    {
        //In the case of chaining, this array will store ArrayList object references.
        //In the case of open addressing, the array will store key-value pairs.
        protected object[] oDataArray;
        //Store the number of data elements in the array
        protected int iCount;
        //Load factor - The percentage full when the array needs to be expanded
        protected double dLoadFactor = 0.5;
        //Minimum Load Factor - The percentage full when the array needs to be shrunk
        protected double dMinLoadFactor = 0.3;
        //Collision Count - not necessary, done for statistics 
        protected int iNumCollisions = 0;


        #region Properties
        public int Count
        {
            get { return iCount; }
        }

        public int NumCollisions
        {
            get { return iNumCollisions; }
        }

        //this is public for statistical purposes
        public int HTSize
        {
            get { return oDataArray.Length; }
        }

        #endregion

        #region Other Methods

        protected int HashFunction(K key)
        {
            //The built in GetHAsCode function returns a large or small number that
            //is unique to the object. We mod by hash table size to get within the range
            //of indices of the table. Absolute value makes sure we have positive indices
            //
            return Math.Abs(key.GetHashCode()%HTSize);
        }

        #endregion

        #region I_Hashtable Implementation

        #endregion
        public abstract V Get(K key);
        public abstract void Add(K key, V vValue);
        public abstract void Remove(K key);

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public abstract IEnumerator<V> GetEnumerator();
 

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
