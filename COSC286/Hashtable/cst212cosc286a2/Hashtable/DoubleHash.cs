﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hashtable
{
    class DoubleHash<K,V>: A_OpenAdressing<K,V>
        where V : IComparable<V>
        where K : IComparable<K>
    {
        //Returns the total increment from the initial hash locations. Based on
        //the number of attemps up to this point
        public override int GetIncrement(int iAttempt, K key)
        {
            return (1 + Math.Abs(key.GetHashCode()) % (HTSize - 1)) * iAttempt;
        }
    }
}
