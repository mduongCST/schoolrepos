﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hashtable
{
    class Linear<K,V> : A_OpenAdressing<K,V>
        where V : IComparable<V>
        where K : IComparable<K>
    {
        public override int GetIncrement(int iAttempt, K key)
        {
            int iIncrement = 1; //Set to a constant
            return iIncrement * iAttempt;
        }
    }
}