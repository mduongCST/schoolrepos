﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hashtable
{
    public abstract class A_OpenAdressing<K,V> : A_Hashtable<K,V>
        where V : IComparable<V>
        where K : IComparable<K>
    {
        //Prime Number class
        private PrimeNumber pm = new PrimeNumber();

        public A_OpenAdressing()
        {
            //Initialize the array to a prime numebr size
            oDataArray = new object[pm.GetNextPrime()];
        }
        /// <summary>
        /// Returns the value based on the key passed in.
        /// Throw an application exception if not found.
        /// </summary>
        /// <param name="key">Key passed in</param>
        /// <returns>The Value in the key value pair</returns>
        public override V Get(K key)
        {
            {
                //How many times we asked for the increment
                int iAttempt = 1;
                //Get the intial hash
                int iInitialHash = HashFunction(key);
                //Current location that we are trying to find
                int iCurrentLocation = iInitialHash;

                V valueToGet = default(V);
               
                //Loop finds the location and checks for duplicate values in the sequence
                while (oDataArray[iCurrentLocation] != null)
                {
                    //if the current location is a Key Value pair
                    if (oDataArray[iCurrentLocation].GetType() == typeof(KeyValue<K, V>))
                    {
                        //Set a key value object to the current location in array
                        KeyValue<K, V> kv = (KeyValue<K, V>)oDataArray[iCurrentLocation];
                        
                        //Set a key value object to where we initially hashed
                        KeyValue<K, V> kv2 = (KeyValue<K,V>)oDataArray[iInitialHash];
                        //Key Value pair has been accessed, increment AccessCount
                        kv.AccessCount++;
                        //if the Key Value pair we are accessing has a higher count than the one that was intially hashed
                        if (kv.AccessCount > kv2.AccessCount)
                        {
                            //Remove both objects
                            Remove(kv.Key);
                            Remove(kv2.Key);
                            //If the position we want to add to is a tombstone, set it to null
                            if(oDataArray[iInitialHash].GetType() == typeof(Tombstone))
                            {
                                oDataArray[iInitialHash] = null;
                            }
                            if (oDataArray[iCurrentLocation].GetType() == typeof(Tombstone))
                            {
                                oDataArray[iCurrentLocation] = null;
                            }
                            //Re-add both objects
                            Add(kv.Key, kv.Value);
                            Add(kv2.Key, kv.Value);
                        }
                        //If the key value object's key is equal to the key passed in
                        if (kv.Key.CompareTo(key) == 0)
                        {
                            //Set value to get to the object's value
                            valueToGet = kv.Value;
                        }
                    }                    
                    else
                    {
                        throw new ApplicationException("Key not found in Hashtable");
                    }
                    //Increment to the next location
                    iNumCollisions++;
                    iCurrentLocation = iInitialHash + GetIncrement(iAttempt++, key);
                    iCurrentLocation %= HTSize;
                }
                return valueToGet;
            }
        }

        //Get the next increment from the child class
        public abstract int GetIncrement(int iAttempt, K key);
        public override void Add(K key, V vValue)
        {
            //How many times we asked for the increment
            int iAttempt = 1;
            //Get the intial hash
            int iInitialHash = HashFunction(key);
            //Current location where we are trying to add
            int iCurrentLocation = iInitialHash;
            //Position where we will ultimately add
            int iPositionToAdd = -1;

            //Loop finds the location and checks for duplicate values in the sequence
            while (oDataArray[iCurrentLocation] != null)
            {
                //if the current location is a Key Value pair
                if (oDataArray[iCurrentLocation].GetType() == typeof(KeyValue<K,V>))
                {
                    KeyValue<K, V> kv = (KeyValue<K, V>)oDataArray[iCurrentLocation];
                    if (kv.Key.CompareTo(key) == 0)
                    {
                        throw new ApplicationException("Item already exists");
                    }
                }
                //else it is a tombstone
                else
                {
                    //If this is the first tombstone, mark its location
                    if (iPositionToAdd == -1)
                    {
                        iPositionToAdd = iCurrentLocation;
                    }
                }
                //Increment to the next location
                iNumCollisions++;
                iCurrentLocation = iInitialHash + GetIncrement(iAttempt++, key);
                iCurrentLocation %= HTSize;
            }
            //Are we at a tombstone or at a null at the end of the sequence
            if (iPositionToAdd == -1)
            {
                //Set position to add to the location of the null
                iPositionToAdd = iCurrentLocation;
            }

            //Add the data to the array
            KeyValue<K, V> kvNew = new KeyValue<K, V>(key, vValue);
            oDataArray[iPositionToAdd] = kvNew;
            iCount++;

            if (IsOverLoaded())
            {
                ExpandHashTable();
            }
        }

        /// <summary>
        /// Remove an item if found, by placing a tombstone at the location where found
        /// If not found, throw an exception
        /// </summary>
        /// <param name="key">Key of the item to find</param>
        public override void Remove(K key)
        {
            //How many times we asked for the increment
            int iAttempt = 1;
            //Get the intial hash
            int iInitialHash = HashFunction(key);
            //Current location where we are trying to remove
            int iCurrentLocation = iInitialHash;
            //Loop finds the location and checks for duplicate values in the sequence
            while (oDataArray[iCurrentLocation] != null)
            {
                //if the current location is a Key Value pair
                if (oDataArray[iCurrentLocation].GetType() == typeof(KeyValue<K, V>))
                {
                    KeyValue<K, V> kv = (KeyValue<K, V>)oDataArray[iCurrentLocation];
                    if (kv.Key.CompareTo(key) == 0)
                    {
                        Tombstone t = new Tombstone();
                        oDataArray[iCurrentLocation] = t;
                    }
                    else
                    {
                        throw new ApplicationException("Item doesn't exists");
                    }
                }               
                //Increment to the next location
                iNumCollisions++;
                iCurrentLocation = iInitialHash + GetIncrement(iAttempt++, key);
                iCurrentLocation %= HTSize;
            }
            iCount--;

            if (isUnderLoaded())
            {
                ShrinkHashTable();
            }
        }

        private void ShrinkHashTable()
        {
            object[] temp = oDataArray;
            iCount = 0;
            iNumCollisions = 0;

            //Create a new larger array
            oDataArray = new object[pm.GetLastPrime()];

            for (int i = 0; i < temp.Length; i++)
            {
                //if the current value is not null
                if (temp[i] != null)
                {
                    //if the current value is a keyvalue (and not a tombstone)
                    if (temp[i].GetType() == typeof(KeyValue<K, V>))
                    {
                        KeyValue<K, V> kv = (KeyValue<K, V>)temp[i];
                        Add(kv.Key, kv.Value);
                    }
                }
            }
        }

        /// <summary>
        /// Checks if a key exists in the hashtable
        /// </summary>
        /// <param name="Key"></param>
        /// <returns>returns true if found else false</returns>
        public bool ContainsKey(K Key)
        {
            int iAttempt = 1;
            int iHashCode = HashFunction(Key);
            int iCurrentLocation = iHashCode;
            bool found = false;
            while(oDataArray[iCurrentLocation] != null && !found)
            {
                //if the current location is a Key Value pair
                if (oDataArray[iCurrentLocation].GetType() == typeof(KeyValue<K, V>))
                {
                    KeyValue<K, V> kv = (KeyValue<K, V>)oDataArray[iCurrentLocation];
                    if (kv.Key.CompareTo(Key) == 0)
                    {
                        found = true;
                    }
                    else
                    {
                        found = false;
                    }
                    
                }
                iCurrentLocation = iHashCode + GetIncrement(iAttempt++, Key);
                iCurrentLocation %= HTSize;

            }
            
            return found;
        }

        public override IEnumerator<V> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        private void ExpandHashTable()
        {
            object[] temp = oDataArray;
            iCount = 0;
            iNumCollisions = 0;

            //Create a new larger array
            oDataArray = new object[pm.GetNextPrime()];

            for (int i = 0; i < temp.Length; i++)
            {
                //if the current value is not null
                if (temp[i] != null)
                {
                    //if the current value is a keyvalue (and not a tombstone)
                    if (temp[i].GetType() == typeof(KeyValue<K, V>))
                    {
                        KeyValue<K, V> kv = (KeyValue<K, V>)temp[i];
                        Add(kv.Key, kv.Value);
                    }
                }
            }
        }

        private bool IsOverLoaded()
        {
            return iCount / (double)HTSize > dLoadFactor;
        }

        private bool isUnderLoaded()
        {
            return iCount / (double)HTSize < dMinLoadFactor;
        }


        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < oDataArray.Length; i++)
            {
                sb.Append("Bucket " + i + ": ");
                if (oDataArray[i] != null)
                {
                    if (oDataArray[i].GetType() == typeof(Tombstone))
                    {
                        sb.Append("Tombstone");
                    }
                    else
                    {
                        KeyValue<K, V> kv = (KeyValue<K, V>)oDataArray[i];
                        sb.Append(kv.Value.ToString());
                    }

                }
                sb.Append("\n");
            }
            return sb.ToString();
        }

    }
}
