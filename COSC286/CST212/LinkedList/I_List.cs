﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataStructuresCommon;

namespace LinkedList
{
    public interface I_List<T> : I_Collection<T> where T: IComparable<T>
    {
        T ElementAt(int index);
        int IndexOf(T data);
        void Insert(int index, T data);
        T RemoveAt(int index);
        T ReplaceAt(int index, T data);
    }
}
