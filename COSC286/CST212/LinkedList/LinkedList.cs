﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataStructuresCommon;

namespace LinkedList
{
    public class LinkedList<T>: A_List<T> where T: IComparable<T>
    {
        private Node head;

        public LinkedList()
        {
            head = null;
        }

        public override void Insert(int index, T data)
        {
            //Make sure the index is valid
            if (index > this.Count || index < 0)
            {
                throw new ApplicationException("Beyond bounds of List");
            }
            head = RecIns(index, data, head);
        }

        private Node RecIns(int index, T data, Node current)
        {
            if (index == 0)
            {
                Node newNode = new Node(data, current);
                current = newNode;
            }
            else
            {
                current.next = RecIns(--index, data, current.next);
            }
            return current;
        }

        public override T RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        public override T ReplaceAt(int index, T data)
        {
            throw new NotImplementedException();
        }

        public override void Add(T data)
        {
            head = RecAdd(head, data);  
        }

        private Node RecAdd(Node current, T data)
        { 
          //Base case
            if (current == null)
            {
                current = new Node(data);
            }
          //Otherwise, recurse to the next node and attempt to add
            else
            {
                current.next = RecAdd(current.next, data);
            }
            return current;
        }

        public override void Clear()
        {
            throw new NotImplementedException();
        }

        public override bool Remove(T data)
        {
            return RecRemove(ref head, data);
        }

        //Recursive method for remove
        private bool RecRemove(ref Node current, T data)
        {
            bool found = false;
            if (current == null)
            {
                found = false;
            }
            //compare the current data item to see if it is the current item to remove
            else if (current.data.Equals(data))
            {
                current = current.next;
                found = true;
            }
            else
            {
               found = RecRemove(ref current.next, data);
            }
            return found;
        }

        public override IEnumerator<T> GetEnumerator()
        {
            return new Enumerator(this);
        }

        #region Enumerator Implementation
        private class Enumerator : IEnumerator<T>
        {
            private LinkedList<T> parent;
            private Node lastVisited; //the current node that we are visiting
            private Node scout; //the node ahead of the leastVisited node

            public Enumerator(LinkedList<T> parent)
            {
                this.parent = parent;
                Reset();
            }
            public T Current
            {
                //Return the data element of the current node
                get { return lastVisited.data; }
            }

            public void Dispose()
            {
                //Set all variables to null so the enumerator can no longer be used
                parent = null;
                scout = null;
                lastVisited = null;
            }

            object System.Collections.IEnumerator.Current
            {
                get { return lastVisited; }
            }

            public bool MoveNext()
            {
                bool result = false;
                //if scout is not nall
                if (scout != null)
                {
                    //We can move to the next node
                    lastVisited = scout;
                    //move the scout to the next node
                    scout = scout.next;
                    result = true;
                }
                return result;
            }

            public void Reset()
            {
                //Set the scout to the first element
                scout = parent.head;
                lastVisited = null;
            }
        }
        #endregion

        #region Node Class
        private class Node
        {
            public T data;
            public Node next;

            public Node(T data) : this(data, null)
            { 
            
            }

            public Node(T data, Node next)
            {
                this.data = data;
                this.next = next;
            }
        }
        #endregion
    }
}
