﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataStructuresCommon;

namespace LinkedList
{

    class Program
    {

        public static void TestAdd()
        {
            LinkedList<int> ll = new LinkedList<int>();
            ll.Add(5);
            ll.Add(7);
            ll.Add(3);
            Console.WriteLine("The linked list: " + ll.ToString());
            Console.WriteLine("Item removed? " + ll.Remove(3));
            Console.WriteLine("The linked list: " + ll.ToString());
            ll.Insert(1, 9);
            Console.WriteLine("The linked list: " + ll.ToString());
            

        }
        static void Main(string[] args)
        {
            TestAdd();
        }
    }
}
