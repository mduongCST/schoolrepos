﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataStructuresCommon
{
    public abstract class A_Collection<T>: I_Collection<T> where T : IComparable<T>
    {
        #region Abstract Methods
        public abstract void Add(T Data);
        public abstract void Clear();
        public abstract bool Remove(T data);
        #endregion
        public bool Contains(T Data)
        {
            bool found = false;
            IEnumerator<T> myEnum = GetEnumerator();
            //Reset the enumerator
            myEnum.Reset();

            //Loop through the Data until the item is found or we reach the end of the collection
            while (!found && myEnum.MoveNext())
            {
                found =  myEnum.Current.Equals(Data);
            }
            return found;

        }
        /// <summary>
        /// Loop through the entire collection counting each data item. Note that this is not
        /// the most efficient implementation possible. In order to make this property overrideable,
        /// we mark it as a 'virtual'.
        /// </summary>
        public virtual int Count
        {
            get 
            {
                int count = 0;
                foreach(T item in this)
                {
                    count++;
                }
                return count;

            }
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder("[");
            String sep = ", ";
            foreach (T item in this)
            {
                result.Append(item + sep);
            }

            if (Count > 0)
            {
                result.Remove(result.Length - sep.Length, sep.Length);
            }
            result.Append("]");
            return result.ToString();
        }
        #region

        public abstract IEnumerator<T> GetEnumerator();

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion
    }
}
