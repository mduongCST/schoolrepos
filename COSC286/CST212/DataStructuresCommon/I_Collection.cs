﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataStructuresCommon
{
    /// <summary>
    /// The collection uses generics, thus the T. The generic type T has a restriction
    /// where it must be IComparable
    /// The I_Collection will be IEnumerable (like an iterator in Java)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface I_Collection<T>: IEnumerable<T> where T: IComparable<T>//IEnumerable is like an iterator in JAVA
    {
        /// <summary>
        /// Add the given element to the collection
        /// </summary>
        /// <param name="data">Data to add</param>
        void Add(T data);

        /// <summary>
        /// Removes all items from the collection
        /// </summary>
        void Clear();


        /// <summary>
        /// Determines if the given data exists in the collection
        /// </summary>
        /// <returns>True if found else False</returns>
        bool Contains(T data);

        /// <summary>
        /// Determine if this collection is equal to another
        /// </summary>
        /// <param name="other">The collection to compare to</param>
        /// <returns>True if equal else false</returns>
        bool Equals(object other);

        /// <summary>
        /// Removes a single value from the collection if it exists. If the
        /// value occurs more than once, remove the first instance only.
        /// </summary>
        /// <param name="data">the item to remove</param>
        /// <returns>True if remove else false</returns>
        bool Remove(T data);

        /// <summary>
        /// This is an example of a C# 'Property'. A Property is similar to
        /// a getter/setter in Java. This property returns the number of elements in the collection
        /// </summary>
        int Count
        {
            get;
        }


    }
}
