﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataStructuresCommon;

namespace BinarySearchTree
{
    public class BST<T> : A_BST<T> where T: IComparable<T>
    {
        public BST()
        {
            //Initialize the root to an empty tree
            nRoot = null;
            //Set the count
            iCount = 0;
        }

        public override T Find(T data)
        {
            bool found = false;
            T dataToFind = default(T);

            IEnumerator<T> findEnum = GetEnumerator();

            while (!found && findEnum.MoveNext())
            {
                found = findEnum.Current.Equals(data);
                if (found)
                {
                    dataToFind = findEnum.Current;
                }
            }
            if (!found)
            {
                throw new ApplicationException("Error: Value not found!");
            }    
            return dataToFind;
        }

        #region Assignment 1 Code
        
        public bool IsSubtree(BST<T> subTree)
        {
            //Set found to false
            bool found = false;

            //Set variable for subTree's root
            Node<T> subTreeRoot = subTree.nRoot;
            //Set up enumerator for tree you are trying to find node in
            IEnumerator<T> findEnum = GetEnumerator();
            //Set up enumerator for subtree
            IEnumerator<T> subTreeEnum = subTree.GetEnumerator();
            //Start on subtree node
            subTreeEnum.MoveNext();
                //Loop while there is no matching Nodes AND while you can move through the main tree
                while (!found && findEnum.MoveNext())
                {
                    //If the current node in the main tree is equal to the Root of the subtree
                    if (findEnum.Current.Equals(subTreeRoot.Data))
                    {
                        //Set found to true
                        found = true;
                        //While you have found the node in the main tree, traverse through both trees
                        while(found && findEnum.MoveNext() && subTreeEnum.MoveNext())
                        {
                            //If the current node does not equal the current node in the subtree
                            if (!findEnum.Current.Equals(subTreeEnum.Current))
                            {
                                //Set found to false
                                found = false;
                                //Display message to show it is not a subtree
                                Console.WriteLine("Is not a subtree");
                            }                                                                                    
                        }
                    }

                }
            //Return whether it was a sub tree or not
            return found;
        }

        public bool PrintLevel(int iLevel)
        {
            //Set count to zero
            int levelCount = 0;
            //Set boolean to false
            bool sameLevel = false;
            //If the root is not null
            if (nRoot != null)
            {
                //Set sameLevel to recurse into the root
                sameLevel = RecPrintLevel(nRoot, levelCount, iLevel, sameLevel);
            }
            return sameLevel;


        }

        private bool RecPrintLevel(Node<T> nCurrent, int levelCount, int iLevel, bool sameLevel )
        {
            //Once recursion is called increment count
            levelCount++;
            //If the count is less than the level entered
            if (levelCount < iLevel)
            {
                //Check if a left node exists
                if (nCurrent.Left != null)
                {
                    //Recurse to the left
                    sameLevel = RecPrintLevel(nCurrent.Left, levelCount, iLevel, sameLevel);
                }
                //Check if a right node exists
                if (nCurrent.Right != null)
                {
                    //Recurse to the Right
                    sameLevel = RecPrintLevel(nCurrent.Right, levelCount, iLevel, sameLevel);
                }
            }
            //Else if levelCount is equal to the level entered
            else if (levelCount == iLevel)
            {
                //Set boolean to true
                sameLevel = true;
                //Print out nodes on the level
                Console.Write("[" + nCurrent.Data + "] ");
            }
            else
            {
                //else the levels aren't the same
                sameLevel = false;
            }
            //return the boolean value
            return sameLevel;

        }
        #endregion


        public override int Height()
        {
            int iHeight = 0;
            if (nRoot != null)
            {
                iHeight = RecHeight(nRoot);
            }
            return iHeight;
        }

        protected int RecHeight(Node<T> nCurrent)
        {
            int iHeightLeft = 0;
            int iHeightRight = 0;
            int iHeightCurrent = 0;

            if(!nCurrent.isLeaf())
            {

            
            //Some cool code that does something goes here!
            //Get the height of the left subtree
            if (nCurrent.Left != null)
            {
                iHeightLeft = RecHeight(nCurrent.Left);
            }
            //Get the height of the left subtree
            if (nCurrent.Right != null)
            {
                iHeightRight = RecHeight(nCurrent.Right);
            }

            //If the height of the elft is larger than the right we use it,
            //otherwise we use the right height
            if (iHeightLeft > iHeightRight)
            {
                iHeightCurrent = iHeightLeft + 1;
            }
            else
            {
                iHeightCurrent = iHeightRight + 1;
            }            
            }
            return iHeightCurrent;
        }

        public override void iterate(ProcessData<T> pd, TRAVERSALORDER to)
        {
            if (nRoot != null)
            {
                RecIterate(nRoot, pd, to);
            }
        }

        private void RecIterate(Node<T> nCurrent, ProcessData<T> pd, TRAVERSALORDER to)
        {
            //EXAM QUESTION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            //Pre-order traversal guarantees that a parent node is visited prior to
            //any of its children being visited. The order is process data, recurse left,
            //recurse right.
            if (to == TRAVERSALORDER.PRE_ORDER)
            {
                pd(nCurrent.Data);
            }

            //If the left exists, recurse to the left
            if (nCurrent.Left != null)
            {
                RecIterate(nCurrent.Left, pd, to);
            }

            //In-order traversal visits the nodes in ascending or descending order (depends
            //if you recurse right or left first). The order is Recurse Left, process data, Recurse
            //right
            if (to == TRAVERSALORDER.IN_ORDER)
            {
                //Process the data
                pd(nCurrent.Data);
            }

            //If the right exists, recurse to the right
            if (nCurrent.Right != null)
            {
                RecIterate(nCurrent.Right, pd, to);
            }

            //Post-order traversal gurantees that a child node will be processed prior to its parent
            //node being processed. The order is recurse left, recurse right, and process data
            if (to == TRAVERSALORDER.POST_ORDER)
            {
                pd(nCurrent.Data);
            }
        }

        public override void Add(T data)
        {
            //if the root is null
            
            

            if (nRoot == null)
            {
                // root == new node with data
                nRoot = new Node<T>(data);
            }
            else
            {
                //RecAdd - current is the node passed in
                RecAdd(data, nRoot);
                nRoot = Balance(nRoot);

                // else recursively add to the tree
            }
            //increment count
            iCount++;
        }

        private void RecAdd(T data, Node<T> nCurrent)
        {
            //Compare data to add with data of current node
            
            int iResult = data.CompareTo(nCurrent.Data); //CompareTo returns -1 if less than, 0 if equal, and 1 if greater than
            //if data is less than current's data
            if (iResult < 0)
            {
                //if current node has no left child 
                if (nCurrent.Left == null)
                {
                    //current's Left <-- new node with the data
                    nCurrent.Left = new Node<T>(data);
                }
                //else 
                else
                {
                    //recursively add to current's left child.
                    RecAdd(data, nCurrent.Left);
                    nCurrent.Left = Balance(nCurrent.Left);
                }
            }
            //else
            else
            {
                //if current node has no right child
                if (nCurrent.Right == null)
                {
                    //current's Right <-- new node with the data
                    nCurrent.Right = new Node<T>(data);
                }
                //else recursively add to current's right child.
                else
                {
                    RecAdd(data, nCurrent.Right);
                    nCurrent.Right = Balance(nCurrent.Right);
                }
            }
        }

        internal virtual Node<T> Balance(Node<T> nCurrent)
        {
            return nCurrent;
        }

        public override void Clear()
        {
            throw new NotImplementedException();
        }


        
        //Remove
        public override bool Remove(T data)
        {
            bool wasRemoved = false;
            nRoot = RecRemove(nRoot, data, ref wasRemoved);
            nRoot = Balance(nRoot);
            return wasRemoved;
        }

        private Node<T> RecRemove(Node<T> nCurrent, T data, ref bool wasRemoved)
        {
            //Variable for the substitute
            T tSubstitute = default(T);
            int iCompare = 0;

            if (nCurrent != null)
            {
                iCompare = data.CompareTo(nCurrent.Data);

                //if item to remove is smaller than the current node's data 
                if (iCompare < 0)
                {
                    //Rec remove from the current nodes left subtree
                    nCurrent.Left = RecRemove(nCurrent.Left, data, ref wasRemoved);
                    nCurrent.Left = Balance(nCurrent.Left);
                }
                //else it is larger than the current node's data
                else if (iCompare > 0)
                {
                    nCurrent.Right = RecRemove(nCurrent.Right, data, ref wasRemoved);
                    nCurrent.Right = Balance(nCurrent.Right);
                }
                //Yay, we found the node to remove
                else
                {
                    //Indicate we found it
                    wasRemoved = true;
                    //Check if the node containing the data to remove is a leaf
                    if (nCurrent.isLeaf())
                    {
                        //reduce the count of the tree
                        iCount--;
                        //Set the current node to null as that is what gets returned to the parent
                        nCurrent = null;
                    }
                    //Else it is not a leaf and we need to find a substitute
                    else
                    {
                        //Case 2: Remove a non-leaf node
                        //Find the node to remove
                        //Find a suitable substitute in one of two places:
                        //  1.Larest element in the node to remove's left subtree
                        if (nCurrent.Left != null)
                        {
                            tSubstitute = RecFindLargest(nCurrent.Left);
                            nCurrent.Left = RecRemove(nCurrent.Left, tSubstitute, ref wasRemoved);
                            nCurrent.Left = Balance(nCurrent.Left);

                        }
                        //  2.Smallest element in the node to remove's right subtree
                        else
                        {
                            tSubstitute = RecFindSmallest(nCurrent.Right);
                            nCurrent.Right = RecRemove(nCurrent.Right, tSubstitute, ref wasRemoved);
                            nCurrent.Right = Balance(nCurrent.Right);
                        }
                        //Replace the node to remove's data with the substitute.

                        nCurrent.Data = tSubstitute;

                    }
                }
                
            }
            return nCurrent;



            //Recursively remove the substitute from the subtree it was found in
        }

        #region enumerator functionality

        public override IEnumerator<T> GetEnumerator()
        {
            return new Enumerator(this);
        }


        private class Enumerator : IEnumerator<T>
        {
            private BST<T> parent = null;
            private Node<T> nCurrent = null;
            private Stack<Node<T>> sNodes = null;

            public Enumerator(BST<T> parent)
            {
                this.parent = parent;
                Reset();
            }

            public T  Current
            {
                get { return nCurrent.Data; }
            }

            public void  Dispose()
            {
                parent = null;
                nCurrent = null;
                sNodes = null;
            }

            object  System.Collections.IEnumerator.Current
            {
	            get { throw new NotImplementedException(); }
            }

            //CREATES A DEPTH FIRST TRAVERSAL
            //Traverse to the depths of the tree and turn back up
            public bool  MoveNext()
            {
                //result = false
 	            bool result = false;
                //if stack count > 0
                if(sNodes.Count > 0)
                {
                    //result = true
                    result = true;
                    //nCurrent = top element on stack(POP)
                    nCurrent = sNodes.Pop();
                    //if nCurrent Right exists
                    if (nCurrent.Right != null)
                    {
                        //push right on stack
                        sNodes.Push(nCurrent.Right);
                    }
                    //if nCurrent left exists
                    if (nCurrent.Left != null)
                    {
                        //push left on stack
                        sNodes.Push(nCurrent.Left);
                    }
                }
                //return result
                return result;
            }

            public void  Reset()
            {
                sNodes = new Stack<Node<T>>();
                //push the root node on the stack
                if (parent.nRoot != null)
                {
                    sNodes.Push(parent.nRoot);
                }
                nCurrent = null;
            }

        }
        #endregion
        /*BreadthFirstTraversal 
         *Visit all nodes 1 jump away from root
         *Then visit all nodes 2 jumps away
         * and so on level by level
         */


        #region FindSmallest and FindLargest
        public T FindSmallest()
        {
            if (nRoot != null)
            {
                return RecFindSmallest(nRoot);
            }
            else
            {
                throw new ApplicationException("Root is null");
            }
        }

        private T RecFindSmallest(Node<T> nCurrent)
        {
            T tReturn = default(T);
            //if the left note exists
            if (nCurrent.Left != null)
            {
                //Recursively ask the left node for its smallest value
                tReturn = RecFindSmallest(nCurrent.Left);
            }
            //We hit the leftmost node
            else
            {
                tReturn = nCurrent.Data;
            }
            return tReturn;

        }
        public T FindLargest()
        {
            if (nRoot != null)
            {
                return RecFindLargest(nRoot);
            }
            else
            {
                throw new ApplicationException("Root is null");
            }
        }

        private T RecFindLargest(Node<T> nCurrent)
        {
            T tReturn = default(T);
            //if the right note exists
            if (nCurrent.Right != null)
            {
                //Recursively ask the left node for its smallest value
                tReturn = RecFindLargest(nCurrent.Right);
            }
            //We hit the rightmost node
            else
            {
                tReturn = nCurrent.Data;
            }
            return tReturn;
        }


        #endregion
    }
}
