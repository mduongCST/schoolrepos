﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataStructuresCommon;

namespace BinarySearchTree
{
    //Define a delegate that will point to a method that will do something to each
    //data member of type T
    public delegate void ProcessData<T>(T tData);
    //Set up an enumeration to determine the order of iteration
    public enum TRAVERSALORDER {PRE_ORDER, IN_ORDER, POST_ORDER};

    public interface I_BST<T>: I_Collection<T> where T: IComparable<T>
    {
        /// <summary>
        /// Give a data element find corresponing element of equal value 
        /// return it
        /// </summary>
        /// <param name="data">The item to find.</param>
        /// <returns>A reference to the item if found. Else returns the default value for type T</returns>
        T Find(T data);
        int Height();
        /// <summary>
        /// Similar to an enumerator, but more efficient. Also, the iterate method utilizes
        /// a delegate to perform some action on each data item.
        /// </summary>
        /// <param name="pd">A delegate or function pointer</param>
        /// <param name="to">to is the travel order</param>

        void iterate(ProcessData<T> pd, TRAVERSALORDER to);
 
    }
}
