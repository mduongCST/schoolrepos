﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataStructuresCommon;

namespace BinarySearchTree
{
    public class Node<T> where T: IComparable<T>
    {
        #region Attributes
        private T tData;
        private Node<T> nLeft;
        private Node<T> nRight;

        #endregion


        

        #region Constructors
        // T is not necessarily a nullable value
        //ex: T is an int
        // int x = null; badlul
        //int x = 0; correct
        //ex: T is a string
        //string s = null; badlul
        //string s = " "; correct
        //use default(T) for initializing T
        public Node(): this(default(T))
        {
    
        }
        
        public Node(T tdata): this(tdata, null, null)
        { 

        }

        public Node(T tData, Node<T> nLeft, Node<T> nRight)
        {
            this.tData = tData;
            this.nLeft = nLeft;
            this.nRight = nRight;
        }
        #endregion

        #region Properties
        //Note that the default property has both a 'get' and 'set'.
        //you should consider whether you need both get and set.
        public T Data
        {
            get 
            {
                //Can put business rules here if you need to
                return tData; 
            }
            set 
            {
                //again, you can validate 'value' before assigning to tData
                //Not that value is simply a variable for whatever is assigned 
                //to the Data property
                tData = value; 
            }
        }

        public Node<T> Left
        {
            get { return nLeft; }
            set { nLeft = value; }
        }
        
        public Node<T> Right
        {
            get { return nRight; }
            set { nRight = value; }
        }

        #endregion

        #region Other Functionality
        public bool isLeaf()
        {
            return this.Left == null && this.Right == null;
        }
        #endregion
    }
}
