﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataStructuresCommon;

namespace BinarySearchTree
{
    public abstract class A_BST<T>: A_Collection<T>, I_BST<T> where T:IComparable<T>
    {
        #region Attributes
        protected Node<T> nRoot;
        //A counter to keep track of the number of nodes in the tree.
        protected int iCount;
        #endregion

        //A propert to return the count
        public override int Count
        {
            get
            {
                return iCount;
            }
        }

        #region I_BST implementation
        public abstract T Find(T data);
        public abstract int Height();
        public abstract void iterate(ProcessData<T> pd, TRAVERSALORDER to);
        #endregion
    }
}
