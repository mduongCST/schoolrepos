﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HelloWorld_B
{
    class Program
    {
        static void foo(int x)
        {
            Console.WriteLine(x);
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello Cruel World");

            int x = 0;

            foo(x);

            int[] iArray = { 12, 43, 54 };

            foreach (int j in iArray)
            {
                Console.WriteLine("The current Element is {0} and its square is {1}",
                    j, Math.Pow(j, 2));

                foo(j);
            }

            for (int i = 0; i < 10; i++)
            {
                x = i;
                Console.WriteLine("The value of x is " + x.ToString());
            }
        }
    }
}
